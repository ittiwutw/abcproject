webpackJsonp([99],{

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_modal_payment_modal__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams, modalCtrl, rest) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.rest = rest;
        this.params = {};
        this.headerData = {};
        this.blur = navParams.get("blur");
        this.headerData.data = {
            "headerTitle": "User Profile"
        };
        this.currentUser = this.rest.currentUser;
        this.setData();
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CompanyProfilePage');
    };
    ProfilePage.prototype.setData = function () {
        this.params.data = this.currentUser;
        var that = this;
        this.params.events = {
            'onItemClick': function (item) {
                console.log("onItemClick");
            },
            'onClickPayment': function () {
                that.openPaymentModal();
            }
        };
    };
    ProfilePage.prototype.openPaymentModal = function () {
        var shopPopup = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__payment_modal_payment_modal__["a" /* PaymentModalPage */]);
        shopPopup.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/profile/profile.html"*/'<!--\n  Generated template for the CompanyProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <wizard-layout-2 [data]="headerData.data">\n      </wizard-layout-2>\n</ion-header>\n\n<ion-content >\n    <user-profile [data]="params.data" [events]="params.events">\n    </user-profile>\n  </ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_register__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, rest) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.rest = rest;
        this.params = {};
        this.headerData = {};
        this.headerData.data = {
            "headerTitle": "Login Page"
        };
        this.params.data = {
            "username": "Username",
            "password": "Password",
            "register": "Register",
            "login": "Login",
            "logo": "assets/images/logo/login.png",
            "errorUser": "Field can't be empty.",
            "errorPassword": "Field can't be empty.",
            "background": "assets/images/background/29.jpg"
        };
        var that = this;
        this.params.events = {
            onLogin: function (params) {
                //that.clickLogin(params);
            },
            onRegister: function (params) {
                that.clickRegister();
            }
        };
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.clickRegister = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__register_register__["a" /* RegisterPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/login/login.html"*/'<ion-header>\n  <wizard-layout-2 [data]="headerData.data">\n  </wizard-layout-2>\n</ion-header>\n\n<ion-content class=\'has-header has-subheader\' padding *ngIf="params.data.background!=null" background-size default-background\n  [ngStyle]="{\'background-image\': \'url(\' + params.data.background + \')\'}">\n\n  <login-layout-1 [data]="params.data" [events]="params.events">\n  </login-layout-1>\n\n\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_register__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CompanyProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CompanyProfilePage = /** @class */ (function () {
    function CompanyProfilePage(navCtrl, navParams, alertCtrl, rest) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.rest = rest;
        this.params = {};
        this.headerData = {};
        this.blur = navParams.get("blur");
        this.companyId = this.navParams.get("companyId");
        this.lang = rest.getLanguage();
        if (this.lang == '1') {
            this.headerData.data = {
                "headerTitle": "Company Information"
            };
        }
        else {
            this.headerData.data = {
                "headerTitle": "ข้อมูลบริษัท"
            };
        }
        this.params.data = {
            "headerImage": "assets/images/background/29.jpg",
            "image": "https://www.beijerrefthai.com/wp-content/uploads/2018/05/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%8B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B9%80%E0%B8%81%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A7%E0%B8%81%E0%B8%B1%E0%B8%9A%E0%B8%9A%E0%B8%A3%E0%B8%B4%E0%B8%A9%E0%B8%B1%E0%B8%97.jpg",
            "companyName": "ABC (Thailand) Co., Ltd.",
            "companySubTitle": "Product Type : Vehicles & Parts",
            "blur": this.blur,
            "companyId": this.companyId,
            "items": [
                {
                    "id": 1,
                    "address": "25th Fl., Nantawan Bldg., 161 Ratchadamri Road, Lumpini, Pathumwan, Bangkok 10330.",
                    "iconPhone": "ios-phone-portrait",
                    "iconMail": "mail-open",
                    "iconGlobe": "globe",
                    "phone": "099-056-9595",
                    "mail": "hr@abc.com",
                    "globe": "acb.com",
                    "content": "Contact",
                    "subtitle": "ABC was established in Thailand since 2000, by launching various car models to fulfill entire customer segments, with “Innovation that Excites” policy that enhance highest customers’ satisfaction from our Nissan and enlarge social participation throughout our Corporate Social Responsibility.",
                    "title": "Company Information"
                }
            ]
        };
        var that = this;
        this.params.events = {
            'onItemClick': function (item) {
                console.log("onItemClick");
            },
            'onLike': function (item) {
                if (item && item.like) {
                    if (item.like.isActive) {
                        item.like.isActive = false;
                        item.like.number--;
                    }
                    else {
                        item.like.isActive = true;
                        item.like.number++;
                    }
                }
            },
            'onComment': function (item) {
                if (item && item.comment) {
                    if (item.comment.isActive) {
                        item.comment.isActive = false;
                        item.comment.number--;
                    }
                    else {
                        item.comment.isActive = true;
                        item.comment.number++;
                    }
                }
            },
            'clickRegister': function (item) {
                if (that.blur == 1) {
                    that.presentAlert();
                }
            }
        };
    }
    CompanyProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CompanyProfilePage');
    };
    CompanyProfilePage.prototype.presentAlert = function () {
        var that = this;
        var alert = this.alertCtrl.create({
            title: "Please sign up",
            message: "To see the details, please signup",
            subTitle: "Go to Register Page",
            cssClass: "info-dialog",
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Go',
                    handler: function () {
                        that.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__register_register__["a" /* RegisterPage */]);
                    }
                }
            ]
        });
        alert.present();
    };
    CompanyProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-company-profile',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/company-profile/company-profile.html"*/'<!--\n  Generated template for the CompanyProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <wizard-layout-2 [data]="headerData.data">\n      </wizard-layout-2>\n</ion-header>\n\n<ion-content >\n    <profile-layout-5 [data]="params.data" [companyData]="params.data" [events]="params.events">\n    </profile-layout-5>\n  </ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/company-profile/company-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]])
    ], CompanyProfilePage);
    return CompanyProfilePage;
}());

//# sourceMappingURL=company-profile.js.map

/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, alertCtrl, rest) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.rest = rest;
        this.params = {};
        this.headerData = {};
        this.headerData.data = {
            "headerTitle": "Register Page"
        };
        this.setParameter();
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.setParameter = function () {
        this.params.data = {
            "logo": "assets/images/logo/login.png",
            "iconAccount": "icon-account",
            "username": "Username",
            "iconHome": "icon-home-variant",
            "iconCity": "icon-city",
            "city": "City",
            "iconWeb": "icon-web",
            "country": "Country",
            "iconLock": "icon-lock",
            "password": "Password",
            "iconEmail": "icon-email-outline",
            "iconTel": "icon-phone",
            "iconName": "icon-account",
            "email": "Email",
            "name": "Full Name",
            "tel": "Phone No.",
            "submit": "submit",
            "skip": "Skip",
            "businessField": "Business Field",
            "profession": "profession",
            "iconBag": "icon-briefcase",
            "error": "Field can't be empty.",
            "errorUser": "Field can't be empty.",
            "errorPassword": "Field can't be empty.",
            "errorEmail": "Invalid email address.",
            "errorCountry": "Field can't be empty.",
            "errorCity": "Field can't be empty.",
            "errorName": "Field can't be empty.",
            "errorTel": "Field can't be empty.",
            "errorBusinessField": "Field can't be empty.",
            "errorProfession": "Field can't be empty.",
            "errorAgree": "Please Check Agree on Term & Condition.",
            "accoutType": this.accoutTypes
        };
        var that = this;
        this.params.events = {
            onRegister: function (params) {
                that.saveRegisterUser(params);
            },
            onSkip: function (params) {
                console.log('onSkip');
            },
            onClickTerm: function () {
                that.showTerm();
            }
        };
    };
    RegisterPage.prototype.showTerm = function () {
        var that = this;
        var alert = this.alertCtrl.create({
            title: "Term & Condition",
            message: "Details ....",
            cssClass: "info-dialog",
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'OK',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    RegisterPage.prototype.saveRegisterUser = function (userInputData) {
        console.log(userInputData);
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/register/register.html"*/'<ion-header>\n    <wizard-layout-2 [data]="headerData.data">\n    </wizard-layout-2>\n</ion-header>\n\n<ion-content>\n    <register-layout-2 [data]="params.data" [events]="params.events">\n    </register-layout-2>\n\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__company_profile_company_profile__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CompanyListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CompanyListPage = /** @class */ (function () {
    function CompanyListPage(navCtrl, rest, navParams, platform, statusBar, alertCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.rest = rest;
        this.navParams = navParams;
        this.platform = platform;
        this.statusBar = statusBar;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.params = {};
        this.headerData = {};
        platform.ready().then(function () {
            _this.statusBar.overlaysWebView(false);
        });
        this.lang = rest.getLanguage();
        //this.setCountry();
        if (this.lang == '1') {
            this.headerData.data = {
                "headerTitle": "Company List"
            };
        }
        else {
            this.headerData.data = {
                "headerTitle": "รายชื่อบริษัท"
            };
        }
        this.params.data = {
            "menuId": this.navParams.get("menuId")
        };
        this.params.events = {
            'onItemClick': function (companyId) {
                //if (item == 2) {
                navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__company_profile_company_profile__["a" /* CompanyProfilePage */], { companyId: companyId });
                //} else {
                //  navCtrl.push(CompanyProfilePage, { blur: 0 });
                //}
            },
            'onFavorite': function (item) {
                item.favorite = !item.favorite;
                console.log("onFavorite");
            }
        };
        if (this.rest.currentCountry) {
            this.countryName = this.rest.currentCountry.countryName;
        }
        else {
            this.countryName = "THAILAND";
        }
    }
    CompanyListPage.prototype.setCountry = function () {
        var country = this.rest.currentCountry;
        if (country.countryId == '1') {
            this.countryName = "THAILAND";
        }
        else if (country.countryId == '2') {
            this.countryName = "TAIWAN";
        }
    };
    CompanyListPage.prototype.choseCountry = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Select Business Area",
            message: "Which Country are you looking for ?",
            cssClass: "chooseCountry",
            buttons: [
                {
                    text: 'THAILAND',
                    cssClass: 'chooseCountry',
                    handler: function () {
                        // get object info
                        _this.countryName = "THAILAND";
                        _this.rest.setCountry("TH", "THAILAND");
                        _this.events.publish('changeCountry:changed');
                    }
                },
                {
                    text: 'TAIWAN',
                    handler: function () {
                        _this.countryName = "TAIWAN";
                        _this.rest.setCountry("TW", "TAIWAN");
                        _this.events.publish('changeCountry:changed');
                    }
                }
            ]
        });
        alert.present();
        console.log('current country : ' + this.rest.currentCountry);
    };
    CompanyListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-company-list',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/company-list/company-list.html"*/'<!--Theme Appearance animation (Fade In Right)-->\n<ion-header>\n  <wizard-layout-2 [data]="headerData.data">\n  </wizard-layout-2>\n\n  <ion-navbar>\n    <!-- <ion-title text-uppercase start>COMPANY LIST IN {{countryName}}</ion-title> -->\n    <ion-buttons start>\n      <h3>COMPANY LIST IN {{countryName}}</h3>\n\n    </ion-buttons>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="choseCountry()">\n        <p>Change Country </p>\n        <ion-icon style="margin-left: 2px;" name="arrow-dropdown-circle"></ion-icon>\n      </button>\n\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content class="has-header has-subheader">\n  <appearance-animation-layout-2 [data]="params.data" [events]="params.events">\n  </appearance-animation-layout-2>\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/company-list/company-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], CompanyListPage);
    return CompanyListPage;
}());

//# sourceMappingURL=company-list.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__ = __webpack_require__(664);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__amcharts_amcharts4_maps__ = __webpack_require__(686);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__amcharts_amcharts4_geodata_worldLow__ = __webpack_require__(849);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_rest_rest__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__company_profile_company_profile__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__company_list_company_list__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser_ngx__ = __webpack_require__(169);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// @ts-ignore








var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, rest, events, alertCtrl, iab) {
        this.navCtrl = navCtrl;
        this.rest = rest;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.iab = iab;
        this.data = {};
        this.headerData = {};
        //this.rest.testPOST();
        this.lang = rest.getLanguage();
        this.data = {
            "toolbarTitle": "ABC Application",
            "title": "Wellcome to",
            "subtitle": "ABC Application",
            "subtitle2": "Demo Version",
            "link": "http://csform.com/documentation-for-ionic-2-ui-template-app/",
            "description": "For better understanding how our template works please read documentation.",
            "background": "assets/images/background/29.jpg"
        };
        if (this.lang == '1') {
            this.headerData = {
                "headerTitle": "Choose Country"
            };
        }
        else {
            this.headerData = {
                "headerTitle": "เลือกประเทศ"
            };
        }
    }
    HomePage.prototype.ionViewDidLoad = function () {
        this.loadBanner();
        this.loadMap2();
    };
    HomePage.prototype.onClickBanner = function (companyId, url) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__company_profile_company_profile__["a" /* CompanyProfilePage */], { companyId: companyId });
    };
    HomePage.prototype.loadBanner = function () {
        var that = this;
        this.rest.getBanners().then(function (result) {
            if (result) {
                that.banners = result;
                console.log(that.banners);
                // this.loadMap2();
            }
            else {
                console.log("wrong");
            }
        });
    };
    HomePage.prototype.loadMap = function () {
        var chart = __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__["b" /* create */](this.mapElement.nativeElement, __WEBPACK_IMPORTED_MODULE_5__amcharts_amcharts4_maps__["a" /* MapChart */]);
        //this.globe = new am4maps.projections.Orthographic();
        // Set projection
        chart.projection = new __WEBPACK_IMPORTED_MODULE_5__amcharts_amcharts4_maps__["c" /* projections */].Orthographic();
        // Set map definition
        chart.geodata = __WEBPACK_IMPORTED_MODULE_6__amcharts_amcharts4_geodata_worldLow__["a" /* default */];
        // Set projection
        chart.projection = new __WEBPACK_IMPORTED_MODULE_5__amcharts_amcharts4_maps__["c" /* projections */].Orthographic();
        // Create map polygon series
        var polygonSeries = chart.series.push(new __WEBPACK_IMPORTED_MODULE_5__amcharts_amcharts4_maps__["b" /* MapPolygonSeries */]());
        // Make map load polygon (like country names) data from GeoJSON
        polygonSeries.useGeodata = true;
        polygonSeries.data = [{
                "id": "TH",
                "selected": true
            }, {
                "id": "TW",
                "selected": true
            }];
        // Configure series
        var polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.tooltipText = "{name}";
        polygonTemplate.fill = __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__["a" /* color */]("#fcd736");
        // Create hover state and set alternative fill color
        var hs = polygonTemplate.states.create("hover");
        hs.properties.fill = __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__["a" /* color */]("#367B25");
        polygonTemplate.adapter.add("fill", function (fill, target) {
            var ctx = target.dataItem.dataContext;
            if (ctx && ctx.selected) {
                return __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__["a" /* color */]("#00af23");
            }
            return fill;
        });
        polygonTemplate.adapter.add("stroke", function (fill, target) {
            var ctx = target.dataItem.dataContext;
            if (ctx && ctx.selected) {
                return __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__["a" /* color */]("#00af23");
            }
            return fill;
        });
        // Make globe rotatable
        chart.seriesContainer.draggable = false;
        chart.seriesContainer.resizable = false;
        chart.maxZoomLevel = 2;
        //chart.seriesContainer.events.disableType("doublehit");
        //chart.chartContainer.background.events.disableType("doublehit");
        chart.deltaLongitude = -110;
        var originalDeltaLongitude = 0;
        chart.seriesContainer.events.on("down", function (ev) {
            originalDeltaLongitude = chart.deltaLongitude;
        });
        chart.seriesContainer.events.on("track", function (ev) {
            if (ev.target.isDown) {
                var pointer = ev.target.interactions.downPointers.getIndex(0);
                var startPoint = pointer.startPoint;
                var point = pointer.point;
                var shift = point.x - startPoint.x;
                chart.deltaLongitude = originalDeltaLongitude + shift / 2;
                //label.text = "chart.deltaLongitude = " + chart.numberFormatter.format(chart.deltaLongitude, "[green]#.|[red]#.|[#555]#");
            }
        });
        var zoomed = false;
        polygonTemplate.events.on("doublehit", function (ev) {
            // zoom to an object
            if (zoomed) {
                chart.goHome();
                zoomed = false;
            }
            else {
                chart.maxZoomLevel = 3;
                zoomed = true;
            }
        });
        polygonTemplate.events.on("hit", this.setCountry, this);
    };
    HomePage.prototype.clickSearch = function () {
        this.chartG.goHome();
    };
    HomePage.prototype.loadMap2 = function () {
        this.chartG = __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__["b" /* create */](this.mapElement.nativeElement, __WEBPACK_IMPORTED_MODULE_5__amcharts_amcharts4_maps__["a" /* MapChart */]);
        //this.globe = new am4maps.projections.Orthographic();
        // Set projection
        var chart = this.chartG;
        // Set map definition
        chart.geodata = __WEBPACK_IMPORTED_MODULE_6__amcharts_amcharts4_geodata_worldLow__["a" /* default */];
        // Set projection
        chart.projection = new __WEBPACK_IMPORTED_MODULE_5__amcharts_amcharts4_maps__["c" /* projections */].Miller();
        this.polyG = this.chartG.series.push(new __WEBPACK_IMPORTED_MODULE_5__amcharts_amcharts4_maps__["b" /* MapPolygonSeries */]());
        this.polyG.useGeodata = true;
        this.polyG.exclude = ["AQ"];
        // Create map polygon series
        var polygonSeries = chart.series.push(new __WEBPACK_IMPORTED_MODULE_5__amcharts_amcharts4_maps__["b" /* MapPolygonSeries */]());
        // Make map load polygon (like country names) data from GeoJSON
        polygonSeries.useGeodata = true;
        //chart.seriesContainer.draggable = false;
        chart.maxPanOut = 0;
        //chart.minZoomLevel = 20;
        // chart.zoomControl = new am4maps.ZoomControl();
        // chart.zoomControl.plusButton.disabled = true;
        // chart.zoomControl.minusButton.disabled = true;
        polygonSeries.data = [{
                "id": "TH",
                "selected": true
            }, {
                "id": "TW",
                "selected": true
            }, {
                "id": "HK",
                "selected": true
            }];
        // Configure series
        var polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.tooltipText = "{name}";
        polygonTemplate.fill = __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__["a" /* color */]("#fcd736");
        // Create hover state and set alternative fill color
        var hs = polygonTemplate.states.create("hover");
        hs.properties.fill = __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__["a" /* color */]("#367B25");
        // Remove Antarctica
        // polygonSeries.include = [
        //   "AM",
        //   "AZ",
        //   "BH",
        //   "BD",
        //   "BT",
        //   "IO",
        //   "BN",
        //   "KH",
        //   "CN",
        //   "CX",
        //   "CC",
        //   "CY",
        //   "GE",
        //   "HK",
        //   "IN",
        //   "ID",
        //   "IR",
        //   "IQ",
        //   "IL",
        //   "JP",
        //   "JO",
        //   "KZ",
        //   "KP",
        //   "KR",
        //   "KW",
        //   "KG",
        //   "LA",
        //   "LB",
        //   "MO",
        //   "MY",
        //   "MV",
        //   "MN",
        //   "MM",
        //   "NP",
        //   "OM",
        //   "PK",
        //   "PS",
        //   "PH",
        //   "QA",
        //   "SA",
        //   "SG",
        //   "LK",
        //   "SY",
        //   "TW",
        //   "TJ",
        //   "TH",
        //   "TL",
        //   "TM",
        //   "AE",
        //   "UZ",
        //   "VN",
        //   "YE"
        // ];
        polygonSeries.exclude = ["AQ"];
        // let zoomTo = ["TH", "TW", "CN"];
        // chart.events.on("ready", function (ev) {
        //   // Init extremes
        //   var north, south, west, east;
        //   // Find extreme coordinates for all pre-zoom countries
        //   for (let i = 0; i < zoomTo.length; i++) {
        //     var country = polygonSeries.getPolygonById(zoomTo[i]);
        //     if (north == undefined || (country.north > north)) {
        //       north = country.north;
        //     }
        //     if (south == undefined || (country.south < south)) {
        //       south = country.south;
        //     }
        //     if (west == undefined || (country.west < west)) {
        //       west = country.west;
        //     }
        //     if (east == undefined || (country.east > east)) {
        //       east = country.east;
        //     }
        //     country.isActive = true
        //   }
        //   // Pre-zoom
        //   chart.zoomToRectangle(north, east, south, west, 1, true);
        // });
        polygonTemplate.adapter.add("fill", function (fill, target) {
            var ctx = target.dataItem.dataContext;
            if (ctx && ctx.selected) {
                return __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__["a" /* color */]("#00af23");
            }
            return fill;
        });
        polygonTemplate.adapter.add("stroke", function (fill, target) {
            var ctx = target.dataItem.dataContext;
            if (ctx && ctx.selected) {
                return __WEBPACK_IMPORTED_MODULE_4__amcharts_amcharts4_core__["a" /* color */]("#00af23");
            }
            return fill;
        });
        var zoomed = false;
        polygonTemplate.events.on("doublehit", function (ev) {
            // zoom to an object
            if (zoomed) {
                chart.goHome();
                zoomed = false;
            }
            else {
                chart.maxZoomLevel = 1;
                zoomed = true;
            }
        });
        polygonTemplate.events.on("hit", this.setCountry, this);
    };
    HomePage.prototype.choseCountry = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Select Business Area",
            message: "Which Country are you looking for ?",
            cssClass: "chooseCountry",
            buttons: [
                {
                    text: 'THAILAND',
                    cssClass: 'chooseCountry',
                    handler: function () {
                        _this.chartG.zoomToMapObject(_this.polyG.getPolygonById("TH"));
                    }
                },
                {
                    text: 'TAIWAN',
                    handler: function () {
                        _this.chartG.zoomToMapObject(_this.polyG.getPolygonById("TW"));
                    }
                }
            ]
        });
        alert.present();
        console.log('current country : ' + this.rest.currentCountry);
    };
    HomePage.prototype.setCountry = function (ev) {
        var _this = this;
        var ctx = ev.target.dataItem.dataContext;
        // get object info
        this.rest.setCountry(ctx.id, ctx.name);
        var alert = this.alertCtrl.create({
            title: ctx.name,
            message: "What are you looking for ?",
            cssClass: "chooseCountry",
            buttons: [
                {
                    text: 'Product',
                    cssClass: 'chooseCountry',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__company_list_company_list__["a" /* CompanyListPage */], { "menuId": 1 });
                    }
                },
                {
                    text: 'Service',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__company_list_company_list__["a" /* CompanyListPage */], { "menuId": 2 });
                    }
                },
                {
                    text: 'All',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__company_list_company_list__["a" /* CompanyListPage */], { "menuId": 3 });
                    }
                }
            ]
        });
        alert.present();
        console.log('current country : ' + this.rest.currentCountry);
    };
    HomePage.prototype.trigerDoubleHit = function () { };
    HomePage.prototype.openLogin = function () {
        if (this.rest.currentUser) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__profile_profile__["a" /* ProfilePage */]);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
        }
    };
    HomePage.prototype.onClickLanguage = function (languageId) {
        this.lang = languageId;
        this.rest.setLanguage(languageId);
        this.events.publish('language:changed');
    };
    HomePage.prototype.presentAlert = function (bannerDetail) {
        var _this = this;
        var that = this;
        var alert = this.alertCtrl.create({
            title: "Confirm",
            subTitle: "Confirm to exit current page ?",
            cssClass: "info-dialog",
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        if (bannerDetail.linkType == '2') {
                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__company_profile_company_profile__["a" /* CompanyProfilePage */], { companyId: bannerDetail.companyId });
                        }
                        else {
                            _this.iab.create(bannerDetail.link, '_system');
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], HomePage.prototype, "mapElement", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/home/home.html"*/'<!-- Main Menu Header -->\n\n<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon class="icon-menu" name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title text-uppercase></ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="openLogin()">\n\n                <ion-icon name="person"></ion-icon>\n\n            </button>\n\n            <button ion-button icon-only padding-left (click)="onClickLanguage(1)">\n\n                <img src="assets/images/flag/en-flag-active.jpg" style="height: 15px;" alt="" *ngIf="lang == \'1\'">\n\n                <img src="assets/images/flag/en-flag.jpg" style="height: 15px;" alt="" *ngIf="lang == \'2\'">\n\n            </button>\n\n            <button style="padding-left: 5px;" ion-button icon-only (click)="onClickLanguage(2)">\n\n                <img src="assets/images/flag/thai-flag-active.jpg" style="height: 15px;" alt="" *ngIf="lang == \'2\'">\n\n                <img src="assets/images/flag/thai-flag.jpg" style="height: 15px;" alt="" *ngIf="lang == \'1\'">\n\n            </button>\n\n\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n    <ion-header-bar custom-subheader class="bar-subheader bar-stable">\n\n        <ion-slides pager="true" autoplay="5000" loop="true" speed="500" *ngIf="banners">\n\n            <ion-slide background-size *ngFor="let img of banners.listBanner">\n\n                <img [src]="img.imageUrl" style="width: -webkit-fill-available; height: -webkit-fill-available;"\n\n                    (click)="presentAlert(img)">\n\n            </ion-slide>\n\n        </ion-slides>\n\n    </ion-header-bar>\n\n    <ion-header-bar custom-subheader class="bar-subheader bar-stable">\n\n        <!-- <div style="width: 100%; height: 350px ; ">\n\n            <div #map id="map" ></div>\n\n        </div> -->\n\n        <div #map id="map" style="width: 100%; height: 350px ; margin-left: 5px; margin-right: 5px;"></div>\n\n        <br>\n\n        <div text-center>\n\n            <img src="../../assets/icon/magnifier.png" alt="" (click)="choseCountry()">\n\n            <br>\n\n            <h2 item-title text-center>BUSINESS AREA</h2>\n\n        </div>\n\n    </ion-header-bar>\n\n\n\n</ion-header>\n\n\n\n<!-- Main Menu List -->\n\n<ion-content background-size default-background >\n\n\n\n    <br>\n\n    <!-- <div #map id="map" style="width: 100%; height: 350px ; margin-left: 5px; margin-right: 5px;" ></div> -->\n\n    <br>\n\n\n\n    <br>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_7__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettings; });
var AppSettings = {
    "IS_FIREBASE_ENABLED": false,
    "SHOW_START_WIZARD": false,
    "SUBSCRIBE": false,
    "TOAST": {
        "duration": 1000,
        "position": "buttom"
    },
    "FIREBASE_CONFIG": {
        "apiKey": "AIzaSyCJgAyuWNAESAGnLEN1hWQ8mEbbPYkJgmo",
        "authDomain": "ionic3-yellow.firebaseapp.com",
        "databaseURL": "https://ionic3-yellow.firebaseio.com",
        "projectId": "ionic3-yellow",
        "storageBucket": "ionic3-yellow.appspot.com",
        "messagingSenderId": "450167842906"
    },
    "MAP_KEY": {
        "apiKey": "AIzaSyCB7jsMv0hjwkVRan7vZZ9hDlI-dQNZRV8"
    }
};
//# sourceMappingURL=app-settings.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_purchase_ngx__ = __webpack_require__(361);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PaymentModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PaymentModalPage = /** @class */ (function () {
    function PaymentModalPage(navCtrl, navParams, platform, inAppPurchase) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.inAppPurchase = inAppPurchase;
        this.product = {
            name: 'Upgrade to Pro',
            appleProductId: 'premiumuser',
            googleProductId: 'com.abcdemo.test.premiumuser'
        };
        platform.ready().then(function () {
            console.log("platform");
            // this.configurePurchasing();
            if (!_this.inAppPurchase) {
                console.log('Store not available.');
                alert('Store not available.');
            }
            else {
                alert('Start');
                alert(inAppPurchase);
                inAppPurchase
                    .getProducts(['com.abcdemo.test.premiumuser'])
                    .then(function (products) {
                    console.log(products);
                    alert(products);
                    /*
                       [{ productId: 'com.yourapp.prod1', 'title': '...', description: '...', currency: '...', price: '...', priceAsDecimal: '...' }, ...]
                    */
                })
                    .catch(function (err) {
                    console.log(err);
                    alert(err);
                });
            }
        });
    }
    PaymentModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-payment-modal',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/payment-modal/payment-modal.html"*/'<!--  Theme Form - Form + Write Comment -->\n<ion-content>\n  <ion-grid class="modal_content">\n    <ion-row padding>\n\n      <ion-col col-12 style="text-align: -webkit-center; width: 50%">\n        <button ion-button default-button block text-capitalize (click)="onEvent(\'onSubmit\', $event)">\n          1 Year $10\n        </button>\n      </ion-col>\n      <ion-col col-12 style="text-align: -webkit-center;">\n        <p text-wrap ion-text color="secondary">Payment with apple strore ....</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/payment-modal/payment-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_purchase_ngx__["a" /* InAppPurchase */]])
    ], PaymentModalPage);
    return PaymentModalPage;
}());

//# sourceMappingURL=payment-modal.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCompanyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser_ngx__ = __webpack_require__(169);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AddCompanyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddCompanyPage = /** @class */ (function () {
    function AddCompanyPage(navCtrl, navParams, iab, rest) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.iab = iab;
        this.rest = rest;
        this.params = {};
        this.headerData = {};
        this.addCompanyUrl = 'http://www.abcappbkk.com/Admin/#/ABC-User/create-profile/';
        this.setData();
        this.userDate = this.rest.currentUser;
        //this.addCompanyUrl = '';
        //this.iab.create('http://www.abcappbkk.com/Admin/#/ABCAdmin/addcompany', '_blank');
    }
    AddCompanyPage.prototype.ngAfterViewInit = function () {
        var url = this.addCompanyUrl + '' + this.rest.currentUser.username + '/' + this.rest.currentUser.password;
        console.log(url);
        this.mapElement.nativeElement.setAttribute("src", url);
    };
    AddCompanyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddCompanyPage');
    };
    AddCompanyPage.prototype.setData = function () {
        this.headerData.data = {
            "headerTitle": "Add Company"
        };
        this.params.data = {
            "headerImage": "assets/images/background/29.jpg",
            "image": "https://www.beijerrefthai.com/wp-content/uploads/2018/05/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%8B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B9%80%E0%B8%81%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A7%E0%B8%81%E0%B8%B1%E0%B8%9A%E0%B8%9A%E0%B8%A3%E0%B8%B4%E0%B8%A9%E0%B8%B1%E0%B8%97.jpg",
            "companyName": "ABC (Thailand) Co., Ltd.",
            "companySubTitle": "Product Type : Vehicles & Parts",
            "blur": "0",
            "companyId": 1,
            "items": [
                {
                    "id": 1,
                    "address": "25th Fl., Nantawan Bldg., 161 Ratchadamri Road, Lumpini, Pathumwan, Bangkok 10330.",
                    "iconPhone": "ios-phone-portrait",
                    "iconMail": "mail-open",
                    "iconGlobe": "globe",
                    "phone": "099-056-9595",
                    "mail": "hr@abc.com",
                    "globe": "acb.com",
                    "content": "Contact",
                    "subtitle": "ABC was established in Thailand since 2000, by launching various car models to fulfill entire customer segments, with “Innovation that Excites” policy that enhance highest customers’ satisfaction from our Nissan and enlarge social participation throughout our Corporate Social Responsibility.",
                    "title": "Company Information"
                }
            ]
        };
        var that = this;
        this.params.events = {
            'onItemClick': function (item) {
                console.log("onItemClick");
            },
            'onLike': function (item) {
                if (item && item.like) {
                    if (item.like.isActive) {
                        item.like.isActive = false;
                        item.like.number--;
                    }
                    else {
                        item.like.isActive = true;
                        item.like.number++;
                    }
                }
            },
            'onComment': function (item) {
                if (item && item.comment) {
                    if (item.comment.isActive) {
                        item.comment.isActive = false;
                        item.comment.number--;
                    }
                    else {
                        item.comment.isActive = true;
                        item.comment.number++;
                    }
                }
            }
        };
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('iframe'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], AddCompanyPage.prototype, "mapElement", void 0);
    AddCompanyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-add-company',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/add-company/add-company.html"*/'<!--\n  Generated template for the CompanyProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <wizard-layout-2 [data]="headerData.data">\n      </wizard-layout-2>\n</ion-header>\n\n<ion-content >\n    \n\n    <div #frame style="width:100%;height:100%;overflow:scroll !important;-webkit-overflow-scrolling:touch !important;-webkit-scrollbar: none">\n      <iframe #iframe width="100%" height="100%" class="iframe" scrolling="yes" ></iframe>\n    </div>\n  </ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/pages/add-company/add-company.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], AddCompanyPage);
    return AddCompanyPage;
}());

//# sourceMappingURL=add-company.js.map

/***/ }),

/***/ 316:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 316;

/***/ }),

/***/ 358:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../components/alert/layout-1/alert-layout-1.module": [
		881,
		90
	],
	"../components/alert/layout-2/alert-layout-2.module": [
		879,
		89
	],
	"../components/alert/layout-3/alert-layout-3.module": [
		880,
		88
	],
	"../components/check-box/layout-1/check-box-layout-1.module": [
		886,
		87
	],
	"../components/check-box/layout-2/check-box-layout-2.module": [
		882,
		86
	],
	"../components/check-box/layout-3/check-box-layout-3.module": [
		884,
		85
	],
	"../components/check-box/layout-4/check-box-layout-4.module": [
		883,
		84
	],
	"../components/comment/layout-1/comment-layout-1.module": [
		885,
		83
	],
	"../components/comment/layout-2/comment-layout-2.module": [
		887,
		82
	],
	"../components/forms/layout-1/form-layout-1.module": [
		888,
		81
	],
	"../components/forms/layout-2/form-layout-2.module": [
		889,
		80
	],
	"../components/forms/layout-3/form-layout-3.module": [
		890,
		79
	],
	"../components/forms/layout-4/form-layout-4.module": [
		892,
		78
	],
	"../components/full-screen-gallery/full-screen-gallery.module": [
		891,
		77
	],
	"../components/image-gallery/layout-1/image-gallery-layout-1.module": [
		893,
		76
	],
	"../components/image-gallery/layout-2/image-gallery-layout-2.module": [
		894,
		75
	],
	"../components/image-gallery/layout-3/image-gallery-layout-3.module": [
		897,
		74
	],
	"../components/list-view/appearance-animation/layout-1/appearance-animation-layout-1.module": [
		895,
		73
	],
	"../components/list-view/appearance-animation/layout-2/appearance-animation-layout-2.module": [
		873,
		98
	],
	"../components/list-view/appearance-animation/layout-3/appearance-animation-layout-3.module": [
		896,
		72
	],
	"../components/list-view/appearance-animation/layout-4/appearance-animation-layout-4.module": [
		898,
		71
	],
	"../components/list-view/appearance-animation/layout-5/appearance-animation-layout-5.module": [
		899,
		70
	],
	"../components/list-view/drag-and-drop/layout-1/drag-and-drop-layout-1.module": [
		900,
		69
	],
	"../components/list-view/drag-and-drop/layout-2/drag-and-drop-layout-2.module": [
		901,
		68
	],
	"../components/list-view/drag-and-drop/layout-3/drag-and-drop-layout-3.module": [
		902,
		67
	],
	"../components/list-view/drag-and-drop/layout-4/drag-and-drop-layout-4.module": [
		903,
		66
	],
	"../components/list-view/expandable/layout-1/expandable-layout-1.module": [
		905,
		65
	],
	"../components/list-view/expandable/layout-2/expandable-layout-2.module": [
		904,
		64
	],
	"../components/list-view/expandable/layout-3/expandable-layout-3.module": [
		906,
		63
	],
	"../components/list-view/google-card/layout-1/google-card-layout-1.module": [
		908,
		62
	],
	"../components/list-view/google-card/layout-2/google-card-layout-2.module": [
		909,
		61
	],
	"../components/list-view/google-card/layout-3/google-card-layout-3.module": [
		907,
		60
	],
	"../components/list-view/google-card/layout-4/google-card-layout-4.module": [
		910,
		59
	],
	"../components/list-view/sticky-list-header/layout-1/sticky-list-header-layout-1.module": [
		913,
		4
	],
	"../components/list-view/sticky-list-header/layout-2/sticky-list-header-layout-2.module": [
		911,
		3
	],
	"../components/list-view/sticky-list-header/layout-3/sticky-list-header-layout-3.module": [
		912,
		2
	],
	"../components/list-view/sticky-list-header/layout-4/sticky-list-header-layout-4.module": [
		914,
		1
	],
	"../components/list-view/swipe-to-dismiss/layout-1/swipe-to-dismiss-layout-1.module": [
		915,
		58
	],
	"../components/list-view/swipe-to-dismiss/layout-2/swipe-to-dismiss-layout-2.module": [
		916,
		57
	],
	"../components/list-view/swipe-to-dismiss/layout-3/swipe-to-dismiss-layout-3.module": [
		918,
		56
	],
	"../components/list-view/swipe-to-dismiss/layout-4/swipe-to-dismiss-layout-4.module": [
		917,
		55
	],
	"../components/login/layout-1/login-layout-1.module": [
		874,
		97
	],
	"../components/login/layout-2/login-layout-2.module": [
		919,
		54
	],
	"../components/maps/layout-1/maps-layout-1.module": [
		921,
		10
	],
	"../components/maps/layout-2/maps-layout-2.module": [
		920,
		53
	],
	"../components/maps/layout-3/maps-layout-3.module": [
		923,
		52
	],
	"../components/parallax/layout-1/parallax-layout-1.module": [
		922,
		9
	],
	"../components/parallax/layout-2/parallax-layout-2.module": [
		924,
		8
	],
	"../components/parallax/layout-3/parallax-layout-3.module": [
		927,
		7
	],
	"../components/parallax/layout-4/parallax-layout-4.module": [
		926,
		6
	],
	"../components/payment/layout-1/payment-layout-1.module": [
		925,
		51
	],
	"../components/profile/add-company/add-company-layout.module": [
		875,
		96
	],
	"../components/profile/layout-1/profile-layout-1.module": [
		928,
		50
	],
	"../components/profile/layout-2/profile-layout-2.module": [
		929,
		49
	],
	"../components/profile/layout-3/profile-layout-3.module": [
		930,
		48
	],
	"../components/profile/layout-4/profile-layout-4.module": [
		931,
		47
	],
	"../components/profile/layout-5/profile-layout-5.module": [
		877,
		95
	],
	"../components/profile/user-profile/user-profile.module": [
		876,
		94
	],
	"../components/register/layout-1/register-layout-1.module": [
		932,
		46
	],
	"../components/register/layout-2/register-layout-2.module": [
		878,
		93
	],
	"../components/search-bar/layout-1/search-bar-layout-1.module": [
		937,
		45
	],
	"../components/search-bar/layout-2/search-bar-layout-2.module": [
		938,
		44
	],
	"../components/search-bar/layout-3/search-bar-layout-3.module": [
		940,
		43
	],
	"../components/segment/layout-1/segment-layout-1.module": [
		939,
		42
	],
	"../components/segment/layout-2/segment-layout-2.module": [
		941,
		41
	],
	"../components/segment/layout-3/segment-layout-3.module": [
		942,
		40
	],
	"../components/select/layout-1/select-layout-1.module": [
		943,
		39
	],
	"../components/select/layout-2/select-layout-2.module": [
		944,
		38
	],
	"../components/select/layout-3/select-layout-3.module": [
		946,
		37
	],
	"../components/select/layout-4/select-layout-4.module": [
		945,
		36
	],
	"../components/select/layout-5/select-layout-5.module": [
		947,
		35
	],
	"../components/select/layout-6/select-layout-6.module": [
		948,
		34
	],
	"../components/spinner/spinner.module": [
		949,
		33
	],
	"../components/splash-screen/layout-1/splash-screen-layout-1.module": [
		950,
		32
	],
	"../components/splash-screen/layout-2/splash-screen-layout-2.module": [
		952,
		31
	],
	"../components/splash-screen/layout-3/splash-screen-layout-3.module": [
		951,
		30
	],
	"../components/sub-image-gallery/sub-image-gallery.module": [
		954,
		29
	],
	"../components/tabs/layout-1/tabs-layout-1.module": [
		953,
		28
	],
	"../components/tabs/layout-2/tabs-layout-2.module": [
		955,
		27
	],
	"../components/tabs/layout-3/tabs-layout-3.module": [
		956,
		26
	],
	"../components/time-line/layout-1/time-line-layout-1.module": [
		957,
		25
	],
	"../components/time-line/layout-2/time-line-layout-2.module": [
		958,
		24
	],
	"../components/time-line/layout-3/time-line-layout-3.module": [
		959,
		23
	],
	"../components/toggle/layout-1/toggle-layout-1.module": [
		960,
		22
	],
	"../components/toggle/layout-2/toggle-layout-2.module": [
		961,
		21
	],
	"../components/toggle/layout-3/toggle-layout-3.module": [
		963,
		20
	],
	"../components/wizard/layout-1/wizard-layout-1.module": [
		962,
		19
	],
	"../components/wizard/layout-2/wizard-layout-2.module": [
		872,
		92
	],
	"../components/wizard/layout-3/wizard-layout-3.module": [
		964,
		18
	],
	"../pages/add-company/add-company.module": [
		965,
		15
	],
	"../pages/company-list/company-list.module": [
		966,
		17
	],
	"../pages/company-profile/company-profile.module": [
		967,
		14
	],
	"../pages/home/home.module": [
		458
	],
	"../pages/login/login.module": [
		968,
		16
	],
	"../pages/payment-modal/payment-modal.module": [
		970,
		91
	],
	"../pages/profile/profile.module": [
		969,
		13
	],
	"../pages/register/register.module": [
		971,
		12
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 358;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__ = __webpack_require__(651);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(654);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestProvider = /** @class */ (function () {
    function RestProvider(http, events, storage) {
        this.http = http;
        this.events = events;
        this.storage = storage;
        this.apiUrl = " http://abcappbkk.ssitconsultant.com/ABC-API/";
        console.log('Hello RestProvider Provider');
        //this.currentLanguage.langId = "1";
    }
    RestProvider.prototype.getMenuList = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var lang;
            if (_this.currentLanguage) {
                lang = _this.currentLanguage;
            }
            else {
                lang = "1";
            }
            _this.http.get(_this.apiUrl + 'getListMenuCompany.php?langId=' + lang).subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getListCompanyByProductType = function (menuId) {
        var _this = this;
        return new Promise(function (resolve) {
            var lang;
            var countryId;
            if (_this.currentLanguage) {
                lang = _this.currentLanguage;
            }
            else {
                lang = "1";
            }
            if (_this.currentCountry) {
                countryId = _this.currentCountry.countryId;
            }
            else {
                countryId = "1";
            }
            _this.http.get(_this.apiUrl + 'getListCompanyByFilter.php?langId=' + lang + '&countryId=' + countryId + '&menuId=' + menuId + '&filter=&limit=5&offset=0').subscribe(function (data) {
                resolve(data);
                console.log(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getCompanyProfile = function (companyId) {
        var _this = this;
        return new Promise(function (resolve) {
            var lang;
            if (_this.currentLanguage) {
                lang = _this.currentLanguage;
            }
            else {
                lang = "1";
            }
            _this.http.get(_this.apiUrl + 'getProfileCompanyInfo.php?langId=' + lang + '&companyId=' + companyId).subscribe(function (data) {
                resolve(data);
                console.log(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.setLanguage = function (language) {
        this.currentLanguage = language;
        console.log("Language change to " + this.currentLanguage);
    };
    RestProvider.prototype.getLanguage = function () {
        return this.currentLanguage;
    };
    RestProvider.prototype.setCountry = function (country, countryName) {
        var countryId;
        if (country == 'TH') {
            countryId = '1';
        }
        else if (country == 'TW') {
            countryId = '2';
        }
        this.currentCountry = {
            countryId: countryId,
            countryName: countryName
        };
        console.log("Country change to " + this.currentCountry);
    };
    RestProvider.prototype.getCountry = function () {
        return this.currentCountry;
    };
    RestProvider.prototype.getBanners = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var lang;
            if (_this.currentLanguage) {
                lang = _this.currentLanguage;
            }
            else {
                lang = "1";
            }
            _this.http.get(_this.apiUrl + 'getListBanner.php').subscribe(function (data) {
                resolve(data);
                console.log(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getMenuLang = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var lang;
            if (_this.currentLanguage) {
                lang = _this.currentLanguage;
            }
            else {
                lang = "1";
            }
            _this.http.get(_this.apiUrl + 'getMenuWording.php?langId=' + lang).subscribe(function (data) {
                _this.setMenuLang(data);
                resolve(data);
                console.log(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.setMenuLang = function (res) {
        this.currentMenu = {
            home: res.almw_Home,
            allCompany: res.almw_AllCompany,
            contactUs: res.almw_ContractUs,
            aboutUs: res.almw_AboutUs,
            login: res.almw_Login,
        };
    };
    RestProvider.prototype.testPOST = function () {
        var link = 'http://localhost/leaveRequestAPI/LeaveRequestService/savePOST.php';
        var myData = JSON.stringify({ username: "XXXXXXX" });
        var datares = {};
        this.http.post(link, myData)
            .subscribe(function (data) {
            datares.response = data["_body"]; //https://stackoverflow.com/questions/39574305/property-body-does-not-exist-on-type-response
            console.log(datares.response);
        }, function (error) {
            console.log("Oooops!");
        });
    };
    RestProvider.prototype.login = function (userName, password, uuid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var header = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]();
            header.append("Content-type", "json/data; charset=utf-8");
            var data = JSON.stringify({ Data: { ID: userName, Pass: password, UID: uuid } });
            _this.http.post(_this.apiUrl + 'Login.php', data, { headers: header })
                .subscribe(function (res) {
                _this.setUserLogin(res);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.setUserLogin = function (resUser) {
        console.log(resUser.aau_username);
        this.currentUser = {
            uid: resUser.aau_UID,
            activeFlg: resUser.aau_activeFlg,
            bussinessField: resUser.aau_bussiness_field,
            city: resUser.aau_city,
            country: resUser.aau_country,
            email: resUser.aau_email,
            member: resUser.aam_name_member,
            name: resUser.aau_name,
            phone: resUser.aau_phone,
            profession: resUser.aau_profession,
            status: resUser.aas_status_name,
            type: resUser.aat_name_type,
            username: resUser.aau_username,
            password: resUser.aau_password
        };
        this.storage.set("user", resUser);
    };
    RestProvider.prototype.logout = function () {
        //console.log(resUser.aau_username)
        this.currentUser = null;
        this.events.publish('logout:changed');
    };
    RestProvider.prototype.getAccoutType = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var lang;
            if (_this.currentLanguage) {
                lang = _this.currentLanguage;
            }
            else {
                lang = "1";
            }
            _this.http.get(_this.apiUrl + 'getListAccountType.php').subscribe(function (data) {
                resolve(data);
                console.log(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getCountries = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var lang;
            if (_this.currentLanguage) {
                lang = _this.currentLanguage;
            }
            else {
                lang = "1";
            }
            _this.http.get(_this.apiUrl + 'getListCountry.php?langId=' + lang).subscribe(function (data) {
                resolve(data);
                console.log(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.register = function (Data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var header = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]();
            header.append("Content-type", "json/data; charset=utf-8");
            var prepareData = { Data: Data };
            var sendData = JSON.stringify(prepareData);
            _this.http.post(_this.apiUrl + 'userRequestRegist.php', sendData, { headers: header })
                .subscribe(function (res) {
                //this.setUserLogin(res);
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(227);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */])
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 500:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WizardLayout2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_profile_profile__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_company_profile_company_profile__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser_ngx__ = __webpack_require__(169);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var WizardLayout2 = /** @class */ (function () {
    function WizardLayout2(navCtrl, rest, events, platform, statusBar, alertCtrl, iab) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.rest = rest;
        this.events = events;
        this.platform = platform;
        this.statusBar = statusBar;
        this.alertCtrl = alertCtrl;
        this.iab = iab;
        platform.ready().then(function () {
            _this.statusBar.overlaysWebView(false);
            //if(window.MobileAccessibility){
            //window.MobileAccessibility.usePreferredTextZoom(false);
            //}
        });
        this.lang = rest.getLanguage();
        this.loadBanner();
    }
    WizardLayout2.prototype.openLogin = function () {
        if (this.rest.currentUser) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_profile_profile__["a" /* ProfilePage */]);
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
        }
    };
    WizardLayout2.prototype.onClickLanguage = function (languageId) {
        this.rest.setLanguage(languageId);
        this.events.publish('language:changed');
    };
    WizardLayout2.prototype.loadBanner = function () {
        var that = this;
        this.rest.getBanners().then(function (result) {
            if (result) {
                that.banners = result;
                console.log(that.banners);
            }
            else {
                console.log("wrong");
            }
        });
    };
    WizardLayout2.prototype.onClickBanner = function (companyId, url) {
    };
    WizardLayout2.prototype.presentAlert = function (bannerDetail) {
        var _this = this;
        var that = this;
        var alert = this.alertCtrl.create({
            title: "Confirm",
            subTitle: "Confirm to exit current page ?",
            cssClass: "info-dialog",
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        //that.onClickBanner(bannerDetail.companyId, bannerDetail.url);
                        if (bannerDetail.linkType == '2') {
                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_company_profile_company_profile__["a" /* CompanyProfilePage */], { companyId: bannerDetail.companyId });
                        }
                        else {
                            _this.iab.create(bannerDetail.link, '_system');
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WizardLayout2.prototype, "data", void 0);
    WizardLayout2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'wizard-layout-2',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/wizard/layout-2/wizard.html"*/'<ion-navbar>\n\n    <button ion-button menuToggle>\n\n        <ion-icon class="icon-menu" name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title text-uppercase></ion-title>\n\n    <ion-buttons end>\n\n        <button ion-button icon-only (click)="openLogin()">\n\n            <ion-icon name="person"></ion-icon>\n\n        </button>\n\n        <button ion-button icon-only padding-left (click)="onClickLanguage(1)">\n\n            <img src="assets/images/flag/en-flag-active.jpg" style="height: 15px;" alt="" *ngIf="lang == \'1\'">\n\n            <img src="assets/images/flag/en-flag.jpg" style="height: 15px;" alt="" *ngIf="lang == \'2\'">\n\n        </button>\n\n        <button style="padding-left: 5px;" ion-button icon-only (click)="onClickLanguage(2)">\n\n            <img src="assets/images/flag/thai-flag-active.jpg" style="height: 15px;" alt="" *ngIf="lang == \'2\'">\n\n            <img src="assets/images/flag/thai-flag.jpg" style="height: 15px;" alt="" *ngIf="lang == \'1\'">\n\n        </button>\n\n    </ion-buttons>\n\n</ion-navbar>\n\n<ion-header-bar custom-subheader class="bar-subheader bar-stable">\n\n    <ion-slides pager="true" autoplay="5000" loop="true" speed="500" *ngIf="!banners">\n\n        <ion-slide background-size>\n\n            <img src="assets/images/gallery/banner3.png"\n\n                style="width: -webkit-fill-available; height: -webkit-fill-available;">\n\n        </ion-slide>\n\n    </ion-slides>\n\n    <ion-slides pager="true" autoplay="5000" loop="true" speed="500" *ngIf="banners">\n\n        <ion-slide background-size *ngFor="let img of banners.listBanner">\n\n            <img [src]="img.imageUrl" style="width: fit-content;" height="100px" (click)="presentAlert(img)">\n\n        </ion-slide>\n\n    </ion-slides>\n\n</ion-header-bar>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/wizard/layout-2/wizard.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */]])
    ], WizardLayout2);
    return WizardLayout2;
}());

//# sourceMappingURL=wizard-layout-2.js.map

/***/ }),

/***/ 501:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppearanceAnimationLayout2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppearanceAnimationLayout2 = /** @class */ (function () {
    function AppearanceAnimationLayout2(rest, ev, navCtrl) {
        var _this = this;
        this.rest = rest;
        this.ev = ev;
        this.navCtrl = navCtrl;
        this.imgUrl = "assets/images/company-logo/mockup-comp-logo.jpg";
        this.animateItems = [];
        this.searchTerm = '';
        this.allData = []; //Store all data from provider
        this.filterData = []; //Store filtered data
        ev.subscribe('changeCountry:changed', function () {
            // user and time are the same arguments passed in `events.publish(user, time)`
            _this.filterData = [];
            _this.allData = [];
            _this.animateItems = [];
            _this.getCompanyList();
        });
        if (this.rest.currentCountry) {
            this.countryName = this.rest.currentCountry.countryName;
        }
        else {
            this.countryName = "THAILAND";
        }
        //
        this.animateClass = { 'fade-in-right-item': true };
        //this.getCompanyList();
        console.log(this.data);
    }
    AppearanceAnimationLayout2.prototype.onEvent = function (event, item, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    AppearanceAnimationLayout2.prototype.ngOnChanges = function (changes) {
        var that = this;
        this.menuId = this.data.menuId;
        this.rest.getListCompanyByProductType(that.menuId).then(function (result) {
            if (result) {
                result.anpc_profile_url = that.imgUrl;
                that.data = changes['data'].currentValue;
                that.data = result;
                if (result) {
                    var _loop_1 = function (i) {
                        setTimeout(function () {
                            that.animateItems.push(result[i]);
                        }, 200 * i);
                    };
                    for (var i = 0; i < result.length; i++) {
                        _loop_1(i);
                    }
                    that.allData = that.animateItems;
                    that.filterData = that.allData;
                    console.log(that.animateItems);
                }
            }
            else {
                console.log("wrong");
            }
        });
    };
    AppearanceAnimationLayout2.prototype.ngAfterViewInit = function () {
        //this.content.ionScroll.subscribe((d) => {
        //    this.fabButton.setElementClass("fab-button-out", d.directionY == "down");
        //});
    };
    AppearanceAnimationLayout2.prototype.getCompanyList = function () {
        var that = this;
        this.rest.getListCompanyByProductType(that.menuId).then(function (result) {
            if (result) {
                result.anpc_profile_url = that.imgUrl;
                that.data = result;
                if (result) {
                    var _loop_2 = function (i) {
                        setTimeout(function () {
                            that.animateItems.push(result[i]);
                        }, 200 * i);
                    };
                    for (var i = 0; i < result.length; i++) {
                        _loop_2(i);
                    }
                    that.allData = that.animateItems;
                    that.filterData = that.allData;
                    console.log(that.animateItems);
                }
            }
            else {
                console.log("wrong");
            }
        });
    };
    AppearanceAnimationLayout2.prototype.filterItems = function () {
        var that = this;
        return this.animateItems.filter(function (item) {
            return item.alpc_company_name.toLowerCase().indexOf(that.searchTerm.toLowerCase()) > -1;
        });
    };
    AppearanceAnimationLayout2.prototype.setFilteredList = function () {
        var _this = this;
        this.filterData = this.allData.filter(function (item) {
            return item.alpc_company_name.toLowerCase().indexOf(_this.searchTerm.toLowerCase()) > -1;
        });
    };
    AppearanceAnimationLayout2.prototype.inputFilter = function () {
        this.allData = this.animateItems;
        this.filterData = this.allData;
        //this.animateItems = this.filterItems();
    };
    AppearanceAnimationLayout2.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            for (var i = 0; i < 30; i++) {
                _this.filterData.push("a");
            }
            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 500);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], AppearanceAnimationLayout2.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], AppearanceAnimationLayout2.prototype, "events", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Content */])
    ], AppearanceAnimationLayout2.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* FabButton */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* FabButton */])
    ], AppearanceAnimationLayout2.prototype, "fabButton", void 0);
    AppearanceAnimationLayout2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'appearance-animation-layout-2',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/list-view/appearance-animation/layout-2/appearance-animation.html"*/'<!--Theme Appearance animation (Fade In Right)-->\n\n<ion-content>\n\n  <ion-grid no-padding *ngIf="data != null">\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <ion-list no-margin blur sticky>\n\n          <ion-list-header no-padding no-margin transparent>\n\n            <ion-searchbar [(ngModel)]="searchTerm" (ionInput)="setFilteredList()"></ion-searchbar>\n\n          </ion-list-header>\n\n          <ion-item text-warp [ngClass]="animateClass" *ngFor="let item of filterData; let i = index;"\n\n            (click)="onEvent(\'onItemClick\', item.anpc_id, $event)">\n\n            <ion-thumbnail item-start>\n\n              <img [src]="item.anpc_profile_url" [alt]="item.alpc_company_name" />\n\n            </ion-thumbnail>\n\n            <h2 item-title padding-left>{{item.alpc_company_name}}</h2>\n\n            <h2 style="color:red;" padding-left></h2>\n\n            <ion-icon icon-small item-end (click)="onEvent(\'onFavorite\', item, $event)">\n\n              <i class="icon" [ngClass]="{\'icon-heart\': item.favorite, \'icon-heart-outline\': !item.favorite}"></i>\n\n            </ion-icon>\n\n          </ion-item>\n\n        </ion-list>\n\n\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/list-view/appearance-animation/layout-2/appearance-animation.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]])
    ], AppearanceAnimationLayout2);
    return AppearanceAnimationLayout2;
}());

//# sourceMappingURL=appearance-animation-layout-2.js.map

/***/ }),

/***/ 502:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginLayout1; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_profile_profile__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_unique_device_id__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginLayout1 = /** @class */ (function () {
    function LoginLayout1(navCtrl, rest, eventsTrigger, uniqueDeviceID, plt) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.rest = rest;
        this.eventsTrigger = eventsTrigger;
        this.uniqueDeviceID = uniqueDeviceID;
        this.plt = plt;
        this.isUsernameValid = true;
        this.isPasswordValid = true;
        this.wrongUsernameOrPassword = false;
        this.onEvent = function (event) {
            if (event == "onLogin" && !_this.validate()) {
                return;
            }
            if (_this.events[event]) {
                _this.events[event]({
                    'username': _this.username,
                    'password': _this.password
                });
            }
        };
        var that = this;
        if (this.plt.is('cordova')) {
            this.uniqueDeviceID.get()
                .then(function (uuid) {
                return that.uuid = uuid;
            })
                .catch(function (error) { return console.log(error); });
        }
        else {
            this.uuid = 'browser';
        }
    }
    LoginLayout1.prototype.validate = function () {
        this.isUsernameValid = true;
        this.isPasswordValid = true;
        if (!this.username || this.username.length == 0) {
            this.isUsernameValid = false;
        }
        if (!this.password || this.password.length == 0) {
            this.isPasswordValid = false;
        }
        return this.isPasswordValid && this.isUsernameValid;
    };
    LoginLayout1.prototype.clickLogin = function () {
        var _this = this;
        var that = this;
        if (this.validate()) {
            if (!this.uuid) {
                this.uuid = 'browser';
            }
            //this.username = 'user01'
            //this.password = 'test1'
            this.rest.login(this.username, this.password, this.uuid).then(function (result) {
                if (result.aau_status == 0 || result.aau_status == 1) {
                    that.wrong = result.msg;
                    _this.wrongUsernameOrPassword = true;
                }
                else {
                    _this.responseUser = result;
                    if (_this.responseUser) {
                        that.eventsTrigger.publish('login:changed');
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_profile_profile__["a" /* ProfilePage */]);
                    }
                }
            }, function (err) {
                // Error log
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], LoginLayout1.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], LoginLayout1.prototype, "events", void 0);
    LoginLayout1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'login-layout-1',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/login/layout-1/login.html"*/'<!-- Themes Login + logo -->\n\n<ion-content style="background-color: transparent;"> \n\n  \n\n    <ion-grid *ngIf="data != null" style="background-color: transparent;">\n\n      <ion-row wrap padding>\n\n        <ion-col col-12 col-sm-12 col-md-12 offset-lg-3 col-lg-6 offset-xl-3 col-xl-6>\n\n          <button ion-button button-clear clear (click)="onEvent(\'onSkip\')">{{data.skip}}</button>\n\n          <!---Logo-->\n\n          <ion-thumbnail>\n\n            <img padding-bottom [src]="data.logo">\n\n          </ion-thumbnail>\n\n          <!---Input field username-->\n\n          <ion-item style="background-color: transparent;">\n\n            <ion-label color="secondary" floating>{{data.username}}</ion-label>\n\n            <ion-input required type="text" [(ngModel)]="username"></ion-input>\n\n            <ion-label error-field color="secondary" no-margin *ngIf="!isUsernameValid">{{data.errorUser}}</ion-label>\n\n          </ion-item>\n\n          <!---Input field password-->\n\n          <ion-item style="background-color: transparent;">\n\n            <ion-label color="secondary" floating>{{data.password}}</ion-label>\n\n            <ion-input required type="password" [(ngModel)]="password"></ion-input>\n\n            <ion-label error-field color="secondary" no-margin *ngIf="!isPasswordValid">{{data.errorPassword}}\n\n            </ion-label>\n\n          </ion-item>\n\n          <ion-label error-field style="color:#ff0000" no-margin *ngIf="wrongUsernameOrPassword">{{wrong}}\n\n          </ion-label>\n\n          <!---Login button-->\n\n          <button ion-button float-right default-button (click)="clickLogin()">{{data.login}}</button>\n\n          <!---Register button-->\n\n          <button ion-button float-right default-button (click)="onEvent(\'onRegister\')">{{data.register}}</button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/login/layout-1/login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_unique_device_id__["a" /* UniqueDeviceID */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */]])
    ], LoginLayout1);
    return LoginLayout1;
}());

//# sourceMappingURL=login-layout-1.js.map

/***/ }),

/***/ 503:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCompanyLayout; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer_ngx__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AddCompanyLayout = /** @class */ (function () {
    function AddCompanyLayout(camera, rest, alertCtrl, transfer, loadingCtrl, toastCtrl, platform) {
        this.camera = camera;
        this.rest = rest;
        this.alertCtrl = alertCtrl;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.slider = {};
        this.photos = [];
        this.options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            saveToPhotoAlbum: true,
        };
        this.item = {
            'description': '',
            'stars': 1,
            'imageUrl': ''
        };
        this.lastImage = null;
        this.base64img = '';
    }
    AddCompanyLayout.prototype.openGallery = function () {
        var _this = this;
        var cameraOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.FILE_URI,
            quality: 100,
            targetWidth: 1000,
            targetHeight: 1000,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
        };
        var that = this;
        // Get the data of an image
        this.camera.getPicture(cameraOptions).then(function (imagePath) {
            // Special handling for Android library
            that.imageURI = imagePath;
            that.uploadFile();
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    AddCompanyLayout.prototype.uploadFile = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Uploading..."
        });
        loader.present();
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'ionicfile',
            fileName: 'ionicfile',
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'imageProfile': 'test' }
        };
        fileTransfer.upload(this.imageURI, 'http://fullsnacks.in/ABC-API/uploadImage.php', options)
            .then(function (data) {
            console.log(data + " Uploaded Successfully");
            _this.imageFileName = data;
            loader.dismiss();
            _this.presentToast("Image uploaded successfully");
        }, function (err) {
            console.log("UploarFailed : " + err);
            loader.dismiss();
            _this.presentToast(err);
        });
    };
    AddCompanyLayout.prototype.uploadPic = function () {
        var loader = this.loadingCtrl.create({
            content: "Uploading...."
        });
        loader.present();
        this.imageCapturedGallery();
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: "photo",
            fileName: "test3.jpg",
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {}
        };
        fileTransfer.upload(this.base64img, 'http://localhost/ABC-API/upload.php', options).then(function (data) {
            alert(JSON.stringify(data));
            loader.dismiss();
        }, function (error) {
            alert("error");
            alert("error" + JSON.stringify(error));
            loader.dismiss();
        });
    };
    AddCompanyLayout.prototype.imageCapturedGallery = function () {
        var _this = this;
        var that = this;
        var options = {
            quality: 70,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: false
        };
        this.camera.getPicture(options).then((function (ImageData) {
            _this.base64img = "data:image/jpeg;base64," + ImageData;
            that.uploadPic();
        }), function (error) {
            console.log(error);
        });
    };
    AddCompanyLayout.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    AddCompanyLayout.prototype.addPhoto = function (photo) {
        this.photos.push(photo);
        this.photos.reverse();
    };
    AddCompanyLayout.prototype.displayErrorAlert = function (err) {
        console.log(err);
        var alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Error while trying to capture picture',
            buttons: ['OK']
        });
        alert.present();
    };
    AddCompanyLayout.prototype.onClickEvent = function (index) {
        if (this.slider[index]) {
            this.slider[index].slidePrev(300);
        }
    };
    AddCompanyLayout.prototype.onEvent = function (event, item, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    AddCompanyLayout.prototype.ngOnChanges = function (changes) {
        var that = this;
        this.rest.getCompanyProfile(that.data.companyId).then(function (result) {
            if (result) {
                that.companyData = result;
                that.hiringData = result.company_info.looking_for;
                that.departmentContactData = result.company_info.department_contact;
            }
            else {
                console.log("wrong");
            }
        });
    };
    AddCompanyLayout.prototype.next = function () {
        this.slides.slideNext();
    };
    AddCompanyLayout.prototype.prev = function () {
        this.slides.slidePrev();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], AddCompanyLayout.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], AddCompanyLayout.prototype, "events", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Content */]),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('slides'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* Slides */])
    ], AddCompanyLayout.prototype, "slides", void 0);
    AddCompanyLayout = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'add-company-layout',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/profile/add-company/add-company-layout.html"*/'<!--Profile- Profile 1-->\n\n<ion-content background-size [ngStyle]="{\'background-image\': \'url(\' + data.headerImage + \')\'}">\n\n\n\n\n\n  <ion-slides pager="false" [options]="slideOpts" #slides>\n\n    <ion-slide>\n\n      <ion-grid *ngIf="data != null" style="background-color: transparent;">\n\n        <ion-row wrap padding style="background-color : rgba(247, 247, 247, 0.19);">\n\n          <ion-col col-12 col-sm-12 col-md-12 offset-lg-3 col-lg-6 offset-xl-3 col-xl-6>\n\n            <!---Input field username-->\n\n            <div style="font-size: 30px;">\n\n              <ion-label color="secondary">English Language</ion-label>\n\n            </div>\n\n            <ion-item>\n\n              <h4>Image Preview</h4>\n\n              <img src="{{imageFileName}}" *ngIf="imageFileName" alt="Ionic File" width="300" />\n\n            </ion-item>\n\n            <p>{{imageURI}}</p>\n\n            <button ion-button default-button full text-capitalize (click)="imageCapturedGallery()">\n\n              Choose Picture\n\n            </button>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Company Name</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Company Country</ion-label>\n\n              <ion-select>\n\n                <ion-option value="TH">Thailand</ion-option>\n\n                <ion-option value="CH">China</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Business Type</ion-label>\n\n              <ion-select>\n\n                <ion-option value="a">Automotive</ion-option>\n\n                <ion-option value="b">Packaging</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Company Information</ion-label>\n\n              <ion-textarea [(ngModel)]="description"></ion-textarea>\n\n            </ion-item>\n\n            <div style="background-color: #fcd736;">\n\n              <ion-label color="primary">Looking For</ion-label>\n\n            </div>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary">Employee</ion-label>\n\n              <ion-checkbox></ion-checkbox>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Employee Requrements</ion-label>\n\n              <ion-textarea [(ngModel)]="description"></ion-textarea>\n\n            </ion-item>\n\n            <br>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary">Partner / Investment</ion-label>\n\n              <ion-checkbox></ion-checkbox>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Partner/ Investment Requrements</ion-label>\n\n              <ion-textarea [(ngModel)]="description"></ion-textarea>\n\n            </ion-item>\n\n            <br>\n\n            <div style="background-color: #fcd736;">\n\n              <ion-label color="primary">Contact</ion-label>\n\n            </div>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Address</ion-label>\n\n              <ion-textarea [(ngModel)]="description"></ion-textarea>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>State</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>City</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Postal Code</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Telephone</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Email</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Website</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <div style="background-color: #fcd736;">\n\n              <ion-label color="primary">Department Contacts</ion-label>\n\n            </div>\n\n            <br>\n\n            <div>\n\n              <ion-label color="secondary">Purchase Department</ion-label>\n\n            </div>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Name</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Telephone</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Email</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <br>\n\n            <div>\n\n              <ion-label color="secondary">Sales Department</ion-label>\n\n            </div>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Name</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Telephone</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Email</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <br>\n\n            <div>\n\n              <ion-label color="secondary">Human Resource Department</ion-label>\n\n            </div>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Name</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Telephone</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Email</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n\n\n            <button ion-button float-right default-button (click)="onEvent(\'onLogin\')">Save</button>\n\n\n\n            <button ion-button float-right default-button (click)="next()">Input Thai Language</button>\n\n\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n    </ion-slide>\n\n    <ion-slide>\n\n      <ion-grid *ngIf="data != null" style="background-color: transparent;">\n\n        <ion-row wrap padding style="background-color : rgba(247, 247, 247, 0.19);">\n\n          <ion-col col-12 col-sm-12 col-md-12 offset-lg-3 col-lg-6 offset-xl-3 col-xl-6>\n\n            <div style="font-size: 30px;">\n\n              <ion-label color="secondary">Thai Language</ion-label>\n\n            </div>\n\n            <!---Input field username-->\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Company Name</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Company Country</ion-label>\n\n              <ion-select>\n\n                <ion-option value="TH">Thailand</ion-option>\n\n                <ion-option value="CH">China</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Business Type</ion-label>\n\n              <ion-select>\n\n                <ion-option value="a">Automotive</ion-option>\n\n                <ion-option value="b">Packaging</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Company Information</ion-label>\n\n              <ion-textarea [(ngModel)]="description"></ion-textarea>\n\n            </ion-item>\n\n            <div style="background-color: #fcd736;">\n\n              <ion-label color="primary">Looking For</ion-label>\n\n            </div>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary">Employee</ion-label>\n\n              <ion-checkbox></ion-checkbox>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Employee Requrements</ion-label>\n\n              <ion-textarea [(ngModel)]="description"></ion-textarea>\n\n            </ion-item>\n\n            <br>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary">Partner / Investment</ion-label>\n\n              <ion-checkbox></ion-checkbox>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Partner/ Investment Requrements</ion-label>\n\n              <ion-textarea [(ngModel)]="description"></ion-textarea>\n\n            </ion-item>\n\n            <br>\n\n            <div style="background-color: #fcd736;">\n\n              <ion-label color="primary">Contact</ion-label>\n\n            </div>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Address</ion-label>\n\n              <ion-textarea [(ngModel)]="description"></ion-textarea>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>State</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>City</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Postal Code</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Telephone</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Email</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Website</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <div style="background-color: #fcd736;">\n\n              <ion-label color="primary">Department Contacts</ion-label>\n\n            </div>\n\n            <br>\n\n            <div>\n\n              <ion-label color="secondary">Purchase Department</ion-label>\n\n            </div>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Name</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Telephone</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Email</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <br>\n\n            <div>\n\n              <ion-label color="secondary">Sales Department</ion-label>\n\n            </div>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Name</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Telephone</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Email</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <br>\n\n            <div>\n\n              <ion-label color="secondary">Human Resource Department</ion-label>\n\n            </div>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Name</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Telephone</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n            <ion-item class="bg-trans">\n\n              <ion-label color="secondary" floating>Email</ion-label>\n\n              <ion-input required type="text" [(ngModel)]="companyNameEng"></ion-input>\n\n            </ion-item>\n\n\n\n            <button ion-button float-right default-button (click)="onEvent(\'onLogin\')">Save</button>\n\n            <button ion-button float-right default-button (click)="prev()">Input English Language</button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n    </ion-slide>\n\n  </ion-slides>\n\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/profile/add-company/add-company-layout.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer_ngx__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */]])
    ], AddCompanyLayout);
    return AddCompanyLayout;
}());

//# sourceMappingURL=add-company-layout.js.map

/***/ }),

/***/ 504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProfileLayout; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserProfileLayout = /** @class */ (function () {
    function UserProfileLayout() {
        this.slider = {};
        this.bgImage = 'assets/images/background/29.jpg';
    }
    UserProfileLayout.prototype.slideHasChanged = function (slider, index) {
        this.slider[index] = slider;
        if (2 == slider._activeIndex) {
            if (this.data.items) {
                this.data.items.splice(index, 1);
            }
            else {
                this.data.splice(index, 1);
            }
        }
    };
    UserProfileLayout.prototype.onClickEvent = function (index) {
        if (this.slider[index]) {
            this.slider[index].slidePrev(300);
        }
    };
    UserProfileLayout.prototype.onEvent = function (event, item, e) {
        console.log("click");
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], UserProfileLayout.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], UserProfileLayout.prototype, "events", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Content */])
    ], UserProfileLayout.prototype, "content", void 0);
    UserProfileLayout = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'user-profile',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/profile/user-profile/user-profile.html"*/'<!--Profile- Profile 1-->\n\n<ion-content *ngIf="data != null" background-size [ngStyle]="{\'background-image\': \'url(\' + bgImage + \')\'}">\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <ion-card transparent text-center>\n\n          <ion-item transparent text-center>\n\n            <h1 style="white-space: normal;" ion-text color="secondary">{{data.name}}</h1>\n\n            <br>\n\n            <p text-wrap ion-text color="secondary">User Type : {{data.type}}</p>\n\n            <p text-wrap ion-text color="secondary">Status : {{data.status}}</p>\n\n            <p text-wrap ion-text color="secondary">Subscribe : {{data.member}} </p>\n\n            <p text-wrap ion-text color="secondary"><ion-icon (click)="onEvent(\'onClickPayment\')" icon-small name="card" item-start><u>Upgrade to Premium User</u></ion-icon></p>\n\n          </ion-item>\n\n          <ion-row no-padding>\n\n            <!-- Info Author-->\n\n            <ion-col col-12 text-left>\n\n              <ion-item transparent>\n\n\n\n                <h1 padding-top item-title>Information</h1>\n\n              </ion-item>\n\n\n\n              <ion-item no-lines transparent>\n\n                <ion-icon icon-medium name="ios-phone-portrait" item-start></ion-icon>\n\n                <h2 subitem-title ion-text color="secondary">{{data.phone}}</h2>\n\n              </ion-item>\n\n              <ion-item no-lines transparent>\n\n                <ion-icon icon-medium name="mail-open" item-start></ion-icon>\n\n                <h2 subitem-title ion-text color="secondary">{{data.email}}</h2>\n\n              </ion-item>\n\n              <ion-item no-lines transparent>\n\n                <ion-icon icon-medium name="globe" item-start></ion-icon>\n\n                <h2 subitem-title ion-text color="secondary">{{data.country}}</h2>\n\n              </ion-item>\n\n              <ion-item no-lines transparent>\n\n                <ion-icon icon-medium name="briefcase" item-start></ion-icon>\n\n                <h2 subitem-title ion-text color="secondary">{{data.bussinessField}}</h2>\n\n              </ion-item>\n\n              <ion-item no-lines transparent>\n\n                <ion-icon icon-medium name="briefcase" item-start></ion-icon>\n\n                <h2 subitem-title ion-text color="secondary">{{data.profession}}</h2>\n\n              </ion-item>\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/profile/user-profile/user-profile.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], UserProfileLayout);
    return UserProfileLayout;
}());

//# sourceMappingURL=user-profile.js.map

/***/ }),

/***/ 505:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileLayout5; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number_ngx__ = __webpack_require__(371);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfileLayout5 = /** @class */ (function () {
    function ProfileLayout5(rest, callNumber) {
        this.rest = rest;
        this.callNumber = callNumber;
        this.slider = {};
        this.isPremium = true;
        if (rest.currentUser) {
            if (rest.currentUser.member == 'Premium User') {
                //alert("PRE")
                this.isPremium = true;
            }
        }
    }
    ProfileLayout5.prototype.phoneCall = function (number) {
        console.log("call");
        // this.callNumber.callNumber(number, true)
        //     .then(res => console.log('Launched dialer!', res))
        //     .catch(err => console.log('Error launching dialer', err));
        setTimeout(function () {
            var tel = '12345678890';
            window.open("tel:" + number, '_system');
        }, 100);
    };
    ProfileLayout5.prototype.onClickEvent = function (index) {
        if (this.slider[index]) {
            this.slider[index].slidePrev(300);
        }
    };
    ProfileLayout5.prototype.onEvent = function (event, item, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    ProfileLayout5.prototype.ngOnChanges = function (changes) {
        var that = this;
        this.rest.getCompanyProfile(that.data.companyId).then(function (result) {
            if (result) {
                that.companyData = result;
                that.hiringData = result.company_info.looking_for;
                that.departmentContactData = result.company_info.department_contact;
                that.companyImg = result.company_info.imgURL;
            }
            else {
                console.log("wrong");
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout5.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout5.prototype, "events", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Content */])
    ], ProfileLayout5.prototype, "content", void 0);
    ProfileLayout5 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'profile-layout-5',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/profile/layout-5/profile.html"*/'<!--Profile- Profile 1-->\n\n<ion-content *ngIf="data != null && companyData != null" background-size\n\n  style="background-color: #000 !important">\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <ion-card transparent text-center>\n\n          <ion-item transparent text-center>\n\n            <ion-avatar>\n\n              <img [src]="companyData.company_info.company_info.anpc_profile_url" alt="">\n\n            </ion-avatar>\n\n            <h1 style="white-space: normal;" ion-text color="secondary">\n\n              {{companyData.company_info.company_info.alpc_company_name}}</h1>\n\n            <br>\n\n            <p text-wrap ion-text color="secondary">{{companyData.company_info.company_info.alpt_product_type_name}}</p>\n\n          </ion-item>\n\n          <ion-row no-padding>\n\n            <!-- Info Author-->\n\n            <ion-col col-12 text-left *ngFor="let item of data.items;let i = index">\n\n              <ion-item transparent style="margin-bottom: -20px;">\n\n                <h1 item-title ion-text>{{companyData.title_name.almpc_menu_CompanyInformation}}</h1>\n\n                <p item-subtitle padding-top text-wrap style="color: #ffffff !important">{{companyData.company_info.company_info.alpc_company_info}}</p>\n\n\n\n              </ion-item>\n\n              <ion-item transparent style="margin-bottom: -20px;">\n\n                <h1 padding-top item-title>Product & Services</h1>\n\n              </ion-item>\n\n              <br>\n\n              <ion-row>\n\n                <ion-slides pager="true" autoplay="5000" loop="true" speed="500"\n\n                  *ngIf="companyImg && companyImg.length">\n\n                  <ion-slide background-size *ngFor="let img of companyImg">\n\n                    <div text-center>\n\n                      <img [src]="img.imgUrl" style="width: fit-content;" height="100px">\n\n                      <br>\n\n                      <h1 padding-top item-title >{{companyData.title_name.almpc_menu_CompanyInformation}}</h1>\n\n                      <br>\n\n                    </div>\n\n\n\n                  </ion-slide>\n\n                </ion-slides>\n\n              </ion-row>\n\n              <ion-item transparent style="margin-bottom: -20px;">\n\n                <h1 padding-top item-title>{{companyData.title_name.almpc_menu_Hiring}}</h1>\n\n              </ion-item>\n\n              <div (click)="onEvent(\'clickRegister\', item, $event)">\n\n                <div class="watermarked" *ngIf="!isPremium ==1" tappable>\n\n                  <!-- <h2 [ngClass]="!isPremium ? \'blur\': \'XXX\'" class="wt-text"><b>members only! Please sign up</b></h2> -->\n\n                </div>\n\n                <ion-item no-lines transparent [ngClass]="!isPremium ? \'blur\': \'XXX\'" *ngFor="let hiring of hiringData">\n\n                  <ion-icon style="color:green; font-size: 32px !important;" name="checkmark" item-start></ion-icon>\n\n                  <h2 [ngClass]="!isPremium ? \'blur\': \'XXX\'" subitem-title ion-text color="secondary">\n\n                    {{hiring.allf_looking_for_name}}</h2>\n\n                  <p padding-top text-wrap style="color: #ffffff !important">{{hiring.amlf_description}} XX</p>\n\n                </ion-item>\n\n\n\n                <ion-item transparent style="margin-bottom: -30px;">\n\n                  <h1 padding-top item-title>{{companyData.title_name.almpc_menu_DepartmentContacts}}</h1>\n\n                  <div *ngFor="let deptContact of departmentContactData">\n\n                    <h2 padding-top item-title style="margin-top: -5px;" *ngIf="deptContact.andct_isDesc != 1">\n\n                      {{deptContact.aldct_department_name}}</h2>\n\n                    <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'" style="margin-top: -5px;" *ngIf="deptContact.andct_isDesc != 1">\n\n                      <h2 subitem-title ion-text color="secondary">{{companyData.title_name.almpc_menu_ContactPerson}}\n\n                      </h2>\n\n                      <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'"\n\n                        style="margin-top: -5px;">\n\n                        <h2 item-subtitle ion-text style="color: #ffffff !important" item-start>\n\n                          {{companyData.title_name.almpc_field_Name}} : </h2>\n\n                        <h2 [ngClass]="!isPremium ? \'blur\': \'XXX\'" item-subtitle ion-text style="color: #ffffff !important">\n\n                          {{deptContact.almdc_name}}</h2>\n\n                      </ion-item>\n\n                      <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'"\n\n                        style="margin-top: -30px;" (click)="phoneCall(deptContact.almdc_telephone)">\n\n                        <h2 item-subtitle ion-text style="color: #ffffff !important" item-start>\n\n                          {{companyData.title_name.almpc_field_Number}} : </h2>\n\n                        <h2 [ngClass]="!isPremium ? \'blur\': \'XXX\'" item-subtitle ion-text style="color: #ffffff !important">\n\n                          {{deptContact.almdc_telephone}}</h2>\n\n                      </ion-item>\n\n                      <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'"\n\n                        style="margin-top: -30px;">\n\n                        <h2 item-subtitle ion-text style="color: #ffffff !important" item-start>\n\n                          {{companyData.title_name.almpc_field_Email}} : </h2>\n\n                        <h2 [ngClass]="!isPremium ? \'blur\': \'XXX\'" item-subtitle ion-text style="color: #ffffff !important">\n\n                          {{deptContact.almdc_email}}</h2>\n\n                      </ion-item>\n\n                    </ion-item>\n\n                    <ion-item no-lines transparent *ngIf="deptContact.andct_isDesc == 1" style="margin-top: -30px;">\n\n                      <h2 subitem-title ion-text style="color: #ffffff !important">Hiring</h2>\n\n                      <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'"\n\n                         (click)="phoneCall(deptContact.almdc_telephone)">\n\n                        <h2 item-subtitle ion-text style="color: #ffffff !important" item-start>\n\n                          {{deptContact.amlf_description}} Looking for Sales</h2>\n\n                      </ion-item>\n\n                    </ion-item>\n\n                  </div>\n\n                </ion-item>\n\n\n\n                <div class="watermarked" *ngIf="data.blur ==1">\n\n                  <h2 [ngClass]="!isPremium ? \'blur\': \'XXX\'" class="wt-text"><b>members only! Please sign up</b></h2>\n\n                </div>\n\n                <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'">\n\n                  <h1 padding-top item-title>{{companyData.title_name.almpc_menu_Contact}}</h1>\n\n                </ion-item>\n\n                <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'">\n\n                  <ion-icon icon-medium name="pin" item-start></ion-icon>\n\n                  <h2 [ngClass]="!isPremium ? \'blur\': \'XXX\'" style="white-space: normal;" subitem-title ion-text\n\n                    color="secondary">\n\n                    {{companyData.company_info.company_info.alpc_contract_address}}\n\n                    {{companyData.company_info.company_info.alpc_contract_city}}\n\n                    {{companyData.company_info.company_info.alpc_contract_state}}\n\n                    {{companyData.company_info.company_info.alpc_contract_postal_code}}</h2>\n\n                </ion-item>\n\n                <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'"\n\n                  (click)="phoneCall(companyData.company_info.company_info.alpc_contract_telephone)">\n\n                  <ion-icon icon-medium name="{{item.iconPhone}}" item-start></ion-icon>\n\n                  <h2 [ngClass]="!isPremium ? \'blur\': \'XXX\'" subitem-title ion-text color="secondary">\n\n                    {{companyData.company_info.company_info.alpc_contract_telephone}}</h2>\n\n                </ion-item>\n\n                <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'">\n\n                  <ion-icon icon-medium name="{{item.iconMail}}" item-start></ion-icon>\n\n                  <h2 [ngClass]="!isPremium ? \'blur\': \'XXX\'" subitem-title ion-text color="secondary">\n\n                    {{companyData.company_info.company_info.alpc_contract_email}}</h2>\n\n                </ion-item>\n\n                <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'">\n\n                  <ion-icon icon-medium name="{{item.iconGlobe}}" item-start></ion-icon>\n\n                  <h2 [ngClass]="!isPremium ? \'blur\': \'XXX\'" subitem-title ion-text color="secondary">\n\n                    {{companyData.company_info.company_info.alpc_contract_website}}</h2>\n\n                </ion-item>\n\n                <ion-item no-lines transparent [ngClass]="data.blur == 1 ? \'blur\': \'XXX\'">\n\n                  <ion-icon icon-medium name="locate" item-start></ion-icon>\n\n                  <button ion-button default-button text-center>LOCATION</button>\n\n                </ion-item>\n\n\n\n\n\n              </div>\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col col-12>\n\n\n\n              <agm-map [latitude]="13.59427" [longitude]="100.23209" [zoom]="15" [mapTypeControl]="true"\n\n                [streetViewControl]="true">\n\n                <agm-marker [latitude]="13.59427" [longitude]="100.23209"></agm-marker>\n\n              </agm-map>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/profile/layout-5/profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number_ngx__["a" /* CallNumber */]])
    ], ProfileLayout5);
    return ProfileLayout5;
}());

//# sourceMappingURL=profile-layout-5.js.map

/***/ }),

/***/ 506:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterLayout2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device_ngx__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_unique_device_id__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RegisterLayout2 = /** @class */ (function () {
    function RegisterLayout2(rest, device, alertCtrl, navCtrl, plt, uniqueDeviceID) {
        var _this = this;
        this.rest = rest;
        this.device = device;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.plt = plt;
        this.uniqueDeviceID = uniqueDeviceID;
        this.bgImage = 'assets/images/background/29.jpg';
        this.isEmailValid = true;
        this.isUsernameValid = true;
        this.isPasswordValid = true;
        this.isCityValid = true;
        this.isCountryValid = true;
        this.isNameValid = true;
        this.isTelValid = true;
        this.isBusinessFieldValid = true;
        this.isProfessionValid = true;
        this.isUserTypeValid = true;
        this.isAgree = true;
        this.regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        this.onEvent = function (event) {
            if (event == "onRegister" && !_this.validate()) {
                return;
            }
            if (_this.events[event]) {
                _this.events[event]({
                    'username': _this.username,
                    'password': _this.password,
                    'country': _this.country,
                    'city': _this.city,
                    'email': _this.email,
                    'name': _this.name,
                    'tel': _this.tel,
                    'businessField': _this.businessField,
                    'profession': _this.profession,
                    'userType': _this.userType
                });
            }
        };
        this.getAccoutType();
        this.getCountries();
        var that = this;
        if (this.plt.is('cordova')) {
            this.uniqueDeviceID.get()
                .then(function (uuid) {
                return that.uuid = uuid;
            })
                .catch(function (error) { return console.log(error); });
        }
    }
    RegisterLayout2.prototype.getAccoutType = function () {
        var that = this;
        this.rest.getAccoutType().then(function (result) {
            if (result) {
                that.accoutTypes = result.listAccountType;
                console.log(that.accoutTypes);
            }
            else {
                console.log("wrong");
            }
        });
    };
    RegisterLayout2.prototype.getCountries = function () {
        var that = this;
        this.rest.getCountries().then(function (result) {
            if (result) {
                that.countries = result.listCountry;
                console.log(that.countries);
            }
            else {
                console.log("wrong");
            }
        });
    };
    RegisterLayout2.prototype.validate = function () {
        this.isEmailValid = true;
        this.isUsernameValid = true;
        this.isPasswordValid = true;
        this.isCityValid = true;
        this.isCountryValid = true;
        this.isNameValid = true;
        this.isTelValid = true;
        this.isBusinessFieldValid = true;
        this.isProfessionValid = true;
        this.isUserTypeValid = true;
        this.isAgree = true;
        if (!this.username || this.username.length == 0) {
            this.isUsernameValid = false;
        }
        if (!this.password || this.password.length == 0) {
            this.isPasswordValid = false;
        }
        if (!this.password || this.password.length == 0) {
            this.isPasswordValid = false;
        }
        if (!this.city || this.city.length == 0) {
            this.isCityValid = false;
        }
        if (!this.country || this.country.length == 0) {
            this.isCountryValid = false;
        }
        if (!this.name || this.name.length == 0) {
            this.isNameValid = false;
        }
        if (!this.tel || this.tel.length == 0) {
            this.isTelValid = false;
        }
        if (!this.businessField || this.businessField.length == 0) {
            this.isBusinessFieldValid = false;
        }
        if (!this.profession || this.profession.length == 0) {
            this.isProfessionValid = false;
        }
        if (!this.userType || this.userType.length == 0) {
            this.isUserTypeValid = false;
        }
        if (!this.email || this.email.length == 0) {
            this.isEmailValid = false;
        }
        if (!this.agree || this.agree.length == 0) {
            this.isAgree = false;
        }
        //this.isEmailValid = this.regex.test(this.email);
        return this.isEmailValid &&
            this.isPasswordValid &&
            this.isUsernameValid &&
            this.isCityValid &&
            this.isCountryValid &&
            this.isNameValid &&
            this.isTelValid &&
            this.isBusinessFieldValid &&
            this.isProfessionValid &&
            this.isUserTypeValid &&
            this.isAgree;
    };
    RegisterLayout2.prototype.clickRegister = function () {
        var that = this;
        var Data = {
            username: this.username.replace(/'/g, "\\'"),
            password: this.password.replace(/'/g, "\\'"),
            name: this.name.replace(/'/g, "\\'"),
            email: this.email.replace(/'/g, "\\'"),
            country: this.country,
            city: this.city.replace(/'/g, "\\'"),
            phone: this.tel.replace(/'/g, "\\'"),
            bussinessField: this.businessField.replace(/'/g, "\\'"),
            profession: this.profession.replace(/'/g, "\\'"),
            UID: this.uuid,
            accountTypeId: this.userType
        };
        if (this.validate()) {
            this.rest.register(Data).then(function (result) {
                that.presentAlert();
            }, function (err) {
                // Error log
            });
        }
    };
    RegisterLayout2.prototype.presentAlert = function () {
        var _this = this;
        var that = this;
        var alert = this.alertCtrl.create({
            title: "Register completed",
            subTitle: "Please wait for Approval",
            cssClass: "info-dialog",
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
                    }
                }
            ]
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], RegisterLayout2.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], RegisterLayout2.prototype, "events", void 0);
    RegisterLayout2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'register-layout-2',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/register/layout-2/register.html"*/'<!-- Themes Register + Image -->\n\n<ion-content background-size [ngStyle]="{\'background-image\': \'url(\' + bgImage + \')\'}">\n\n  <ion-grid *ngIf="data != null" style="background-color: transparent;">\n\n      <ion-row wrap padding style="background-color : rgba(247, 247, 247, 0.19);">\n\n      <ion-col col-12 col-sm-12 col-md-12 offset-lg-3 col-lg-6 offset-xl-3 col-xl-6>\n\n        <!--Logo-->\n\n        <div padding-bottom class="logo">\n\n          <ion-thumbnail>\n\n            <img [src]="data.logo" style="height:100px;">\n\n          </ion-thumbnail>\n\n        </div>\n\n        <!--End logo-->\n\n        <!--Form-->\n\n        <div class="form">\n\n          <ion-item class="bg-trans">\n\n            <ion-label color="secondary" floating >User Type</ion-label>\n\n            <ion-select [(ngModel)]="userType">\n\n              <ion-option  *ngFor="let type of accoutTypes" [value]="type.aat_id">{{type.aat_name_type}}</ion-option>\n\n            </ion-select>\n\n            <ion-label error-field color="secondary" no-margin *ngIf="!isUserTypeValid">{{data.error}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bg-trans">\n\n            <!--Icon for username-->\n\n            <ion-icon item-left>\n\n              <i icon-small class="icon {{data.iconAccount}}"></i>\n\n            </ion-icon>\n\n            <!---Input field username-->\n\n            <ion-label floating>{{data.username}}</ion-label>\n\n            <ion-input required type="text" [(ngModel)]="username"></ion-input>\n\n            <ion-label error-field color="secondary" no-margin *ngIf="!isUsernameValid">{{data.errorUser}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bg-trans">\n\n            <!--Icon for password-->\n\n            <ion-icon item-left>\n\n              <i icon-small class="icon {{data.iconLock}}"></i>\n\n            </ion-icon>\n\n            <!---Input field password-->\n\n            <ion-label floating>{{data.password}}</ion-label>\n\n            <ion-input required type="password" [(ngModel)]="password"></ion-input>\n\n            <ion-label error-field color="secondary" no-margin *ngIf="!isPasswordValid">{{data.errorPassword}}\n\n            </ion-label>\n\n          </ion-item>\n\n          <ion-item class="bg-trans">\n\n            <!--Icon for name-->\n\n            <ion-icon item-left>\n\n              <i icon-small class="icon {{data.iconName}}"></i>\n\n            </ion-icon>\n\n            <!---Input field city-->\n\n            <ion-label floating>{{data.name}}</ion-label>\n\n            <ion-input required type="text" pattern="[a-zA-Z ]*" required [(ngModel)]="name"></ion-input>\n\n            <ion-label error-field *ngIf="!isNameValid">{{data.errorName}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bg-trans">\n\n            <!--Icon for email-->\n\n            <ion-icon item-left>\n\n              <i icon-small class="icon {{data.iconEmail}}"></i>\n\n            </ion-icon>\n\n            <!---Input field email-->\n\n            <ion-label floating>{{data.email}}</ion-label>\n\n            <ion-input required type="email" pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}"\n\n              [(ngModel)]="email"></ion-input>\n\n            <ion-label error-field *ngIf="!isEmailValid">{{data.errorEmail}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bg-trans">\n\n            <!--Icon for country-->\n\n            <ion-icon item-left>\n\n              <i icon-small class="icon {{data.iconWeb}}"></i>\n\n            </ion-icon>\n\n            <!---Input field country-->\n\n            <ion-label floating>{{data.country}}</ion-label>\n\n            <ion-select [(ngModel)]="country">\n\n                <ion-option  *ngFor="let country of countries" [value]="country.alml_nature_id">{{country.alml_country_name}}</ion-option>\n\n              </ion-select>\n\n            <ion-label error-field *ngIf="!isCountryValid">{{data.errorCountry}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bg-trans">\n\n            <!--Icon for city-->\n\n            <ion-icon item-left>\n\n              <i icon-small class="icon {{data.iconCity}}"></i>\n\n            </ion-icon>\n\n            <!---Input field city-->\n\n            <ion-label floating>{{data.city}}</ion-label>\n\n            <ion-input required type="text" pattern="[a-zA-Z ]*" required [(ngModel)]="city"></ion-input>\n\n            <ion-label error-field *ngIf="!isCityValid">{{data.errorCity}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bg-trans">\n\n            <!--Icon for Tel-->\n\n            <ion-icon item-left>\n\n              <i icon-small class="icon {{data.iconTel}}"></i>\n\n            </ion-icon>\n\n            <!---Input field Tel-->\n\n            <ion-label floating>{{data.tel}}</ion-label>\n\n            <ion-input required type="text" pattern="[a-zA-Z ]*" required [(ngModel)]="tel"></ion-input>\n\n            <ion-label error-field *ngIf="!isTelValid">{{data.errorTel}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bg-trans">\n\n            <!--Icon for Tel-->\n\n            <ion-icon item-left>\n\n              <i icon-small class="icon {{data.iconBag}}"></i>\n\n            </ion-icon>\n\n            <!---Input field Tel-->\n\n            <ion-label floating>{{data.businessField}}</ion-label>\n\n            <ion-input required type="text" pattern="[a-zA-Z ]*" required [(ngModel)]="businessField"></ion-input>\n\n            <ion-label error-field *ngIf="!isBusinessFieldValid">{{data.errorBusinessField}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bg-trans">\n\n            <!--Icon for Tel-->\n\n            <ion-icon item-left>\n\n              <i icon-small class="icon {{data.iconBag}}"></i>\n\n            </ion-icon>\n\n            <!---Input field Tel-->\n\n            <ion-label floating>{{data.profession}}</ion-label>\n\n            <ion-input required type="text" pattern="[a-zA-Z ]*" required [(ngModel)]="profession"></ion-input>\n\n            <ion-label error-field *ngIf="!isProfessionValid">{{data.errorProfession}}</ion-label>\n\n          </ion-item>\n\n          <br>\n\n          <button ion-button clear button-clear (click)="onEvent(\'onClickTerm\')"><u>Term &\n\n              Condition</u></button>\n\n          <div style="display: flex; align-items: center;" padding-left>\n\n            <ion-checkbox [(ngModel)]="agree" ></ion-checkbox>\n\n            <span padding-left text-capitalize ion-text color="secondary" text-uppercase>I\'m Agree with Term & Condition\n\n            </span>\n\n\n\n          </div>\n\n\n\n          <ion-item class="bg-trans">\n\n            <ion-label error-field *ngIf="!isAgree">{{data.errorAgree}}</ion-label>\n\n          </ion-item>\n\n          \n\n\n\n\n\n          <br>\n\n\n\n          <!---Register button-->\n\n          <button ion-button float-right clear button-clear (click)="clickRegister()">{{data.submit}}</button>\n\n        </div>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/register/layout-2/register.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_unique_device_id__["a" /* UniqueDeviceID */]])
    ], RegisterLayout2);
    return RegisterLayout2;
}());

//# sourceMappingURL=register-layout-2.js.map

/***/ }),

/***/ 507:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(508);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(622);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 622:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(867);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser_ngx__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_toast_service__ = __webpack_require__(869);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_loading_service__ = __webpack_require__(870);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common_http__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_camera__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_company_list_company_list__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_company_profile_company_profile__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_register_register__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_login_login__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_add_company_add_company__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_home_home_module__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_profile_profile__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_payment_modal_payment_modal__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_list_view_appearance_animation_layout_2_appearance_animation_layout_2__ = __webpack_require__(501);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_profile_layout_5_profile_layout_5__ = __webpack_require__(505);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_profile_user_profile_user_profile__ = __webpack_require__(504);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_profile_add_company_add_company_layout__ = __webpack_require__(503);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_register_layout_2_register_layout_2__ = __webpack_require__(506);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_login_layout_1_login_layout_1__ = __webpack_require__(502);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_wizard_layout_2_wizard_layout_2__ = __webpack_require__(500);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_barcode_scanner__ = __webpack_require__(871);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_rest_rest__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__agm_core__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_transfer_ngx__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_device_ngx__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_unique_device_id__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_call_number_ngx__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_native_in_app_purchase_ngx__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_storage__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_company_list_company_list__["a" /* CompanyListPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_company_profile_company_profile__["a" /* CompanyProfilePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_payment_modal_payment_modal__["a" /* PaymentModalPage */],
                __WEBPACK_IMPORTED_MODULE_20__components_profile_layout_5_profile_layout_5__["a" /* ProfileLayout5 */],
                __WEBPACK_IMPORTED_MODULE_15__pages_add_company_add_company__["a" /* AddCompanyPage */],
                __WEBPACK_IMPORTED_MODULE_23__components_register_layout_2_register_layout_2__["a" /* RegisterLayout2 */],
                __WEBPACK_IMPORTED_MODULE_24__components_login_layout_1_login_layout_1__["a" /* LoginLayout1 */],
                __WEBPACK_IMPORTED_MODULE_25__components_wizard_layout_2_wizard_layout_2__["a" /* WizardLayout2 */],
                __WEBPACK_IMPORTED_MODULE_21__components_profile_user_profile_user_profile__["a" /* UserProfileLayout */],
                __WEBPACK_IMPORTED_MODULE_22__components_profile_add_company_add_company_layout__["a" /* AddCompanyLayout */],
                __WEBPACK_IMPORTED_MODULE_19__components_list_view_appearance_animation_layout_2_appearance_animation_layout_2__["a" /* AppearanceAnimationLayout2 */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_16__pages_home_home_module__["HomePageModule"],
                __WEBPACK_IMPORTED_MODULE_34__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    platforms: {
                        ios: {
                            statusbarPadding: true
                        }
                    }
                }, {
                    links: [
                        { loadChildren: '../components/alert/layout-2/alert-layout-2.module#AlertLayout2Module', name: 'AlertLayout2', segment: 'alert-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/alert/layout-3/alert-layout-3.module#AlertLayout3Module', name: 'AlertLayout3', segment: 'alert-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/alert/layout-1/alert-layout-1.module#AlertLayout1Module', name: 'AlertLayout1', segment: 'alert-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/check-box/layout-2/check-box-layout-2.module#CheckBoxLayout2Module', name: 'CheckBoxLayout2', segment: 'check-box-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/check-box/layout-4/check-box-layout-4.module#CheckBoxLayout4Module', name: 'CheckBoxLayout4', segment: 'check-box-layout-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/check-box/layout-3/check-box-layout-3.module#CheckBoxLayout3Module', name: 'CheckBoxLayout3', segment: 'check-box-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/comment/layout-1/comment-layout-1.module#CommentLayout1Module', name: 'CommentLayout1', segment: 'comment-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/check-box/layout-1/check-box-layout-1.module#CheckBoxLayout1Module', name: 'CheckBoxLayout1', segment: 'check-box-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/comment/layout-2/comment-layout-2.module#CommentLayout2Module', name: 'CommentLayout2', segment: 'comment-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/forms/layout-1/form-layout-1.module#FormLayout1Module', name: 'FormLayout1', segment: 'form-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/forms/layout-2/form-layout-2.module#FormLayout2Module', name: 'FormLayout2', segment: 'form-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/forms/layout-3/form-layout-3.module#FormLayout3Module', name: 'FormLayout3', segment: 'form-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/full-screen-gallery/full-screen-gallery.module#FullScreenGalleryModule', name: 'FullScreenGallery', segment: 'full-screen-gallery', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/forms/layout-4/form-layout-4.module#FormLayout4Module', name: 'FormLayout4', segment: 'form-layout-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/image-gallery/layout-1/image-gallery-layout-1.module#ImageGalleryLayout1Module', name: 'ImageGalleryLayout1', segment: 'image-gallery-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/image-gallery/layout-2/image-gallery-layout-2.module#ImageGalleryLayout2Module', name: 'ImageGalleryLayout2', segment: 'image-gallery-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/appearance-animation/layout-1/appearance-animation-layout-1.module#AppearanceAnimationLayout1Module', name: 'AppearanceAnimationLayout1', segment: 'appearance-animation-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/appearance-animation/layout-3/appearance-animation-layout-3.module#AppearanceAnimationLayout3Module', name: 'AppearanceAnimationLayout3', segment: 'appearance-animation-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/appearance-animation/layout-2/appearance-animation-layout-2.module#AppearanceAnimationLayout2Module', name: 'AppearanceAnimationLayout2', segment: 'appearance-animation-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/image-gallery/layout-3/image-gallery-layout-3.module#ImageGalleryLayout3Module', name: 'ImageGalleryLayout3', segment: 'image-gallery-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/appearance-animation/layout-4/appearance-animation-layout-4.module#AppearanceAnimationLayout4Module', name: 'AppearanceAnimationLayout4', segment: 'appearance-animation-layout-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/appearance-animation/layout-5/appearance-animation-layout-5.module#AppearanceAnimationLayout5Module', name: 'AppearanceAnimationLayout5', segment: 'appearance-animation-layout-5', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/drag-and-drop/layout-1/drag-and-drop-layout-1.module#DragAndDropLayout1Module', name: 'DragAndDropLayout1', segment: 'drag-and-drop-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/drag-and-drop/layout-2/drag-and-drop-layout-2.module#DragAndDropLayout2Module', name: 'DragAndDropLayout2', segment: 'drag-and-drop-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/drag-and-drop/layout-3/drag-and-drop-layout-3.module#DragAndDropLayout3Module', name: 'DragAndDropLayout3', segment: 'drag-and-drop-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/drag-and-drop/layout-4/drag-and-drop-layout-4.module#DragAndDropLayout4Module', name: 'DragAndDropLayout4', segment: 'drag-and-drop-layout-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/expandable/layout-2/expandable-layout-2.module#ExpandableLayout2Module', name: 'ExpandableLayout2', segment: 'expandable-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/expandable/layout-1/expandable-layout-1.module#ExpandableLayout1Module', name: 'ExpandableLayout1', segment: 'expandable-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/expandable/layout-3/expandable-layout-3.module#ExpandableLayout3Module', name: 'ExpandableLayout3', segment: 'expandable-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/google-card/layout-3/google-card-layout-3.module#GoogleCardLayout3Module', name: 'GoogleCardLayout3', segment: 'google-card-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/google-card/layout-1/google-card-layout-1.module#GoogleCardLayout1Module', name: 'GoogleCardLayout1', segment: 'google-card-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/google-card/layout-2/google-card-layout-2.module#GoogleCardLayout2Module', name: 'GoogleCardLayout2', segment: 'google-card-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/google-card/layout-4/google-card-layout-4.module#GoogleCardLayout4Module', name: 'GoogleCardLayout4', segment: 'google-card-layout-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/sticky-list-header/layout-2/sticky-list-header-layout-2.module#StickyListHeaderLayout2Module', name: 'StickyListHeaderLayout2', segment: 'sticky-list-header-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/sticky-list-header/layout-3/sticky-list-header-layout-3.module#StickyListHeaderLayout3Module', name: 'StickyListHeaderLayout3', segment: 'sticky-list-header-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/sticky-list-header/layout-1/sticky-list-header-layout-1.module#StickyListHeaderLayout1Module', name: 'StickyListHeaderLayout1', segment: 'sticky-list-header-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/sticky-list-header/layout-4/sticky-list-header-layout-4.module#StickyListHeaderLayout4Module', name: 'StickyListHeaderLayout4', segment: 'sticky-list-header-layout-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/swipe-to-dismiss/layout-1/swipe-to-dismiss-layout-1.module#SwipeToDismissLayout1Module', name: 'SwipeToDismissLayout1', segment: 'swipe-to-dismiss-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/swipe-to-dismiss/layout-2/swipe-to-dismiss-layout-2.module#SwipeToDismissLayout2Module', name: 'SwipeToDismissLayout2', segment: 'swipe-to-dismiss-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/swipe-to-dismiss/layout-4/swipe-to-dismiss-layout-4.module#SwipeToDismissLayout4Module', name: 'SwipeToDismissLayout4', segment: 'swipe-to-dismiss-layout-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/list-view/swipe-to-dismiss/layout-3/swipe-to-dismiss-layout-3.module#SwipeToDismissLayout3Module', name: 'SwipeToDismissLayout3', segment: 'swipe-to-dismiss-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/login/layout-1/login-layout-1.module#LoginLayout1Module', name: 'LoginLayout1', segment: 'login-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/login/layout-2/login-layout-2.module#LoginLayout2Module', name: 'LoginLayout2', segment: 'login-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/maps/layout-2/maps-layout-2.module#MapsLayout2Module', name: 'MapsLayout2', segment: 'maps-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/maps/layout-1/maps-layout-1.module#MapsLayout1Module', name: 'MapsLayout1', segment: 'maps-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/parallax/layout-1/parallax-layout-1.module#ParallaxLayout1Module', name: 'ParallaxLayout1', segment: 'parallax-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/maps/layout-3/maps-layout-3.module#MapsLayout3Module', name: 'MapsLayout3', segment: 'maps-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/parallax/layout-2/parallax-layout-2.module#ParallaxLayout2Module', name: 'ParallaxLayout2', segment: 'parallax-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/payment/layout-1/payment-layout-1.module#PaymentLayout1Module', name: 'PaymentLayout1', segment: 'payment-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/parallax/layout-4/parallax-layout-4.module#ParallaxLayout4Module', name: 'ParallaxLayout4', segment: 'parallax-layout-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/parallax/layout-3/parallax-layout-3.module#ParallaxLayout3Module', name: 'ParallaxLayout3', segment: 'parallax-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/profile/layout-1/profile-layout-1.module#ProfileLayout1Module', name: 'ProfileLayout1', segment: 'profile-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/profile/add-company/add-company-layout.module#AddCompanyModule', name: 'AddCompanyLayout', segment: 'add-company-layout', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/profile/layout-2/profile-layout-2.module#ProfileLayout2Module', name: 'ProfileLayout2', segment: 'profile-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/profile/layout-3/profile-layout-3.module#ProfileLayout3Module', name: 'ProfileLayout3', segment: 'profile-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/profile/layout-4/profile-layout-4.module#ProfileLayout4Module', name: 'ProfileLayout4', segment: 'profile-layout-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/profile/user-profile/user-profile.module#UserProfileModule', name: 'UserProfileLayout', segment: 'user-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/profile/layout-5/profile-layout-5.module#ProfileLayout5Module', name: 'ProfileLayout5', segment: 'profile-layout-5', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/register/layout-1/register-layout-1.module#RegisterLayout1Module', name: 'RegisterLayout1', segment: 'register-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/register/layout-2/register-layout-2.module#RegisterLayout2Module', name: 'RegisterLayout2', segment: 'register-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/search-bar/layout-1/search-bar-layout-1.module#SearchBarLayout1Module', name: 'SearchBarLayout1', segment: 'search-bar-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/search-bar/layout-2/search-bar-layout-2.module#SearchBarLayout2Module', name: 'SearchBarLayout2', segment: 'search-bar-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/segment/layout-1/segment-layout-1.module#SegmentLayout1Module', name: 'SegmentLayout1', segment: 'segment-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/search-bar/layout-3/search-bar-layout-3.module#SearchBarLayout3Module', name: 'SearchBarLayout3', segment: 'search-bar-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/segment/layout-2/segment-layout-2.module#SegmentLayout2Module', name: 'SegmentLayout2', segment: 'segment-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/segment/layout-3/segment-layout-3.module#SegmentLayout3Module', name: 'SegmentLayout3', segment: 'segment-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/select/layout-1/select-layout-1.module#SelectLayout1Module', name: 'SelectLayout1', segment: 'select-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/select/layout-2/select-layout-2.module#SelectLayout2Module', name: 'SelectLayout2', segment: 'select-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/select/layout-4/select-layout-4.module#SelectLayout4Module', name: 'SelectLayout4', segment: 'select-layout-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/select/layout-3/select-layout-3.module#SelectLayout3Module', name: 'SelectLayout3', segment: 'select-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/select/layout-5/select-layout-5.module#SelectLayout5Module', name: 'SelectLayout5', segment: 'select-layout-5', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/select/layout-6/select-layout-6.module#SelectLayout6Module', name: 'SelectLayout6', segment: 'select-layout-6', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/spinner/spinner.module#SpinnerModule', name: 'Spinner', segment: 'spinner', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/splash-screen/layout-1/splash-screen-layout-1.module#SplashScreenLayout1Module', name: 'SplashScreenLayout1', segment: 'splash-screen-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/splash-screen/layout-3/splash-screen-layout-3.module#SplashScreenLayout3Module', name: 'SplashScreenLayout3', segment: 'splash-screen-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/splash-screen/layout-2/splash-screen-layout-2.module#SplashScreenLayout2Module', name: 'SplashScreenLayout2', segment: 'splash-screen-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/tabs/layout-1/tabs-layout-1.module#TabsLayout1Module', name: 'TabsLayout1', segment: 'tabs-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/sub-image-gallery/sub-image-gallery.module#SubImageGalleryModule', name: 'SubImageGallery', segment: 'sub-image-gallery', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/tabs/layout-2/tabs-layout-2.module#TabsLayout2Module', name: 'TabsLayout2', segment: 'tabs-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/tabs/layout-3/tabs-layout-3.module#TabsLayout3Module', name: 'TabsLayout3', segment: 'tabs-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/time-line/layout-1/time-line-layout-1.module#TimeLineLayout1Module', name: 'TimeLineLayout1', segment: 'time-line-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/time-line/layout-2/time-line-layout-2.module#TimeLineLayout2Module', name: 'TimeLineLayout2', segment: 'time-line-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/time-line/layout-3/time-line-layout-3.module#TimeLineLayout3Module', name: 'TimeLineLayout3', segment: 'time-line-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/toggle/layout-1/toggle-layout-1.module#ToggleLayout1Module', name: 'ToggleLayout1', segment: 'toggle-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/toggle/layout-2/toggle-layout-2.module#ToggleLayout2Module', name: 'ToggleLayout2', segment: 'toggle-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/wizard/layout-1/wizard-layout-1.module#WizardLayout1Module', name: 'WizardLayout1', segment: 'wizard-layout-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/toggle/layout-3/toggle-layout-3.module#ToggleLayout3Module', name: 'ToggleLayout3', segment: 'toggle-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/wizard/layout-3/wizard-layout-3.module#WizardLayout3Module', name: 'WizardLayout3', segment: 'wizard-layout-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../components/wizard/layout-2/wizard-layout-2.module#WizardLayout2Module', name: 'WizardLayout2', segment: 'wizard-layout-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-company/add-company.module#AddCompanyPageModule', name: 'AddCompanyPage', segment: 'add-company', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/company-list/company-list.module#CompanyListPageModule', name: 'CompanyListPage', segment: 'company-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/company-profile/company-profile.module#CompanyProfilePageModule', name: 'CompanyProfilePage', segment: 'company-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment-modal/payment-modal.module#PaymentModalPageModule', name: 'PaymentModalPage', segment: 'payment-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_28__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyCB7jsMv0hjwkVRan7vZZ9hDlI-dQNZRV8'
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_company_list_company_list__["a" /* CompanyListPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_company_profile_company_profile__["a" /* CompanyProfilePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_payment_modal_payment_modal__["a" /* PaymentModalPage */],
                __WEBPACK_IMPORTED_MODULE_20__components_profile_layout_5_profile_layout_5__["a" /* ProfileLayout5 */],
                __WEBPACK_IMPORTED_MODULE_15__pages_add_company_add_company__["a" /* AddCompanyPage */],
                __WEBPACK_IMPORTED_MODULE_23__components_register_layout_2_register_layout_2__["a" /* RegisterLayout2 */],
                __WEBPACK_IMPORTED_MODULE_24__components_login_layout_1_login_layout_1__["a" /* LoginLayout1 */],
                __WEBPACK_IMPORTED_MODULE_25__components_wizard_layout_2_wizard_layout_2__["a" /* WizardLayout2 */],
                __WEBPACK_IMPORTED_MODULE_21__components_profile_user_profile_user_profile__["a" /* UserProfileLayout */],
                __WEBPACK_IMPORTED_MODULE_22__components_profile_add_company_add_company_layout__["a" /* AddCompanyLayout */],
                __WEBPACK_IMPORTED_MODULE_19__components_list_view_appearance_animation_layout_2_appearance_animation_layout_2__["a" /* AppearanceAnimationLayout2 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]],
            providers: [
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_26__ionic_native_barcode_scanner__["a" /* BarcodeScanner */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_30__ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_31__ionic_native_unique_device_id__["a" /* UniqueDeviceID */], __WEBPACK_IMPORTED_MODULE_32__ionic_native_call_number_ngx__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_5__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_6__services_loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_transfer_ngx__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_33__ionic_native_in_app_purchase_ngx__["a" /* InAppPurchase */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_27__providers_rest_rest__["a" /* RestProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 867:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_menu_service__ = __webpack_require__(868);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_app_settings__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_company_list_company_list__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_rest_rest__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_add_company_add_company__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_storage__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var MyApp = /** @class */ (function () {
    function MyApp(platform, menu, splashScreen, statusBar, menuService, modalCtrl, rest, events, storage) {
        var _this = this;
        this.platform = platform;
        this.menu = menu;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.menuService = menuService;
        this.modalCtrl = modalCtrl;
        this.rest = rest;
        this.events = events;
        this.storage = storage;
        this.rootPage = "HomePage";
        this.showLevel1 = null;
        this.showLevel2 = null;
        this.showLevel1Profile = null;
        this.showLevel2Profile = null;
        this.isLogin = false;
        this.isCoperateUser = false;
        this.initializeApp();
        //this.pages = menuService.getAllThemes();
        // set default Language
        this.rest.setLanguage('1');
        this.getMenuList();
        this.leftMenuTitle = menuService.getTitle();
        this.menuService.load(null).subscribe(function (snapshot) {
            _this.params = snapshot;
            if (__WEBPACK_IMPORTED_MODULE_5__services_app_settings__["a" /* AppSettings */].SHOW_START_WIZARD) {
                _this.presentProfileModal();
            }
        });
        this.storage.get("user").then(function (val) {
            //console.log(val);
            if (val != null) {
                _this.rest.setUserLogin(val);
                _this.isLogin = true;
                if (_this.rest.currentUser.type == 'Corporate User') {
                    _this.isCoperateUser = true;
                }
            }
        });
        events.subscribe('language:changed', function () {
            // user and time are the same arguments passed in `events.publish(user, time)`
            _this.getMenuList();
            _this.rest.getMenuLang();
        });
        events.subscribe('login:changed', function () {
            // user and time are the same arguments passed in `events.publish(user, time)`
            if (_this.rest.currentUser) {
                _this.isLogin = true;
                if (_this.rest.currentUser.type == 'Corporate User') {
                    _this.isCoperateUser = true;
                }
            }
        });
        events.subscribe('logout:changed', function () {
            // user and time are the same arguments passed in `events.publish(user, time)`
            if (!_this.rest.currentUser) {
                _this.isLogin = false;
                _this.isCoperateUser = false;
            }
        });
    }
    MyApp.prototype.getMenuList = function () {
        var _this = this;
        this.rest.getMenuLang().then(function (data) {
            if (_this.rest.currentMenu) {
                _this.menuLang = _this.rest.currentMenu;
            }
        });
        ;
        this.rest.getMenuList().then(function (data) {
            if (data) {
                _this.pages = data;
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]);
            }
            else {
                console.log("wrong");
            }
        });
        this.profilePages = [
            {
                "menu": "My Company",
                "subMenu": [
                    {
                        "subMenu": "My Company",
                        "page": "myCompany"
                    },
                    {
                        "subMenu": "Add / Edit My Company",
                        "page": "editMyCompany"
                    }
                ]
            }
        ];
        //this.profilePages = [];
        //this.profilePages  = null;
    };
    MyApp.prototype.presentProfileModal = function () {
        var profileModal = this.modalCtrl.create("IntroPage");
        profileModal.present();
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            localStorage.setItem("mailChimpLocal", "true");
        });
    };
    MyApp.prototype.openPage = function (menuId) {
        // close the menu when clicking a link from the menu
        // navigate to the new page if it is not the current page
        //if (page.singlePage) {
        //  this.menu.open();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_company_list_company_list__["a" /* CompanyListPage */], { "menuId": menuId });
        //} else {
        //  this.nav.setRoot("ItemsPage", {
        //    componentName: page.theme
        //  });
        //}
    };
    ;
    MyApp.prototype.openProfilePage = function (page) {
        if (page === 'editMyCompany') {
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_11__pages_add_company_add_company__["a" /* AddCompanyPage */]);
        }
    };
    ;
    MyApp.prototype.openHomePage = function () {
        // close the menu when clicking a link from the menu
        // navigate to the new page if it is not the current page
        //if (page.singlePage) {
        //  this.menu.open();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]);
        //} else {
        //  this.nav.setRoot("ItemsPage", {
        //    componentName: page.theme
        //  });
        //}
    };
    ;
    MyApp.prototype.getPageForOpen = function (value) {
        return null;
    };
    MyApp.prototype.getServiceForPage = function (value) {
        return null;
    };
    MyApp.prototype.toggleLevel1 = function (idx) {
        if (this.isLevel1Shown(idx)) {
            this.showLevel1 = null;
        }
        else {
            this.showLevel1 = idx;
        }
    };
    MyApp.prototype.toggleLevel2 = function (idx) {
        if (this.isLevel2Shown(idx)) {
            this.showLevel1 = null;
            this.showLevel2 = null;
        }
        else {
            this.showLevel1 = idx;
            this.showLevel2 = idx;
        }
    };
    MyApp.prototype.isLevel1Shown = function (idx) {
        return this.showLevel1 === idx;
    };
    MyApp.prototype.isLevel2Shown = function (idx) {
        return this.showLevel2 === idx;
    };
    MyApp.prototype.toggleLevel1Profile = function (idx) {
        if (this.isLevel1ShownProfile(idx)) {
            this.showLevel1Profile = null;
        }
        else {
            this.showLevel1Profile = idx;
        }
    };
    MyApp.prototype.toggleLevel2Profile = function (idx) {
        if (this.isLevel2ShownProfile(idx)) {
            this.showLevel1Profile = null;
            this.showLevel2Profile = null;
        }
        else {
            this.showLevel1Profile = idx;
            this.showLevel2Profile = idx;
        }
    };
    MyApp.prototype.isLevel1ShownProfile = function (idx) {
        return this.showLevel1Profile === idx;
    };
    MyApp.prototype.isLevel2ShownProfile = function (idx) {
        return this.showLevel2Profile === idx;
    };
    MyApp.prototype.login = function () {
        if (this.rest.currentUser) {
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__["a" /* ProfilePage */]);
        }
        else {
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */]);
        }
    };
    MyApp.prototype.logout = function () {
        this.rest.logout();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/app/app.html"*/'<ion-split-pane when="md">\n\n  <ion-menu [content]="content" *ngIf="params != null">\n\n    <!-- Menu Main Top -->\n\n    <ion-header padding background-size header-background-image menuClose style="background-color: #000;" (click)="openHomePage()">\n\n\n\n      <ion-thumbnail text-center>\n\n        <div text-center>\n\n          <img src="../assets/images/logo/login.png">\n\n        </div>\n\n      </ion-thumbnail>\n\n\n\n    </ion-header>\n\n    <!-- Menu Main List -->\n\n    <ion-content>\n\n\n\n      <ion-list no-margin no-padding>\n\n        <ion-item menuClose ion-item item-title no-padding main-menu text-wrap (click)="openHomePage()">\n\n          <ion-icon slot="start" name="home"></ion-icon>\n\n          <h2 padding-left *ngIf="menuLang">\n\n            {{menuLang.home}}\n\n          </h2>\n\n        </ion-item>\n\n        <ion-item menuClose ion-item item-title no-padding main-menu text-wrap (click)="openPage(3)">\n\n          <ion-icon slot="start" name="search"></ion-icon>\n\n          <h2 padding-left *ngIf="menuLang">\n\n            {{menuLang.allCompany}}\n\n          </h2>\n\n        </ion-item>\n\n        <ion-item menuClose ion-item item-title no-padding main-menu text-wrap (click)="openPage(p.almc_nature_id)" *ngFor="let p of pages; let i=index">\n\n          <ion-icon slot="start" name="list-box"></ion-icon>\n\n          <h2 padding-left>\n\n            {{p.almc_name}}\n\n          </h2>\n\n        </ion-item>\n\n        \n\n        <div *ngIf="isCoperateUser">\n\n          <ion-item ion-item item-title no-padding main-menu *ngFor="let profile of profilePages; let j=index" text-wrap\n\n            (click)="toggleLevel1Profile(\'idx\'+j)" [ngClass]="{active: isLevel1ShownProfile(\'idx\'+j)}">\n\n            <h2 padding-left>\n\n              {{profile.menu}}\n\n            </h2>\n\n            <ion-list *ngIf="isLevel1ShownProfile(\'idx\'+j)">\n\n              <ion-item menuClose ion-item item-title no-padding main-menu\n\n                *ngFor="let sub of profile.subMenu; let j2=index" text-wrap (click)="openProfilePage(sub.page)">\n\n                <h2 padding-left>\n\n                  {{sub.subMenu}}\n\n                  <ion-icon color="success" item-right></ion-icon>\n\n                </h2>\n\n              </ion-item>\n\n            </ion-list>\n\n          </ion-item>\n\n        </div>\n\n        <ion-item ion-item item-title no-padding main-menu text-wrap>\n\n          <ion-icon name="call"></ion-icon>\n\n          <h2 padding-left *ngIf="menuLang">\n\n            {{menuLang.contactUs}}\n\n          </h2>\n\n        </ion-item>\n\n        <ion-item ion-item item-title no-padding main-menu text-wrap>\n\n          <ion-icon name="people"></ion-icon>\n\n          <h2 padding-left *ngIf="menuLang">\n\n            {{menuLang.aboutUs}}\n\n          </h2>\n\n        </ion-item>\n\n        <ion-item *ngIf="!isLogin" ion-item item-title no-padding main-menu text-wrap (click)="login()" menuClose>\n\n          <ion-icon name="log-in"></ion-icon>\n\n          <h2 padding-left *ngIf="menuLang">\n\n            {{menuLang.login}}\n\n          </h2>\n\n        </ion-item>\n\n        <ion-item *ngIf="isLogin" ion-item item-title no-padding main-menu text-wrap (click)="logout()" menuClose>\n\n          <ion-icon name="log-out"></ion-icon>\n\n          <h2 padding-left >\n\n            Logout\n\n          </h2>\n\n        </ion-item>\n\n      </ion-list>\n\n\n\n    </ion-content>\n\n    <ion-footer>\n\n      <ion-item nav-clear menu-close href="#" style="left:0;right:0;margin:0; width: 100%;position: fixed;">Logout\n\n      </ion-item>\n\n    </ion-footer>\n\n  </ion-menu>\n\n  <!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n  <ion-nav [root]="rootPage" #content main swipeBackEnabled="false"></ion-nav>\n\n</ion-split-pane>'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/app/app.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__services_menu_service__["a" /* MenuService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_4__services_menu_service__["a" /* MenuService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_storage__["b" /* Storage */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 868:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MenuService = /** @class */ (function () {
    function MenuService(rest) {
        var _this = this;
        this.rest = rest;
        this.getId = function () { return 'menu'; };
        this.getTitle = function () { return 'UIAppTemplate'; };
        this.getAllThemes = function () {
            //this.rest.getMenuList("1");
            return _this.result;
        };
        this.getDataForTheme = function (menuItem) {
            return {
                "background": "assets/images/background/16.jpg",
                "image": "assets/images/logo/login.png",
                "title": "Ionic3 UI Theme - Yellow Dark"
            };
        };
        this.getEventsForTheme = function (menuItem) {
            return {};
        };
        this.prepareParams = function (item) {
            return {
                title: item.title,
                data: {},
                events: _this.getEventsForTheme(item)
            };
        };
    }
    MenuService.prototype.load = function (item) {
        var _this = this;
        return new __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"](function (observer) {
            observer.next(_this.getDataForTheme(item));
            observer.complete();
        });
    };
    MenuService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], MenuService);
    return MenuService;
}());

//# sourceMappingURL=menu-service.js.map

/***/ }),

/***/ 869:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToastService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_settings__ = __webpack_require__(279);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ToastService = /** @class */ (function () {
    function ToastService(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ToastService.prototype.presentToast = function (message) {
        var toastItem = __WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].TOAST;
        toastItem["message"] = message;
        var toast = this.toastCtrl.create(toastItem);
        toast.present();
    };
    ToastService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["r" /* ToastController */]])
    ], ToastService);
    return ToastService;
}());

//# sourceMappingURL=toast-service.js.map

/***/ }),

/***/ 870:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoadingService = /** @class */ (function () {
    function LoadingService(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    LoadingService.prototype.show = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
    };
    LoadingService.prototype.hide = function () {
        this.loading.dismiss();
    };
    LoadingService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* LoadingController */]])
    ], LoadingService);
    return LoadingService;
}());

//# sourceMappingURL=loading-service.js.map

/***/ })

},[507]);
//# sourceMappingURL=main.js.map