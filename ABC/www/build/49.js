webpackJsonp([49],{

/***/ 1038:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileLayout2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfileLayout2 = /** @class */ (function () {
    function ProfileLayout2() {
    }
    ProfileLayout2.prototype.onEvent = function (event, item, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout2.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout2.prototype, "events", void 0);
    ProfileLayout2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'profile-layout-2',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/profile/layout-2/profile.html"*/'<!--Profile- Profile With Slider + Comments-->\n\n<ion-content>\n\n  <ion-grid *ngIf="data != null">\n\n    <ion-row>\n\n      <ion-col col-12 align-self-start>\n\n        <ion-card profile padding text-center>\n\n          <ion-avatar>\n\n            <img [src]="data.image" alt="">\n\n          </ion-avatar>\n\n          <ion-card-content text-center>\n\n            <h1 ion-text color="secondary">{{data.title}}</h1>\n\n            <p ion-text color="secondary">{{data.subtitle}}</p>\n\n          </ion-card-content>\n\n          <ion-row no-padding>\n\n            <ion-col>\n\n              <button ion-button button-clear clear no-padding>\n\n                <span small-font font-bold ion-text color="secondary">{{data.valueFollowers}}</span>\n\n                <span text-capitalize ion-text color="secondary">{{data.followers}}</span>\n\n              </button>\n\n            </ion-col>\n\n            <ion-col>\n\n              <button ion-button button-clear clear no-padding>\n\n                <span small-font font-bold ion-text color="secondary">{{data.valueFollowing}}</span>\n\n                <span text-capitalize ion-text color="secondary">{{data.following}}</span>\n\n              </button>\n\n            </ion-col>\n\n            <ion-col>\n\n              <button ion-button button-clear clear no-padding>\n\n                <span small-font font-bold ion-text color="secondary">{{data.valuePosts}}</span>\n\n                <span text-capitalize ion-text color="secondary">{{data.posts}}</span>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n      <!-- Slider-->\n\n      <ion-col col-12 align-self-start text-center>\n\n        <span span-small text-uppercase margin-top>{{data.category}}</span>\n\n        <ion-slides pager="true">\n\n          <ion-slide text-center padding *ngFor="let item of data.items;let i = index" (click)="onEvent(\'onItemClick\', item, $event)">\n\n            <ion-card padding>\n\n              <span span-medium text-uppercase>{{item.category}}</span>\n\n              <h1 card-title margin-top margin-bottom text-wrap>{{item.title}}</h1>\n\n              <button transparent ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onLike\', item, $event)">\n\n                <ion-icon ion-text color="accentLight" [ngClass]="{\'active\' : item.like.isActive}" [name]="item.like.icon"></ion-icon>\n\n                 <p ion-text color="accentLight">{{item.like.text}}</p>\n\n              </button>\n\n              <button transparent ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onComment\', item, $event)">\n\n                <ion-icon ion-text color="accentLight" [ngClass]="{\'active\' : item.comment.isActive}" [name]="item.comment.icon"></ion-icon>\n\n                <p ion-text color="accentLight">{{item.comment.number}} {{item.comment.text}}</p>\n\n              </button>\n\n            </ion-card>\n\n          </ion-slide>\n\n        </ion-slides>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/profile/layout-2/profile.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ProfileLayout2);
    return ProfileLayout2;
}());

//# sourceMappingURL=profile-layout-2.js.map

/***/ }),

/***/ 929:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileLayout2Module", function() { return ProfileLayout2Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_layout_2__ = __webpack_require__(1038);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfileLayout2Module = /** @class */ (function () {
    function ProfileLayout2Module() {
    }
    ProfileLayout2Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_2__["a" /* ProfileLayout2 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_layout_2__["a" /* ProfileLayout2 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_2__["a" /* ProfileLayout2 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ProfileLayout2Module);
    return ProfileLayout2Module;
}());

//# sourceMappingURL=profile-layout-2.module.js.map

/***/ })

});
//# sourceMappingURL=49.js.map