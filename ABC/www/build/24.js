webpackJsonp([24],{

/***/ 1073:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeLineLayout2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TimeLineLayout2 = /** @class */ (function () {
    function TimeLineLayout2() {
        var _this = this;
        this.onEvent = function (event, item) {
            if (_this.events[event]) {
                _this.events[event](item);
            }
        };
    }
    TimeLineLayout2.prototype.ngOnChanges = function (changes) {
        this.data = changes['data'].currentValue;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('data'),
        __metadata("design:type", Object)
    ], TimeLineLayout2.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('events'),
        __metadata("design:type", Object)
    ], TimeLineLayout2.prototype, "events", void 0);
    TimeLineLayout2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'time-line-layout-2',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/time-line/layout-2/time-line.html"*/'<!--Themes Time Line - Timeline With Avatar -->\n\n<ion-content>\n\n    <ion-grid *ngIf="data != null">\n\n      <ion-row>\n\n        <ion-col col-11 offset-1>\n\n          <ion-list margin-top>\n\n          <div item-thumbnail margin-bottom *ngFor="let item of data.items; let i= index" (click)="onEvent(\'onItemClick\', item, $event)">\n\n            <span span-small>{{item.time}}</span>\n\n            <ion-item margin-top no-lines>\n\n              <ion-thumbnail item-start>\n\n                <img [src]="item.avatar">\n\n              </ion-thumbnail>\n\n              <h2 text-wrap item-title>{{item.title}}</h2>\n\n              <div item-content>\n\n                <ion-badge badge-light>{{item.price}}</ion-badge>\n\n              </div>\n\n            </ion-item>\n\n          </div>\n\n      </ion-list>\n\n      </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/time-line/layout-2/time-line.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TimeLineLayout2);
    return TimeLineLayout2;
}());

//# sourceMappingURL=time-line-layout-2.js.map

/***/ }),

/***/ 958:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeLineLayout2Module", function() { return TimeLineLayout2Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__time_line_layout_2__ = __webpack_require__(1073);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TimeLineLayout2Module = /** @class */ (function () {
    function TimeLineLayout2Module() {
    }
    TimeLineLayout2Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__time_line_layout_2__["a" /* TimeLineLayout2 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__time_line_layout_2__["a" /* TimeLineLayout2 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__time_line_layout_2__["a" /* TimeLineLayout2 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], TimeLineLayout2Module);
    return TimeLineLayout2Module;
}());

//# sourceMappingURL=time-line-layout-2.module.js.map

/***/ })

});
//# sourceMappingURL=24.js.map