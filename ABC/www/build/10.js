webpackJsonp([10],{

/***/ 1030:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapsLayout1; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MapsLayout1 = /** @class */ (function () {
    function MapsLayout1() {
    }
    MapsLayout1.prototype.onEvent = function (event, index, e) {
        if (this.events[event]) {
            this.events[event]();
        }
        console.log(event);
    };
    MapsLayout1.prototype.onStarClass = function (items, index, e) {
        for (var i = 0; i < items.length; i++) {
            items[i].isActive = i <= index;
        }
        this.onEvent("onRates", index, e);
    };
    ;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], MapsLayout1.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], MapsLayout1.prototype, "events", void 0);
    MapsLayout1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'maps-layout-1',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/maps/layout-1/maps.html"*/'<!--Content -->\n\n<ion-content elastic-header>\n\n    <div id="elastic-header">\n\n        <agm-map *ngIf="data != null" [latitude]="data.map.lat" [longitude]="data.map.lng" [zoom]="data.map.zoom" [mapTypeControl]="data.map.mapTypeControl" [streetViewControl]="data.map.streetViewControl">\n\n            <agm-marker [latitude]="data.map.lat" [longitude]="data.map.lng"></agm-marker>\n\n        </agm-map>\n\n    </div>\n\n    <ion-grid no-padding *ngIf="data != null">\n\n        <ion-row>\n\n            <ion-col col-12 map-header>\n\n                <ion-item social no-lines transparent>\n\n                    <ion-icon (click)="onEvent(\'onLike\', data)">\n\n                        <i class="icon {{data.iconLike}}"></i>\n\n                    </ion-icon>\n\n                    <ion-icon (click)="onEvent(\'onFavorite\', data)">\n\n                        <i class="icon {{data.iconFavorite}}"></i>\n\n                    </ion-icon>\n\n                    <ion-icon (click)="onEvent(\'onShare\', data)">\n\n                        <i class="icon {{data.iconShare}}"></i>\n\n                    </ion-icon>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                    <h1 padding-left maps-title>{{data.title}}</h1>\n\n                    <p padding-left maps-subtitle>{{data.titleDescription}}</p>\n\n                    <!--Parallax Rateing-->\n\n                    <ion-item no-margin padding-left transparent>\n\n                        <ion-icon *ngFor="let item of data.iconsStars;let i = index" (click)="onStarClass(data.iconsStars, i, $event)">\n\n                            <i *ngIf="item.isActive" class="icon {{item.iconActive}}"></i>\n\n                            <i *ngIf="!item.isActive" class="icon {{item.iconInactive}}"></i>\n\n                        </ion-icon>\n\n                        <span>{{data.reviews}}</span>\n\n                    </ion-item>\n\n                </ion-item>\n\n            </ion-col>\n\n            <!---Content-->\n\n            <ion-col col-12 map-content>\n\n                <ion-item padding no-lines>\n\n                    <h2 title padding-left>{{data.contentTitle}}</h2>\n\n                    <p description text-wrap padding-left>{{data.contentDescription}}</p>\n\n                </ion-item>\n\n            </ion-col>\n\n            <!---Content Info-->\n\n            <ion-col col-12 map-content>\n\n                <ion-list info>\n\n                    <ion-item no-lines padding-left>\n\n                        <ion-icon item-start icon-small padding-left>\n\n                            <i class="icon {{data.iconLoacation}}"></i>\n\n                        </ion-icon>\n\n                        <p no-margin description>{{data.iconLoacationText}}</p>\n\n                    </ion-item>\n\n                    <ion-item no-lines padding-left>\n\n                        <ion-icon item-start icon-small padding-left>\n\n                            <i class="icon {{data.iconWatch}}"></i>\n\n                        </ion-icon>\n\n                        <p no-margin description>{{data.iconWatchText}}</p>\n\n                    </ion-item>\n\n                    <ion-item no-lines padding-left>\n\n                        <ion-icon item-start icon-small padding-left>\n\n                            <i class="icon {{data.iconPhone}}"></i>\n\n                        </ion-icon>\n\n                        <p no-margin description>{{data.iconPhoneText}}</p>\n\n                    </ion-item>\n\n                    <ion-item no-lines padding-left>\n\n                        <ion-icon item-start icon-small padding-left>\n\n                            <i class="icon {{data.iconEarth}}"></i>\n\n                        </ion-icon>\n\n                        <p no-margin description>{{data.iconEarthText}}</p>\n\n                    </ion-item>\n\n                    <ion-item no-lines padding-left>\n\n                        <ion-icon item-start icon-small padding-left>\n\n                            <i class="icon {{data.iconEmail}}"></i>\n\n                        </ion-icon>\n\n                        <p no-margin description>{{data.iconEmailText}}</p>\n\n                    </ion-item>\n\n                </ion-list>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/maps/layout-1/maps.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], MapsLayout1);
    return MapsLayout1;
}());

//# sourceMappingURL=maps-layout-1.js.map

/***/ }),

/***/ 921:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapsLayout1Module", function() { return MapsLayout1Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maps_layout_1__ = __webpack_require__(1030);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__elastic_header_elastic_header_module__ = __webpack_require__(973);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_app_settings__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__agm_core__ = __webpack_require__(499);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var MapsLayout1Module = /** @class */ (function () {
    function MapsLayout1Module() {
    }
    MapsLayout1Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__maps_layout_1__["a" /* MapsLayout1 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__elastic_header_elastic_header_module__["a" /* ElasticHeaderModule */],
                __WEBPACK_IMPORTED_MODULE_5__agm_core__["a" /* AgmCoreModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__services_app_settings__["a" /* AppSettings */].MAP_KEY),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__maps_layout_1__["a" /* MapsLayout1 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__maps_layout_1__["a" /* MapsLayout1 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], MapsLayout1Module);
    return MapsLayout1Module;
}());

//# sourceMappingURL=maps-layout-1.module.js.map

/***/ }),

/***/ 973:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ElasticHeaderModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__elastic_header__ = __webpack_require__(974);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ElasticHeaderModule = /** @class */ (function () {
    function ElasticHeaderModule() {
    }
    ElasticHeaderModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__elastic_header__["a" /* ElasticHeader */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__elastic_header__["a" /* ElasticHeader */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ElasticHeaderModule);
    return ElasticHeaderModule;
}());

//# sourceMappingURL=elastic-header.module.js.map

/***/ }),

/***/ 974:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ElasticHeader; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ElasticHeader = /** @class */ (function () {
    function ElasticHeader(element, renderer) {
        this.element = element;
        this.renderer = renderer;
    }
    ElasticHeader.prototype.ngOnInit = function () {
        var _this = this;
        this.scrollerHandle = this.element.nativeElement.getElementsByClassName('scroll-content')[0];
        this.header = this.scrollerHandle.firstElementChild;
        this.headerHeight = this.scrollerHandle.clientHeight;
        this.ticking = false;
        this.renderer.setElementStyle(this.header, 'webkitTransformOrigin', 'center bottom');
        window.addEventListener('resize', function () {
            _this.headerHeight = _this.scrollerHandle.clientHeight;
        }, false);
        this.scrollerHandle.addEventListener('scroll', function () {
            if (!_this.ticking) {
                window.requestAnimationFrame(function () {
                    _this.updateElasticHeader();
                });
            }
            _this.ticking = true;
        });
    };
    ElasticHeader.prototype.updateElasticHeader = function () {
        this.scrollTop = this.scrollerHandle.scrollTop;
        if (this.scrollTop >= 0) {
            this.translateAmt = this.scrollTop / 2;
            this.scaleAmt = 1;
        }
        else {
            this.translateAmt = 0;
            this.scaleAmt = -this.scrollTop / this.headerHeight + 1;
        }
        this.renderer.setElementStyle(this.header, 'webkitTransform', 'translate3d(0,' + this.translateAmt + 'px,0) scale(' + this.scaleAmt + ',' + this.scaleAmt + ')');
        this.ticking = false;
    };
    ElasticHeader = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Directive */])({
            selector: '[elastic-header]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer */]])
    ], ElasticHeader);
    return ElasticHeader;
}());

//# sourceMappingURL=elastic-header.js.map

/***/ })

});
//# sourceMappingURL=10.js.map