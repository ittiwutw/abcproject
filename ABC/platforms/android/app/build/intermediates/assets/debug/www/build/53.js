webpackJsonp([53],{

/***/ 1029:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapsLayout2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MapsLayout2 = /** @class */ (function () {
    function MapsLayout2() {
    }
    MapsLayout2.prototype.onEvent = function (event) {
        if (this.events[event]) {
            this.events[event]();
        }
        console.log(event);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], MapsLayout2.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], MapsLayout2.prototype, "events", void 0);
    MapsLayout2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'maps-layout-2',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/maps/layout-2/maps.html"*/'<!--Google Maps Gmaps + About Us-->\n\n<ion-content *ngIf="data != null">\n\n  <ion-grid no-padding>\n\n    <ion-row no-padding>\n\n      <!--Social Shere-->\n\n      <ion-col col-12>\n\n        <ion-item no-lines>\n\n          <div float-right social>\n\n            <ion-icon icon-small (click)="onEvent(\'onLike\', data)">\n\n              <i class="icon {{data.iconLike}}"></i>\n\n            </ion-icon>\n\n            <ion-icon icon-small (click)="onEvent(\'onFavorite\', data)">\n\n              <i class="icon {{data.iconFavorite}}"></i>\n\n            </ion-icon>\n\n            <ion-icon icon-small (click)="onEvent(\'onShare\', data)">\n\n              <i class="icon {{data.iconShare}}"></i>\n\n            </ion-icon>\n\n          </div>\n\n        </ion-item>\n\n      </ion-col>\n\n      <!---About Name-->\n\n      <ion-col col-12>\n\n        <ion-item map-header no-lines>\n\n          <h1 maps-title padding-horizontal>{{data.title}}</h1>\n\n          <p maps-subtitle padding-horizontal>{{data.titleDescription}}</p>\n\n        </ion-item>\n\n      </ion-col>\n\n      <!---About Us-->\n\n      <ion-col col-12 map-content>\n\n        <ion-item no-lines>\n\n          <h2 title padding-horizontal>{{data.contentTitle}}</h2>\n\n          <p description text-wrap padding-horizontal>{{data.contentDescription}}</p>\n\n        </ion-item>\n\n        <!---About Info-->\n\n        <ion-item no-lines>\n\n          <ion-icon item-start icon-small padding-left>\n\n            <i class="icon {{data.iconLoacation}}"></i>\n\n          </ion-icon>\n\n          <p no-margin description>{{data.iconLoacationText}}</p>\n\n        </ion-item>\n\n        <ion-item no-lines>\n\n          <ion-icon item-start icon-small padding-left>\n\n            <i class="icon {{data.iconWatch}}"></i>\n\n          </ion-icon>\n\n          <p no-margin description>{{data.iconWatchText}}</p>\n\n        </ion-item>\n\n        <ion-item no-lines>\n\n          <ion-icon item-start icon-small padding-left>\n\n            <i class="icon {{data.iconPhone}}"></i>\n\n          </ion-icon>\n\n          <p no-margin description>{{data.iconPhoneText}}</p>\n\n        </ion-item>\n\n        <ion-item no-lines>\n\n          <ion-icon item-start icon-small padding-left>\n\n            <i class="icon {{data.iconEarth}}"></i>\n\n          </ion-icon>\n\n          <p no-margin description>{{data.iconEarthText}}</p>\n\n        </ion-item>\n\n        <ion-item no-lines>\n\n          <ion-icon item-start icon-small padding-left>\n\n            <i class="icon {{data.iconEmail}}"></i>\n\n          </ion-icon>\n\n          <p no-margin description>{{data.iconEmailText}}</p>\n\n        </ion-item>\n\n      </ion-col>\n\n      <!---MAP TEXT-->\n\n      <ion-col col-12>\n\n        <ion-item no-lines>\n\n          <h2 title padding-horizontal>{{data.titleMap}}</h2>\n\n        </ion-item>\n\n      </ion-col>\n\n      <!---MAP-->\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <agm-map [latitude]="data.map.lat" [longitude]="data.map.lng" [zoom]="data.map.zoom" [mapTypeControl]="data.map.mapTypeControl" [streetViewControl]="data.map.streetViewControl">\n\n          <agm-marker [latitude]="data.map.lat" [longitude]="data.map.lng"></agm-marker>\n\n        </agm-map>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/maps/layout-2/maps.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], MapsLayout2);
    return MapsLayout2;
}());

//# sourceMappingURL=maps-layout-2.js.map

/***/ }),

/***/ 920:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapsLayout2Module", function() { return MapsLayout2Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maps_layout_2__ = __webpack_require__(1029);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_app_settings__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__agm_core__ = __webpack_require__(499);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var MapsLayout2Module = /** @class */ (function () {
    function MapsLayout2Module() {
    }
    MapsLayout2Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__maps_layout_2__["a" /* MapsLayout2 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__agm_core__["a" /* AgmCoreModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__services_app_settings__["a" /* AppSettings */].MAP_KEY),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__maps_layout_2__["a" /* MapsLayout2 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__maps_layout_2__["a" /* MapsLayout2 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], MapsLayout2Module);
    return MapsLayout2Module;
}());

//# sourceMappingURL=maps-layout-2.module.js.map

/***/ })

});
//# sourceMappingURL=53.js.map