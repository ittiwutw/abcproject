webpackJsonp([51],{

/***/ 1034:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentLayout1; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PaymentLayout1 = /** @class */ (function () {
    function PaymentLayout1() {
        this.cardItem = {};
    }
    PaymentLayout1.prototype.onPay = function () {
        if (this.events["onPay"]) {
            this.events["onPay"](this.cardItem);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], PaymentLayout1.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], PaymentLayout1.prototype, "events", void 0);
    PaymentLayout1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'payment-layout-1',template:/*ion-inline-start:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/payment/layout-1/payment.html"*/'<!--Theme Payment - Payment -->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n    <div *ngIf="data != null">\n\n      <ion-title>{{data.title}}</ion-title>\n\n    </div>\n\n  </ion-navbar>\n\n</ion-header>\n\n<!-- Content -->\n\n<ion-content>\n\n  <ion-grid padding>\n\n    <ion-row padding *ngIf="data != null">\n\n      <ion-col col-12>\n\n        <img [src]="data.images" alt="">\n\n      </ion-col>\n\n      <ion-col col-12>\n\n        <ion-item margin-top no-lines>\n\n          <ion-input type="number" [placeholder]="data.cardNumber" [(ngModel)]="cardItem.cardNumber"></ion-input>\n\n        </ion-item>\n\n        <ion-item margin-top no-lines>\n\n          <ion-input type="text" [placeholder]="data.cardHolder" [(ngModel)]="cardItem.cardHolder"></ion-input>\n\n        </ion-item>\n\n        <ion-item margin-top no-lines>\n\n          <ion-datetime [displayFormat]="data.dateFormat" [placeholder]="data.experienceDate" [(ngModel)]="birth" [ngModelOptions]="{standalone: true}"></ion-datetime>\n\n        </ion-item>\n\n        <ion-item margin-top no-lines>\n\n          <ion-input type="number" [placeholder]="data.code" [(ngModel)]="cardItem.code"></ion-input>\n\n        </ion-item>\n\n      </ion-col>\n\n      <ion-col col-12 align-self-end>\n\n        <button ion-button block default-button *ngIf="data != null" (click)="onPay()">{{data.button}}</button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/ittiwutwongsawat/Documents/freelance/git/ABC/ABC/src/components/payment/layout-1/payment.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], PaymentLayout1);
    return PaymentLayout1;
}());

//# sourceMappingURL=payment-layout-1.js.map

/***/ }),

/***/ 925:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentLayout1Module", function() { return PaymentLayout1Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_layout_1__ = __webpack_require__(1034);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaymentLayout1Module = /** @class */ (function () {
    function PaymentLayout1Module() {
    }
    PaymentLayout1Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__payment_layout_1__["a" /* PaymentLayout1 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__payment_layout_1__["a" /* PaymentLayout1 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__payment_layout_1__["a" /* PaymentLayout1 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], PaymentLayout1Module);
    return PaymentLayout1Module;
}());

//# sourceMappingURL=payment-layout-1.module.js.map

/***/ })

});
//# sourceMappingURL=51.js.map