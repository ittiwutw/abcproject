import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

export interface Language {
  langId: String;
}

export interface Menu {
  home: String;
  allCompany: String;
  contactUs: String;
  aboutUs: String;
  login: String;
}

export interface Country {
  countryId: String;
  countryName: String;
}

export interface User {
  uid: String;
  username: String;
  type: String;
  member: String;
  status: String;
  activeFlg: String;
  bussinessField: String;
  profession: String;
  city: String;
  country: String;
  email: String;
  name: String;
  phone: String;
  password: String;
}

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  apiUrl = " http://abcappbkk.ssitconsultant.com/ABC-API/";

  currentLanguage: Language;
  currentCountry: Country;
  currentUser: User;
  currentMenu: Menu;

  constructor(public http: HttpClient, public events: Events, private storage: Storage) {
    console.log('Hello RestProvider Provider');
    //this.currentLanguage.langId = "1";

  }

  getMenuList() {
    return new Promise(resolve => {
      let lang: any;
      if (this.currentLanguage) {
        lang = this.currentLanguage
      } else {
        lang = "1";
      }
      this.http.get(this.apiUrl + 'getListMenuCompany.php?langId=' + lang).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getListCompanyByProductType(menuId) {
    return new Promise(resolve => {
      let lang: any;
      let countryId: any;
      if (this.currentLanguage) {
        lang = this.currentLanguage
      } else {
        lang = "1";
      }

      if (this.currentCountry) {
        countryId = this.currentCountry.countryId
      } else {
        countryId = "1";
      }
      this.http.get(this.apiUrl + 'getListCompanyByFilter.php?langId=' + lang + '&countryId=' + countryId + '&menuId=' + menuId + '&filter=&limit=5&offset=0').subscribe(data => {
        resolve(data);
        console.log(data)
      }, err => {
        console.log(err);
      });
    });
  }

  getCompanyProfile(companyId) {
    return new Promise(resolve => {
      let lang: any;
      if (this.currentLanguage) {
        lang = this.currentLanguage
      } else {
        lang = "1";
      }

      this.http.get(this.apiUrl + 'getProfileCompanyInfo.php?langId=' + lang + '&companyId=' + companyId).subscribe(data => {
        resolve(data);
        console.log(data)
      }, err => {
        console.log(err);
      });
    });
  }

  setLanguage(language: any) {
    this.currentLanguage = language;
    console.log("Language change to " + this.currentLanguage)
  }

  getLanguage() {
    return this.currentLanguage;
  }

  setCountry(country: any, countryName: any) {
    var countryId: any;
    if (country == 'TH') {
      countryId = '1';
    } else if (country == 'TW') {
      countryId = '2'
    }
    this.currentCountry = {
      countryId: countryId,
      countryName: countryName
    };
    console.log("Country change to " + this.currentCountry)
  }

  getCountry() {
    return this.currentCountry;
  }

  getBanners() {

    return new Promise(resolve => {
      let lang: any;
      if (this.currentLanguage) {
        lang = this.currentLanguage
      } else {
        lang = "1";
      }
      this.http.get(this.apiUrl + 'getListBanner.php').subscribe(data => {
        resolve(data);
        console.log(data)
      }, err => {
        console.log(err);
      });
    });
  }

  getMenuLang() {
    return new Promise(resolve => {
      let lang: any;
      if (this.currentLanguage) {
        lang = this.currentLanguage
      } else {
        lang = "1";
      }
      this.http.get(this.apiUrl + 'getMenuWording.php?langId=' + lang).subscribe(data => {
        this.setMenuLang(data);
        resolve(data);
        console.log(data)
      }, err => {
        console.log(err);
      });
    });
  }

  setMenuLang(res) {
    this.currentMenu = {
      home: res.almw_Home,
      allCompany: res.almw_AllCompany,
      contactUs: res.almw_ContractUs,
      aboutUs: res.almw_AboutUs,
      login: res.almw_Login,
    }
  }

  testPOST() {
    var link = 'http://localhost/leaveRequestAPI/LeaveRequestService/savePOST.php';
    var myData = JSON.stringify({ username: "XXXXXXX" });
    let datares: any = {};
    this.http.post(link, myData)
      .subscribe(data => {
        datares.response = data["_body"]; //https://stackoverflow.com/questions/39574305/property-body-does-not-exist-on-type-response

        console.log(datares.response);
      }, error => {
        console.log("Oooops!");
      });

  }

  login(userName: any, password: any, uuid: any) {
    return new Promise((resolve, reject) => {
      var header = new HttpHeaders();
      header.append("Content-type", "json/data; charset=utf-8");

      let data = JSON.stringify({ Data: { ID: userName, Pass: password, UID: uuid } });

      this.http.post(this.apiUrl + 'Login.php', data, { headers: header })
        .subscribe(res => {

          this.setUserLogin(res);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }

  setUserLogin(resUser: any) {
    console.log(resUser.aau_username)
    this.currentUser = {
      uid: resUser.aau_UID,
      activeFlg: resUser.aau_activeFlg,
      bussinessField: resUser.aau_bussiness_field,
      city: resUser.aau_city,
      country: resUser.aau_country,
      email: resUser.aau_email,
      member: resUser.aam_name_member,
      name: resUser.aau_name,
      phone: resUser.aau_phone,
      profession: resUser.aau_profession,
      status: resUser.aas_status_name,
      type: resUser.aat_name_type,
      username: resUser.aau_username,
      password: resUser.aau_password
    };

    this.storage.set("user", resUser);

  }

  logout() {
    //console.log(resUser.aau_username)
    this.currentUser = null;
    this.events.publish('logout:changed');
  }

  getAccoutType() {

    return new Promise(resolve => {
      let lang: any;
      if (this.currentLanguage) {
        lang = this.currentLanguage
      } else {
        lang = "1";
      }
      this.http.get(this.apiUrl + 'getListAccountType.php').subscribe(data => {
        resolve(data);
        console.log(data)
      }, err => {
        console.log(err);
      });
    });
  }

  getCountries() {

    return new Promise(resolve => {
      let lang: any;
      if (this.currentLanguage) {
        lang = this.currentLanguage
      } else {
        lang = "1";
      }
      this.http.get(this.apiUrl + 'getListCountry.php?langId=' + lang).subscribe(data => {
        resolve(data);
        console.log(data)
      }, err => {
        console.log(err);
      });
    });
  }

  register(Data: any) {
    return new Promise((resolve, reject) => {
      var header = new HttpHeaders();
      header.append("Content-type", "json/data; charset=utf-8");

      let prepareData = { Data };

      let sendData = JSON.stringify(prepareData);

      this.http.post(this.apiUrl + 'userRequestRegist.php', sendData, { headers: header })
        .subscribe(res => {

          //this.setUserLogin(res);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }


}
