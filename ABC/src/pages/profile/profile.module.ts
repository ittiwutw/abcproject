import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';

import { UserProfileModule } from '../../components/profile/user-profile/user-profile.module';

import { WizardLayout2Module } from '../../components/wizard/layout-2/wizard-layout-2.module';

@NgModule({
  declarations: [
    //ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    UserProfileModule,
    WizardLayout2Module
  ],
})
export class ProfilePageModule {}
