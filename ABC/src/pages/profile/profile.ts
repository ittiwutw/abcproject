import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PaymentModalPage } from '../payment-modal/payment-modal';

import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  params: any = {};
  headerData: any = {};
  blur: any;
  currentUser: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    public rest: RestProvider) {
    this.blur = navParams.get("blur");
    this.headerData.data = {
      "headerTitle": "User Profile"
    }
    this.currentUser = this.rest.currentUser;
    this.setData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyProfilePage');
  }

  setData() {
    this.params.data = this.currentUser;
    let that = this;
    this.params.events = {
      'onItemClick': function (item: any) {
        console.log("onItemClick");
      },
      'onClickPayment': function () {
        that.openPaymentModal();
      }
    };
  }

  openPaymentModal() {
    let shopPopup = this.modalCtrl.create(PaymentModalPage);
    shopPopup.present();
  }


}
