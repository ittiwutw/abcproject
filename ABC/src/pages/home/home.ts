import { Component, ElementRef, ViewChild, OnChanges } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { ProfilePage } from '../profile/profile';
// @ts-ignore
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";

import { RestProvider } from '../../providers/rest/rest';
import { Events } from 'ionic-angular';

import { CompanyProfilePage } from '../company-profile/company-profile';
import { CompanyListPage } from '../company-list/company-list';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
declare var google;

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'

})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  data: any = {};
  headerData: any = {};
  lang: any;
  globe: any;
  banners: any;
  zoomed: false;

  chartG : any;
  polyG: any;


  constructor(public navCtrl: NavController, public rest: RestProvider, public events: Events,
    private alertCtrl: AlertController, private iab: InAppBrowser) {
    //this.rest.testPOST();
    this.lang = rest.getLanguage();


    this.data = {
      "toolbarTitle": "ABC Application",
      "title": "Wellcome to",
      "subtitle": "ABC Application",
      "subtitle2": "Demo Version",
      "link": "http://csform.com/documentation-for-ionic-2-ui-template-app/",
      "description": "For better understanding how our template works please read documentation.",
      "background": "assets/images/background/29.jpg"
    };

    if (this.lang == '1') {
      this.headerData = {
        "headerTitle": "Choose Country"
      }
    } else {
      this.headerData = {
        "headerTitle": "เลือกประเทศ"
      }
    }



  }
  ionViewDidLoad() {
    this.loadBanner();
    this.loadMap2();
  }

  onClickBanner(companyId, url) {
    this.navCtrl.push(CompanyProfilePage, { companyId: companyId });
  }

  loadBanner() {
    let that = this;
    this.rest.getBanners().then((result: any) => {
      if (result) {
        that.banners = result;
        console.log(that.banners);
        // this.loadMap2();
      } else {
        console.log("wrong")
      }
    });

  }

  loadMap() {
    let chart = am4core.create(this.mapElement.nativeElement, am4maps.MapChart);

    //this.globe = new am4maps.projections.Orthographic();
    // Set projection
    chart.projection = new am4maps.projections.Orthographic();

    // Set map definition
    chart.geodata = am4geodata_worldLow;

    // Set projection
    chart.projection = new am4maps.projections.Orthographic();

    // Create map polygon series
    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());



    // Make map load polygon (like country names) data from GeoJSON
    polygonSeries.useGeodata = true;

    polygonSeries.data = [{
      "id": "TH",
      "selected": true
    }, {
      "id": "TW",
      "selected": true
    }]

    // Configure series
    var polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    polygonTemplate.fill = am4core.color("#fcd736");

    // Create hover state and set alternative fill color
    var hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color("#367B25");


    polygonTemplate.adapter.add("fill", function (fill, target) {
      const ctx = target.dataItem.dataContext as any;
      if (ctx && ctx.selected) {

        return am4core.color("#00af23");
      }
      return fill;
    });

    polygonTemplate.adapter.add("stroke", function (fill, target) {
      const ctx = target.dataItem.dataContext as any;
      if (ctx && ctx.selected) {
        return am4core.color("#00af23");
      }
      return fill;
    });

    // Make globe rotatable
    chart.seriesContainer.draggable = false;
    chart.seriesContainer.resizable = false;
    chart.maxZoomLevel = 2;
    //chart.seriesContainer.events.disableType("doublehit");
    //chart.chartContainer.background.events.disableType("doublehit");
    chart.deltaLongitude = -110;



    var originalDeltaLongitude = 0;

    chart.seriesContainer.events.on("down", function (ev) {
      originalDeltaLongitude = chart.deltaLongitude;
    });

    chart.seriesContainer.events.on("track", function (ev) {
      if (ev.target.isDown) {
        var pointer = ev.target.interactions.downPointers.getIndex(0);
        var startPoint = pointer.startPoint;
        var point = pointer.point;
        var shift = point.x - startPoint.x;
        chart.deltaLongitude = originalDeltaLongitude + shift / 2;
        //label.text = "chart.deltaLongitude = " + chart.numberFormatter.format(chart.deltaLongitude, "[green]#.|[red]#.|[#555]#");
      }
    });

    var zoomed = false;
    polygonTemplate.events.on("doublehit", function (ev) {
      // zoom to an object
      if (zoomed) {
        chart.goHome();
        zoomed = false;
      } else {
        chart.maxZoomLevel = 3;
        zoomed = true;
      }

    });

    polygonTemplate.events.on("hit", this.setCountry, this);


  }

  clickSearch() {
    this.chartG.goHome();
  }

  loadMap2() {
    this.chartG = am4core.create(this.mapElement.nativeElement, am4maps.MapChart);
    //this.globe = new am4maps.projections.Orthographic();
    // Set projection

    let chart = this.chartG;

    // Set map definition
    chart.geodata = am4geodata_worldLow;

    // Set projection
    chart.projection = new am4maps.projections.Miller();

   
    this.polyG = this.chartG.series.push(new am4maps.MapPolygonSeries());
    this.polyG.useGeodata = true;
    this.polyG.exclude = ["AQ"];

    // Create map polygon series
    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

    // Make map load polygon (like country names) data from GeoJSON
    polygonSeries.useGeodata = true;

    //chart.seriesContainer.draggable = false;
    chart.maxPanOut = 0;
    //chart.minZoomLevel = 20;

    // chart.zoomControl = new am4maps.ZoomControl();
    // chart.zoomControl.plusButton.disabled = true;
    // chart.zoomControl.minusButton.disabled = true;
    polygonSeries.data = [{
      "id": "TH",
      "selected": true
    }, {
      "id": "TW",
      "selected": true
    }, {
      "id": "HK",
      "selected": true
    }]

    // Configure series
    var polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    polygonTemplate.fill = am4core.color("#fcd736");


    // Create hover state and set alternative fill color
    var hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color("#367B25");

    // Remove Antarctica
    // polygonSeries.include = [
    //   "AM",
    //   "AZ",
    //   "BH",
    //   "BD",
    //   "BT",
    //   "IO",
    //   "BN",
    //   "KH",
    //   "CN",
    //   "CX",
    //   "CC",
    //   "CY",
    //   "GE",
    //   "HK",
    //   "IN",
    //   "ID",
    //   "IR",
    //   "IQ",
    //   "IL",
    //   "JP",
    //   "JO",
    //   "KZ",
    //   "KP",
    //   "KR",
    //   "KW",
    //   "KG",
    //   "LA",
    //   "LB",
    //   "MO",
    //   "MY",
    //   "MV",
    //   "MN",
    //   "MM",
    //   "NP",
    //   "OM",
    //   "PK",
    //   "PS",
    //   "PH",
    //   "QA",
    //   "SA",
    //   "SG",
    //   "LK",
    //   "SY",
    //   "TW",
    //   "TJ",
    //   "TH",
    //   "TL",
    //   "TM",
    //   "AE",
    //   "UZ",
    //   "VN",
    //   "YE"
    // ];
    polygonSeries.exclude = ["AQ"];

    // let zoomTo = ["TH", "TW", "CN"];

    // chart.events.on("ready", function (ev) {
    //   // Init extremes
    //   var north, south, west, east;

    //   // Find extreme coordinates for all pre-zoom countries
    //   for (let i = 0; i < zoomTo.length; i++) {
    //     var country = polygonSeries.getPolygonById(zoomTo[i]);
    //     if (north == undefined || (country.north > north)) {
    //       north = country.north;
    //     }
    //     if (south == undefined || (country.south < south)) {
    //       south = country.south;
    //     }
    //     if (west == undefined || (country.west < west)) {
    //       west = country.west;
    //     }
    //     if (east == undefined || (country.east > east)) {
    //       east = country.east;
    //     }
    //     country.isActive = true
    //   }

    //   // Pre-zoom
    //   chart.zoomToRectangle(north, east, south, west, 1, true);
    // });

    polygonTemplate.adapter.add("fill", function (fill, target) {
      const ctx = target.dataItem.dataContext as any;
      if (ctx && ctx.selected) {

        return am4core.color("#00af23");
      }
      return fill;
    });

    polygonTemplate.adapter.add("stroke", function (fill, target) {
      const ctx = target.dataItem.dataContext as any;
      if (ctx && ctx.selected) {
        return am4core.color("#00af23");
      }
      return fill;
    });

    var zoomed = false;
    polygonTemplate.events.on("doublehit", function (ev) {
      // zoom to an object
      if (zoomed) {
        chart.goHome();
        zoomed = false;
      } else {
        chart.maxZoomLevel = 1;
        zoomed = true;
      }

    });
    
    polygonTemplate.events.on("hit", this.setCountry, this);
  }

  choseCountry() {

    let alert = this.alertCtrl.create({
      title: "Select Business Area",
      message: "Which Country are you looking for ?",
      cssClass: "chooseCountry",
      buttons: [
        {
          text: 'THAILAND',
          cssClass: 'chooseCountry',
          handler: () => {
            this.chartG.zoomToMapObject(this.polyG.getPolygonById("TH"));
          }
        },
        {
          text: 'TAIWAN',
          handler: () => {
            this.chartG.zoomToMapObject(this.polyG.getPolygonById("TW"));
          }
        }
      ]
    });
    alert.present();

    console.log('current country : ' + this.rest.currentCountry);
  }

  setCountry(ev) {
    const ctx = ev.target.dataItem.dataContext as any;

    // get object info
    this.rest.setCountry(ctx.id, ctx.name);

    let alert = this.alertCtrl.create({
      title: ctx.name,
      message: "What are you looking for ?",
      cssClass: "chooseCountry",
      buttons: [
        {
          text: 'Product',
          cssClass: 'chooseCountry',
          handler: () => {
            this.navCtrl.setRoot(CompanyListPage, { "menuId": 1 });
          }
        },
        {
          text: 'Service',
          handler: () => {
            this.navCtrl.setRoot(CompanyListPage, { "menuId": 2 });
          }
        },
        {
          text: 'All',
          handler: () => {
            this.navCtrl.setRoot(CompanyListPage, { "menuId": 3 });
          }
        }
      ]
    });
    alert.present();

    console.log('current country : ' + this.rest.currentCountry);
  }

  trigerDoubleHit() { }

  openLogin() {
    if (this.rest.currentUser) {
      this.navCtrl.setRoot(ProfilePage);
    } else {
      this.navCtrl.setRoot(LoginPage);
    }
  }

  onClickLanguage(languageId) {
    this.lang = languageId;
    this.rest.setLanguage(languageId);
    this.events.publish('language:changed');
  }

  presentAlert(bannerDetail: any) {
    let that = this;
    let alert = this.alertCtrl.create({
      title: "Confirm",
      subTitle: "Confirm to exit current page ?",
      cssClass: "info-dialog",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            if (bannerDetail.linkType == '2') {
              this.navCtrl.push(CompanyProfilePage, { companyId: bannerDetail.companyId });
            } else {
              this.iab.create(bannerDetail.link, '_system');
            }
          }
        }
      ]
    });
    alert.present();
  }
}
