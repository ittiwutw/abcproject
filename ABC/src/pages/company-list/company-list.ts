import { Component, Input, ViewChild, AfterViewInit, OnChanges } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController,Events } from 'ionic-angular';
import { CompanyProfilePage } from '../company-profile/company-profile';
import { StatusBar } from '@ionic-native/status-bar';
import { LoadingService } from '../../services/loading-service';


import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the CompanyListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-company-list',
  templateUrl: 'company-list.html',
})
export class CompanyListPage {
  params: any = {};
  headerData: any = {};
  lang: any;
  country: any;
  countryName: any;
  constructor(public navCtrl: NavController, public rest: RestProvider, public navParams: NavParams, public platform: Platform,
    private statusBar: StatusBar, private alertCtrl: AlertController, public events: Events) {

    platform.ready().then(() => {
      this.statusBar.overlaysWebView(false);
    });

    this.lang = rest.getLanguage();
    //this.setCountry();

    if(this.lang == '1'){
      this.headerData.data = {
        "headerTitle": "Company List"
      }
    }else{
      this.headerData.data = {
        "headerTitle": "รายชื่อบริษัท"
      }
    }

    this.params.data = {
      "menuId": this.navParams.get("menuId")
    }

    this.params.events = {
      'onItemClick': function (companyId: any) {
        //if (item == 2) {
        navCtrl.push(CompanyProfilePage, { companyId: companyId });
        //} else {
        //  navCtrl.push(CompanyProfilePage, { blur: 0 });
        //}

      },
      'onFavorite': function (item) {
        item.favorite = !item.favorite;
        console.log("onFavorite");
      }
    };

    if (this.rest.currentCountry) {

      this.countryName = this.rest.currentCountry.countryName;

  } else {
      this.countryName = "THAILAND"
  }
  }

  setCountry(){
    let country = this.rest.currentCountry;

    if(country.countryId == '1'){
      this.countryName = "THAILAND"
    }else if (country.countryId == '2'){
      this.countryName = "TAIWAN"
    }
  }

  choseCountry() {

    let alert = this.alertCtrl.create({
      title: "Select Business Area",
      message: "Which Country are you looking for ?",
      cssClass: "chooseCountry",
      buttons: [
        {
          text: 'THAILAND',
          cssClass: 'chooseCountry',
          handler: () => {
            // get object info
            this.countryName = "THAILAND";
            this.rest.setCountry("TH", "THAILAND");
            this.events.publish('changeCountry:changed');
          }
        },
        {
          text: 'TAIWAN',
          handler: () => {
            this.countryName = "TAIWAN";
            this.rest.setCountry("TW", "TAIWAN");
            this.events.publish('changeCountry:changed');
          }
        }
      ]
    });
    alert.present();

    console.log('current country : ' + this.rest.currentCountry);
  }



}

