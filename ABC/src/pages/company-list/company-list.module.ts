import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppearanceAnimationLayout2Module } from '../../components/list-view/appearance-animation/layout-2/appearance-animation-layout-2.module';
import { CompanyListPage } from './company-list';

import { WizardLayout2Module } from '../../components/wizard/layout-2/wizard-layout-2.module';

@NgModule({
    declarations: [
      //CompanyListPage,
    ],
    imports: [
        IonicPageModule.forChild(CompanyListPage),
        AppearanceAnimationLayout2Module,
        WizardLayout2Module
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class CompanyListPageModule { }
