import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';


/**
 * Generated class for the PaymentModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-modal',
  templateUrl: 'payment-modal.html',
})
export class PaymentModalPage {

  public product: any = {
    name: 'Upgrade to Pro',
    appleProductId: 'premiumuser',
    googleProductId: 'com.abcdemo.test.premiumuser'
  };

  products: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public inAppPurchase: InAppPurchase
  ) {
    platform.ready().then(() => {
      console.log("platform")
      // this.configurePurchasing();
      if (!this.inAppPurchase) {
        console.log('Store not available.');
        alert('Store not available.');
      } else {
        alert('Start');
        alert(inAppPurchase)
        inAppPurchase
          .getProducts(['com.abcdemo.test.premiumuser'])
          .then(function (products) {
            console.log(products);
            alert(products);
            /*
               [{ productId: 'com.yourapp.prod1', 'title': '...', description: '...', currency: '...', price: '...', priceAsDecimal: '...' }, ...]
            */
          })
          .catch(function (err) {
            console.log(err);
            alert(err);
          });
      }

    });

  }





}
