
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RegisterLayout2Module } from '../../components/register/layout-2/register-layout-2.module';
import { WizardLayout2Module } from '../../components/wizard/layout-2/wizard-layout-2.module';


@NgModule({
  declarations: [
    //RegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
    RegisterLayout2Module,
    WizardLayout2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class RegisterPageModule { }
