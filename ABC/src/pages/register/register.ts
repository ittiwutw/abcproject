import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  params: any = {};
  headerData: any = {};
  accoutTypes: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController,
    public rest: RestProvider) {

    this.headerData.data = {
      "headerTitle": "Register Page"
    }

    this.setParameter();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  setParameter() {
    this.params.data = {
      "logo": "assets/images/logo/login.png",
      "iconAccount": "icon-account",
      "username": "Username",
      "iconHome": "icon-home-variant",
      "iconCity": "icon-city",
      "city": "City",
      "iconWeb": "icon-web",
      "country": "Country",
      "iconLock": "icon-lock",
      "password": "Password",
      "iconEmail": "icon-email-outline",
      "iconTel": "icon-phone",
      "iconName": "icon-account",
      "email": "Email",
      "name": "Full Name",
      "tel": "Phone No.",
      "submit": "submit",
      "skip": "Skip",
      "businessField": "Business Field",
      "profession": "profession",
      "iconBag": "icon-briefcase",
      "error": "Field can't be empty.",
      "errorUser": "Field can't be empty.",
      "errorPassword": "Field can't be empty.",
      "errorEmail": "Invalid email address.",
      "errorCountry": "Field can't be empty.",
      "errorCity": "Field can't be empty.",
      "errorName": "Field can't be empty.",
      "errorTel": "Field can't be empty.",
      "errorBusinessField": "Field can't be empty.",
      "errorProfession": "Field can't be empty.",
      "errorAgree": "Please Check Agree on Term & Condition.",
      "accoutType": this.accoutTypes
    };

    let that = this;
    this.params.events = {
      onRegister: function (params) {
        that.saveRegisterUser(params);
      },
      onSkip: function (params) {
        console.log('onSkip');
      },
      onClickTerm: function () {
        that.showTerm();
      }
    };
  }

  showTerm() {
    let that = this;
    let alert = this.alertCtrl.create({
      title: "Term & Condition",
      message: "Details ....",
      cssClass: "info-dialog",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {

          }
        }
      ]
    });
    alert.present();
  }

  saveRegisterUser(userInputData) {
    console.log(userInputData)
  }

}
