import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { RegisterPage } from '../register/register'
import { ProfilePage } from '../profile/profile'

import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  params: any = {};
  headerData: any = {};

  responseUser: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider) {

    this.headerData.data = {
      "headerTitle": "Login Page"
    }

    this.params.data = {
      "username": "Username",
      "password": "Password",
      "register": "Register",
      "login": "Login",
      "logo": "assets/images/logo/login.png",
      "errorUser": "Field can't be empty.",
      "errorPassword": "Field can't be empty.",
      "background": "assets/images/background/29.jpg"
    };
    let that = this;
    this.params.events = {
      onLogin: function (params) {
        //that.clickLogin(params);
      },
      onRegister: function (params) {
        that.clickRegister();
      }
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  clickRegister() {
    this.navCtrl.setRoot(RegisterPage);
  }
  

}
