import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { LoginLayout1Module } from '../../components/login/layout-1/login-layout-1.module';
import { WizardLayout2Module } from '../../components/wizard/layout-2/wizard-layout-2.module';

@NgModule({
  declarations: [
    //LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    LoginLayout1Module,
    WizardLayout2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LoginPageModule { }
