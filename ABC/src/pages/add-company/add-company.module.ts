import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCompanyPage } from './add-company';

import { AddCompanyModule } from '../../components/profile/add-company/add-company-layout.module';
import { WizardLayout2Module } from '../../components/wizard/layout-2/wizard-layout-2.module';

@NgModule({
  declarations: [
    //AddCompanyPage,
  ],
  imports: [
    IonicPageModule.forChild(AddCompanyPage),
    AddCompanyModule,
    WizardLayout2Module
  ],
})
export class AddCompanyPageModule {}
