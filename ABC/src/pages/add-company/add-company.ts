import { Component,ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { RestProvider } from '../../providers/rest/rest';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

/**
 * Generated class for the AddCompanyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-company',
  templateUrl: 'add-company.html',
})
export class AddCompanyPage {
  @ViewChild('iframe') mapElement: ElementRef;
  params: any = {};
  headerData: any = {};

  userDate: any;

  addCompanyUrl: any = 'http://www.abcappbkk.com/Admin/#/ABC-User/create-profile/';

  constructor(public navCtrl: NavController, public navParams: NavParams, private iab: InAppBrowser, public rest: RestProvider) {
    this.setData();

    this.userDate = this.rest.currentUser
    
    //this.addCompanyUrl = '';
    //this.iab.create('http://www.abcappbkk.com/Admin/#/ABCAdmin/addcompany', '_blank');
  }

  ngAfterViewInit() {
    let url = this.addCompanyUrl+''+this.rest.currentUser.username+'/'+this.rest.currentUser.password;
    console.log(url)
    this.mapElement.nativeElement.setAttribute("src", url);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCompanyPage');
  }

  setData(){
    this.headerData.data = {
      "headerTitle": "Add Company"
    }
    this.params.data = {
      "headerImage": "assets/images/background/29.jpg",
      "image": "https://www.beijerrefthai.com/wp-content/uploads/2018/05/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%8B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B9%80%E0%B8%81%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A7%E0%B8%81%E0%B8%B1%E0%B8%9A%E0%B8%9A%E0%B8%A3%E0%B8%B4%E0%B8%A9%E0%B8%B1%E0%B8%97.jpg",
      "companyName": "ABC (Thailand) Co., Ltd.",
      "companySubTitle": "Product Type : Vehicles & Parts",
      "blur": "0",
      "companyId": 1,
      "items": [
        {
          "id": 1,
          "address": "25th Fl., Nantawan Bldg., 161 Ratchadamri Road, Lumpini, Pathumwan, Bangkok 10330.",
          "iconPhone": "ios-phone-portrait",
          "iconMail": "mail-open",
          "iconGlobe": "globe",
          "phone": "099-056-9595",
          "mail": "hr@abc.com",
          "globe": "acb.com",
          "content": "Contact",
          "subtitle": "ABC was established in Thailand since 2000, by launching various car models to fulfill entire customer segments, with “Innovation that Excites” policy that enhance highest customers’ satisfaction from our Nissan and enlarge social participation throughout our Corporate Social Responsibility.",
          "title": "Company Information"
        }
      ]
    };
    let that = this;
    this.params.events = {
      'onItemClick': function (item: any) {
        console.log("onItemClick");
      },
      'onLike': function (item: any) {
        if (item && item.like) {
          if (item.like.isActive) {
            item.like.isActive = false;
            item.like.number--;
          } else {
            item.like.isActive = true;
            item.like.number++;
          }
        }
      },
      'onComment': function (item: any) {
        if (item && item.comment) {
          if (item.comment.isActive) {
            item.comment.isActive = false;
            item.comment.number--;
          } else {
            item.comment.isActive = true;
            item.comment.number++;
          }
        }
      }
    };
  }

}
