import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ProfileLayout5Module } from '../../components/profile/layout-5/profile-layout-5.module';
import { IonicPageModule } from 'ionic-angular';
import { CompanyProfilePage } from './company-profile';

import { WizardLayout2Module } from '../../components/wizard/layout-2/wizard-layout-2.module';

@NgModule({
  declarations: [
    //CompanyProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyProfilePage),
    ProfileLayout5Module,
    WizardLayout2Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CompanyProfilePageModule { }
