import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { RegisterPage } from '../register/register';


import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the CompanyProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-company-profile',
  templateUrl: 'company-profile.html',
})
export class CompanyProfilePage {
  params: any = {};
  headerData: any = {};
  blur: any;
  companyId: any;
  companyData: any;
  lang: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public rest: RestProvider) {
    this.blur = navParams.get("blur");
    this.companyId = this.navParams.get("companyId");

    this.lang = rest.getLanguage();

    if(this.lang == '1'){
      this.headerData.data = {
        "headerTitle": "Company Information"
      }
    }else{
      this.headerData.data = {
        "headerTitle": "ข้อมูลบริษัท"
      }
    }

    this.params.data = {
      "headerImage": "assets/images/background/29.jpg",
      "image": "https://www.beijerrefthai.com/wp-content/uploads/2018/05/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%8B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B9%80%E0%B8%81%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A7%E0%B8%81%E0%B8%B1%E0%B8%9A%E0%B8%9A%E0%B8%A3%E0%B8%B4%E0%B8%A9%E0%B8%B1%E0%B8%97.jpg",
      "companyName": "ABC (Thailand) Co., Ltd.",
      "companySubTitle": "Product Type : Vehicles & Parts",
      "blur": this.blur,
      "companyId": this.companyId,
      "items": [
        {
          "id": 1,
          "address": "25th Fl., Nantawan Bldg., 161 Ratchadamri Road, Lumpini, Pathumwan, Bangkok 10330.",
          "iconPhone": "ios-phone-portrait",
          "iconMail": "mail-open",
          "iconGlobe": "globe",
          "phone": "099-056-9595",
          "mail": "hr@abc.com",
          "globe": "acb.com",
          "content": "Contact",
          "subtitle": "ABC was established in Thailand since 2000, by launching various car models to fulfill entire customer segments, with “Innovation that Excites” policy that enhance highest customers’ satisfaction from our Nissan and enlarge social participation throughout our Corporate Social Responsibility.",
          "title": "Company Information"
        }
      ]
    };
    let that = this;
    this.params.events = {
      'onItemClick': function (item: any) {
        console.log("onItemClick");
      },
      'onLike': function (item: any) {
        if (item && item.like) {
          if (item.like.isActive) {
            item.like.isActive = false;
            item.like.number--;
          } else {
            item.like.isActive = true;
            item.like.number++;
          }
        }
      },
      'onComment': function (item: any) {
        if (item && item.comment) {
          if (item.comment.isActive) {
            item.comment.isActive = false;
            item.comment.number--;
          } else {
            item.comment.isActive = true;
            item.comment.number++;
          }
        }
      },
      'clickRegister': function (item: any) {
        if(that.blur == 1){
          that.presentAlert();
        }
        

      }
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyProfilePage');
  }

  presentAlert(): void {
    let that = this;
    let alert = this.alertCtrl.create({
      title: "Please sign up",
      message: "To see the details, please signup",
      subTitle: "Go to Register Page",
      cssClass: "info-dialog",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Go',
          handler: () => {
            that.navCtrl.setRoot(RegisterPage);
          }
        }
      ]
    });
    alert.present();
  }



}
