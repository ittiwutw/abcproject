import { IService } from './IService';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestProvider } from '../providers/rest/rest';

@Injectable()
export class MenuService implements IService {

  result: any;
  constructor( public rest: RestProvider) { }

  getId = (): string => 'menu';

  getTitle = (): string => 'UIAppTemplate';

  getAllThemes = (): Array<any> => {
    //this.rest.getMenuList("1");


    return this.result;
  };

  getDataForTheme = (menuItem: any) => {
    return {
      "background": "assets/images/background/16.jpg",
      "image": "assets/images/logo/login.png",
      "title": "Ionic3 UI Theme - Yellow Dark"
    };
  };

  getEventsForTheme = (menuItem: any): any => {
    return {};
  };

  prepareParams = (item: any) => {
    return {
      title: item.title,
      data: {},
      events: this.getEventsForTheme(item)
    };
  };

  load(item: any): Observable<any> {

    return new Observable(observer => {
      observer.next(this.getDataForTheme(item));
      observer.complete();
    });
  }

}
