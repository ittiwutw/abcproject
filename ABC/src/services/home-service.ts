import { IService } from './IService';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './app-settings';
import { ToastService } from './toast-service';
import { LoadingService } from './loading-service';

@Injectable()
export class HomeService {
    product;
    constructor(private loadingService: LoadingService) { }

    getData = () => {
        return {
            "toolbarTitle": "ABC Application",
            "title": "Wellcome to",
            "subtitle": "ABC Application",
            "subtitle2": "Demo Version",
            "link": "http://csform.com/documentation-for-ionic-2-ui-template-app/",
            "description": "For better understanding how our template works please read documentation.",
            "background": "assets/images/background/29.jpg"
        };
    };

    load(): Observable<any> {
        var that = this;
        that.loadingService.show();

        return new Observable(observer => {
            that.loadingService.hide();
            observer.next(this.getData());
            observer.complete();
        });

    }

    
}
