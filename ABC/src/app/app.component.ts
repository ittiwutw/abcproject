import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MenuService } from '../services/menu-service';
import { AppSettings } from '../services/app-settings';

import { CompanyListPage } from '../pages/company-list/company-list';
import { HomePage } from '../pages/home/home';

import { IService } from '../services/IService';
import { RestProvider } from '../providers/rest/rest';
import { Events } from 'ionic-angular';


import { LoginPage } from '../pages/login/login';
import {ProfilePage} from '../pages/profile/profile';
import {AddCompanyPage} from '../pages/add-company/add-company';

import { Storage } from '@ionic/storage';


@Component({
  templateUrl: 'app.html',
  providers: [MenuService]
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage = "HomePage";
  pages: any;
  params: any;
  leftMenuTitle: string;
  profilePages: any;
  menuLang: any;

  showLevel1 = null;
  showLevel2 = null;

  showLevel1Profile = null;
  showLevel2Profile = null;

  isLogin = false;
  isCoperateUser = false;


  constructor(public platform: Platform,
    public menu: MenuController,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menuService: MenuService,
    public modalCtrl: ModalController,
    public rest: RestProvider,
    public events: Events, 
    private storage: Storage) {
    this.initializeApp();

    //this.pages = menuService.getAllThemes();

    // set default Language
    this.rest.setLanguage('1');
    this.getMenuList();
    this.leftMenuTitle = menuService.getTitle();
    this.menuService.load(null).subscribe(snapshot => {
      this.params = snapshot;
      if (AppSettings.SHOW_START_WIZARD) {
        this.presentProfileModal();
      }
    });

    this.storage.get("user").then((val) => {
      //console.log(val);
      if(val != null){
         this.rest.setUserLogin(val);
         this.isLogin = true;
        if(this.rest.currentUser.type == 'Corporate User'){
          this.isCoperateUser = true;
        }
      }
      
    });

    events.subscribe('language:changed', () => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.getMenuList();
      this.rest.getMenuLang();
    });

    events.subscribe('login:changed', () => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      if(this.rest.currentUser){
        this.isLogin = true;
        if(this.rest.currentUser.type == 'Corporate User'){
          this.isCoperateUser = true;
        }
      }
    });

    events.subscribe('logout:changed', () => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      if(!this.rest.currentUser){
        this.isLogin = false;
        this.isCoperateUser = false;
      }
    });


  }

  getMenuList() {

    this.rest.getMenuLang().then(data => {
      if(this.rest.currentMenu){
        this.menuLang = this.rest.currentMenu;
      }
    });;

    this.rest.getMenuList().then(data => {
      if (data) {

        this.pages = data;
        this.nav.setRoot(HomePage);

      } else {
        console.log("wrong")
      }
    });

    

    this.profilePages = [
      {
        "menu": "My Company",
        "subMenu": [
          {
            "subMenu": "My Company",
            "page": "myCompany"
          },
          {
            "subMenu": "Add / Edit My Company",
            "page": "editMyCompany"
          }
        ]
      }
    ];

    //this.profilePages = [];

    //this.profilePages  = null;
  }

  presentProfileModal() {
    const profileModal = this.modalCtrl.create("IntroPage");
    profileModal.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      localStorage.setItem("mailChimpLocal", "true");
    });
  }

  openPage(menuId) {
    // close the menu when clicking a link from the menu
    // navigate to the new page if it is not the current page
    //if (page.singlePage) {
    //  this.menu.open();
    this.nav.setRoot(CompanyListPage, { "menuId": menuId });
    //} else {
    //  this.nav.setRoot("ItemsPage", {
    //    componentName: page.theme
    //  });
    //}
  };

  openProfilePage(page) {
    if(page === 'editMyCompany'){
      this.nav.setRoot(AddCompanyPage);
    }
    
    
  };

  openHomePage() {
    // close the menu when clicking a link from the menu
    // navigate to the new page if it is not the current page
    //if (page.singlePage) {
    //  this.menu.open();
    this.nav.setRoot(HomePage);
    //} else {
    //  this.nav.setRoot("ItemsPage", {
    //    componentName: page.theme
    //  });
    //}
  };

  getPageForOpen(value: string): any {
    return null;
  }

  getServiceForPage(value: string): IService {
    return null;
  }

  toggleLevel1(idx) {
    if (this.isLevel1Shown(idx)) {
      this.showLevel1 = null;
    } else {
      this.showLevel1 = idx;
    }
  }

  toggleLevel2(idx) {
    if (this.isLevel2Shown(idx)) {
      this.showLevel1 = null;
      this.showLevel2 = null;
    } else {
      this.showLevel1 = idx;
      this.showLevel2 = idx;
    }
  }

  isLevel1Shown(idx) {
    return this.showLevel1 === idx;
  }

  isLevel2Shown(idx) {
    return this.showLevel2 === idx;
  }

  toggleLevel1Profile(idx) {
    if (this.isLevel1ShownProfile(idx)) {
      this.showLevel1Profile = null;
    } else {
      this.showLevel1Profile = idx;
    }
  }

  toggleLevel2Profile(idx) {
    if (this.isLevel2ShownProfile(idx)) {
      this.showLevel1Profile = null;
      this.showLevel2Profile = null;
    } else {
      this.showLevel1Profile = idx;
      this.showLevel2Profile = idx;
    }
  }

  isLevel1ShownProfile(idx) {
    return this.showLevel1Profile === idx;
  }

  isLevel2ShownProfile(idx) {
    return this.showLevel2Profile === idx;
  }

  login(){
    if(this.rest.currentUser){
      this.nav.setRoot(ProfilePage);
    }else{
      this.nav.setRoot(LoginPage);
    }
  }

  logout(){
    this.rest.logout();
    this.nav.setRoot(HomePage);
  }
}
