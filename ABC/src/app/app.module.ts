import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { MyApp } from './app.component';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { AppSettings } from '../services/app-settings'
import { ToastService } from '../services/toast-service'
import { LoadingService } from '../services/loading-service'

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { Camera } from '@ionic-native/camera';

import { CompanyListPage } from '../pages/company-list/company-list';
import { CompanyProfilePage } from '../pages/company-profile/company-profile';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { AddCompanyPage } from '../pages/add-company/add-company';
import { HomePageModule } from '../pages/home/home.module';
import { ProfilePage } from '../pages/profile/profile';
import { PaymentModalPage } from '../pages/payment-modal/payment-modal';

import { AppearanceAnimationLayout2 } from '../components/list-view/appearance-animation/layout-2/appearance-animation-layout-2';
import { ProfileLayout5 } from '../components/profile/layout-5/profile-layout-5';
import { UserProfileLayout } from '../components/profile/user-profile/user-profile';
import { AddCompanyLayout } from '../components/profile/add-company/add-company-layout';
import { RegisterLayout2 } from '../components/register/layout-2/register-layout-2';
import { LoginLayout1 } from '../components/login/layout-1/login-layout-1';

import { WizardLayout2 } from '../components/wizard/layout-2/wizard-layout-2';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { RestProvider } from '../providers/rest/rest';

import { AgmCoreModule } from '@agm/core';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';

import { Device } from '@ionic-native/device/ngx';

import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { IonicStorageModule } from '@ionic/storage';



@NgModule({
  declarations: [
    MyApp,
    CompanyListPage,
    CompanyProfilePage,
    RegisterPage,
    LoginPage,
    ProfilePage,
    PaymentModalPage,
    ProfileLayout5,
    AddCompanyPage,
    RegisterLayout2,
    LoginLayout1,
    WizardLayout2,
    UserProfileLayout,
    AddCompanyLayout,
    AppearanceAnimationLayout2],
  imports: [
    BrowserModule,
    HttpClientModule,
    HomePageModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      platforms: {
        ios: {
          statusbarPadding: true
        }
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCB7jsMv0hjwkVRan7vZZ9hDlI-dQNZRV8'
    })

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CompanyListPage,
    CompanyProfilePage,
    RegisterPage,
    LoginPage,
    ProfilePage,
    PaymentModalPage,
    ProfileLayout5,
    AddCompanyPage,
    RegisterLayout2,
    LoginLayout1,
    WizardLayout2,
    UserProfileLayout,
    AddCompanyLayout,
    AppearanceAnimationLayout2],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    StatusBar, SplashScreen, BarcodeScanner, Camera, Device, UniqueDeviceID, CallNumber,
    ToastService, LoadingService, FileTransfer, InAppBrowser, InAppPurchase,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    RestProvider]
})
export class AppModule { }
