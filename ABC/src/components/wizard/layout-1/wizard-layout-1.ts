import { Component, Input, ViewChild, OnChanges } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import {LoginPage} from '../../../pages/login/login'

@IonicPage()
@Component({
    selector: 'wizard-layout-1',
    templateUrl: 'wizard.html'
})
export class WizardLayout1 {
    @Input() data: any;
    

    constructor(public navCtrl: NavController) {
       
    }


    openLogin(){
        this.navCtrl.setRoot(LoginPage);
      }
}
