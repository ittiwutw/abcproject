import { Component, Input } from '@angular/core';
import { IonicPage, NavController, Platform, AlertController } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';

import { LoginPage } from '../../../pages/login/login';
import { ProfilePage } from '../../../pages/profile/profile';
import { Events } from 'ionic-angular';


import { CompanyProfilePage } from '../../../pages/company-profile/company-profile';

import { RestProvider } from '../../../providers/rest/rest';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@IonicPage()
@Component({
  selector: 'wizard-layout-2',
  templateUrl: 'wizard.html'
})
export class WizardLayout2 {
  @Input() data: any;
  lang: any;
  banners: any;
  countryName: any;

  constructor(public navCtrl: NavController, public rest: RestProvider, public events: Events, public platform: Platform,
    private statusBar: StatusBar, private alertCtrl: AlertController, private iab: InAppBrowser) {
    platform.ready().then(() => {
      this.statusBar.overlaysWebView(false);

      //if(window.MobileAccessibility){
      //window.MobileAccessibility.usePreferredTextZoom(false);
      //}
    });
    this.lang = rest.getLanguage();
    this.loadBanner();
  }

  openLogin() {
    if(this.rest.currentUser){
      this.navCtrl.setRoot(ProfilePage);
    }else{
      this.navCtrl.setRoot(LoginPage);
    }
    
  }

  onClickLanguage(languageId) {
    this.rest.setLanguage(languageId);
    this.events.publish('language:changed');
  }

  loadBanner() {
    let that = this;
    this.rest.getBanners().then((result: any) => {
      if (result) {
        that.banners = result;
        console.log(that.banners);
      } else {
        console.log("wrong")
      }
    });

  }

  onClickBanner(companyId, url) {

  }

  presentAlert(bannerDetail: any): void {
    let that = this;
    let alert = this.alertCtrl.create({
      title: "Confirm",
      subTitle: "Confirm to exit current page ?",
      cssClass: "info-dialog",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            //that.onClickBanner(bannerDetail.companyId, bannerDetail.url);
            if (bannerDetail.linkType == '2') {
              this.navCtrl.push(CompanyProfilePage, { companyId: bannerDetail.companyId });
            } else {
              this.iab.create(bannerDetail.link, '_system');
            }
          }
        }
      ]
    });
    alert.present();
  }
}
