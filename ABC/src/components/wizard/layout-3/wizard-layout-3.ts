import { Component, Input, ViewChild, OnChanges } from '@angular/core';
import { IonicPage, Slides, NavController } from 'ionic-angular';


import { RestProvider } from '../../../providers/rest/rest';

@IonicPage()
@Component({
    selector: 'wizard-layout-3',
    templateUrl: 'wizard.html'
})
export class WizardLayout3 implements OnChanges {
    @Input() data: any;
    @Input() events: any;
    @ViewChild('wizardSlider') slider: Slides;
    sliderOptions = { pager: true };
    path: boolean = false;
    prev: boolean = true;
    next: boolean = true;
    finish: boolean = true;
    banners: any;

    constructor(public navCtrl: NavController, public rest: RestProvider) {
        this.prev = false;
        this.next = true;
        this.finish = false;
        this.loadBanner();
    }

    loadBanner() {
        let that = this;
        this.rest.getBanners().then((result: any) => {
          if (result) {
            that.banners = result;
            console.log(that.banners);
          } else {
            console.log("wrong")
          }
        });
    
      }

    changeSlide(index: number): void {
        if (index > 0) {
            this.slider.slideNext(300);
        } else {
            this.slider.slidePrev(300);
        }
    }

    slideHasChanged(index: number): void {
        try {
            this.prev = !this.slider.isBeginning();
            this.next = this.slider.getActiveIndex() < (this.slider.length() - 1);
            this.finish = this.slider.isEnd();
        } catch (e) { }
    }

    ngOnChanges(changes: { [propKey: string]: any }) {
        this.data = changes['data'].currentValue;
    }

    onEvent(event: string) {
        if (this.events[event]) {
            this.events[event]();
        }
        console.log(event);
    }

    onClickBanner(companyId, url) {
        //this.navCtrl.push(CompanyProfilePage, { companyId: companyId });
    }
}
