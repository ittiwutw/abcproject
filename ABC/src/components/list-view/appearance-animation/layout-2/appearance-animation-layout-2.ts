import { Component, Input, ViewChild, AfterViewInit, OnChanges } from '@angular/core';
import { IonicPage, Content, FabButton, Events, NavController } from 'ionic-angular';

import { RestProvider } from '../../../../providers/rest/rest';

@IonicPage()
@Component({
    selector: 'appearance-animation-layout-2',
    templateUrl: 'appearance-animation.html'
})
export class AppearanceAnimationLayout2 implements AfterViewInit, OnChanges {
    @Input() data: any;
    @Input() events: any;
    @ViewChild(Content)
    content: Content;
    @ViewChild(FabButton)
    fabButton: FabButton;
    imgUrl = "assets/images/company-logo/mockup-comp-logo.jpg";

    animateItems = [];
    animateClass: any;
    searchTerm: string = '';

    allData = []; //Store all data from provider
    filterData = [];//Store filtered data

    countryName: any;
    menuId: any;


    constructor(public rest: RestProvider, public ev: Events, public navCtrl: NavController) {

        ev.subscribe('changeCountry:changed', () => {
            // user and time are the same arguments passed in `events.publish(user, time)`
            this.filterData = [];
            this.allData = [];
            this.animateItems = [];
            this.getCompanyList();
        });

        if (this.rest.currentCountry) {

            this.countryName = this.rest.currentCountry.countryName;

        } else {
            this.countryName = "THAILAND"
        }

        //

        this.animateClass = { 'fade-in-right-item': true };

        //this.getCompanyList();
        console.log(this.data)
    }

    onEvent(event: string, item: any, e: any) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    }

    ngOnChanges(changes: { [propKey: string]: any }) {
        let that = this;
        this.menuId = this.data.menuId;
        this.rest.getListCompanyByProductType(that.menuId).then((result: any) => {
            if (result) {
                result.anpc_profile_url = that.imgUrl;
                that.data = changes['data'].currentValue;
                that.data = result;
                if (result) {
                    for (let i = 0; i < result.length; i++) {
                        setTimeout(function () {
                            that.animateItems.push(result[i]);
                        }, 200 * i);
                    }
                    that.allData = that.animateItems;
                    that.filterData = that.allData;
                    console.log(that.animateItems);
                }

            } else {
                console.log("wrong")
            }
        });

    }

    ngAfterViewInit() {
        //this.content.ionScroll.subscribe((d) => {
        //    this.fabButton.setElementClass("fab-button-out", d.directionY == "down");
        //});
    }

    getCompanyList() {
        let that = this;
        this.rest.getListCompanyByProductType(that.menuId).then((result: any) => {
            if (result) {
                result.anpc_profile_url = that.imgUrl;
                that.data = result;
                if (result) {
                    for (let i = 0; i < result.length; i++) {
                        setTimeout(function () {
                            that.animateItems.push(result[i]);
                        }, 200 * i);
                    }
                    that.allData = that.animateItems;
                    that.filterData = that.allData;
                    console.log(that.animateItems);
                }

            } else {
                console.log("wrong")
            }
        });

    }

    filterItems() {
        let that = this;
        return this.animateItems.filter((item) => {
            return item.alpc_company_name.toLowerCase().indexOf(that.searchTerm.toLowerCase()) > -1;
        });

    }

    setFilteredList() {
        this.filterData = this.allData.filter((item) => {
            return item.alpc_company_name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
        });
    }

    inputFilter() {
        this.allData = this.animateItems;
        this.filterData = this.allData;
        //this.animateItems = this.filterItems();
    }

    doInfinite(infiniteScroll) {
        console.log('Begin async operation');

        setTimeout(() => {
            for (let i = 0; i < 30; i++) {
                this.filterData.push("a");
            }

            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 500);
    }


}
