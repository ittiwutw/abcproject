import { Component, Input } from '@angular/core';
import { IonicPage, AlertController, NavController, Platform } from 'ionic-angular';
import { RestProvider } from '../../../providers/rest/rest';

import { Device } from '@ionic-native/device/ngx';
import { HomePage } from '../../../pages/home/home';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';

@IonicPage()
@Component({
    selector: 'register-layout-2',
    templateUrl: 'register.html'
})
export class RegisterLayout2 {
    @Input() data: any;
    @Input() events: any;

    bgImage = 'assets/images/background/29.jpg';

    public username: string;
    public password: string;
    public country: string;
    public city: string;
    public email: string;
    public name: string;
    public tel: string;
    public businessField: string;
    public profession: string;
    public userType: string;
    public agree: string;
    public uuid: string;


    public accoutTypes: any;
    public countries: any;

    private isEmailValid: boolean = true;
    private isUsernameValid: boolean = true;
    private isPasswordValid: boolean = true;
    private isCityValid: boolean = true;
    private isCountryValid: boolean = true;
    private isNameValid: boolean = true;
    private isTelValid: boolean = true;
    private isBusinessFieldValid: boolean = true;
    private isProfessionValid: boolean = true;
    private isUserTypeValid: boolean = true;
    private isAgree: boolean = true;

    private regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    constructor(public rest: RestProvider, private device: Device, private alertCtrl: AlertController, public navCtrl: NavController,
        public plt: Platform, private uniqueDeviceID: UniqueDeviceID) {
        this.getAccoutType();
        this.getCountries();

        let that = this;
        if (this.plt.is('cordova')) {
            this.uniqueDeviceID.get()
                .then((uuid: any) => 
                that.uuid = uuid
                )
                .catch((error: any) => console.log(error));
        }




    }


    getAccoutType() {
        let that = this;
        this.rest.getAccoutType().then((result: any) => {
            if (result) {
                that.accoutTypes = result.listAccountType;
                console.log(that.accoutTypes);
            } else {
                console.log("wrong")
            }
        });

    }

    getCountries() {
        let that = this;
        this.rest.getCountries().then((result: any) => {
            if (result) {
                that.countries = result.listCountry;
                console.log(that.countries);
            } else {
                console.log("wrong")
            }
        });

    }

    onEvent = (event: string): void => {
        if (event == "onRegister" && !this.validate()) {
            return;
        }
        if (this.events[event]) {
            this.events[event]({
                'username': this.username,
                'password': this.password,
                'country': this.country,
                'city': this.city,
                'email': this.email,
                'name': this.name,
                'tel': this.tel,
                'businessField': this.businessField,
                'profession': this.profession,
                'userType': this.userType
            });
        }
    }

    validate(): boolean {
        this.isEmailValid = true;
        this.isUsernameValid = true;
        this.isPasswordValid = true;
        this.isCityValid = true;
        this.isCountryValid = true;
        this.isNameValid = true;
        this.isTelValid = true;
        this.isBusinessFieldValid = true;
        this.isProfessionValid = true;
        this.isUserTypeValid = true;
        this.isAgree = true;

        if (!this.username || this.username.length == 0) {
            this.isUsernameValid = false;
        }

        if (!this.password || this.password.length == 0) {
            this.isPasswordValid = false;
        }

        if (!this.password || this.password.length == 0) {
            this.isPasswordValid = false;
        }

        if (!this.city || this.city.length == 0) {
            this.isCityValid = false;
        }

        if (!this.country || this.country.length == 0) {
            this.isCountryValid = false;
        }

        if (!this.name || this.name.length == 0) {
            this.isNameValid = false;
        }

        if (!this.tel || this.tel.length == 0) {
            this.isTelValid = false;
        }

        if (!this.businessField || this.businessField.length == 0) {
            this.isBusinessFieldValid = false;
        }

        if (!this.profession || this.profession.length == 0) {
            this.isProfessionValid = false;
        }

        if (!this.userType || this.userType.length == 0) {
            this.isUserTypeValid = false;
        }

        if (!this.email || this.email.length == 0) {
            this.isEmailValid = false;
        }

        if (!this.agree || this.agree.length == 0) {
            this.isAgree = false;
        }

        //this.isEmailValid = this.regex.test(this.email);

        return this.isEmailValid &&
            this.isPasswordValid &&
            this.isUsernameValid &&
            this.isCityValid &&
            this.isCountryValid &&
            this.isNameValid &&
            this.isTelValid &&
            this.isBusinessFieldValid &&
            this.isProfessionValid &&
            this.isUserTypeValid &&
            this.isAgree;
    }

    clickRegister() {

        let that = this;
        let Data = {
            username: this.username.replace(/'/g, "\\'"),
            password: this.password.replace(/'/g, "\\'"),
            name: this.name.replace(/'/g, "\\'"),
            email: this.email.replace(/'/g, "\\'"),
            country: this.country,
            city: this.city.replace(/'/g, "\\'"),
            phone: this.tel.replace(/'/g, "\\'"),
            bussinessField: this.businessField.replace(/'/g, "\\'"),
            profession: this.profession.replace(/'/g, "\\'"),
            UID: this.uuid,
            accountTypeId: this.userType
        }
        if (this.validate()) {
            this.rest.register(Data).then((result) => {
                that.presentAlert();
            }, (err) => {
                // Error log
            });
        }

    }

    presentAlert() {
        let that = this;
        let alert = this.alertCtrl.create({
            title: "Register completed",
            subTitle: "Please wait for Approval",
            cssClass: "info-dialog",
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {
                        this.navCtrl.setRoot(HomePage);
                    }
                }
            ]
        });
        alert.present();
    }
}
