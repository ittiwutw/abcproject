import { Component, Input } from '@angular/core';
import { IonicPage, NavController, Events, Platform } from 'ionic-angular';


import { ProfilePage } from '../../../pages/profile/profile'

import { RestProvider } from '../../../providers/rest/rest';

import { UniqueDeviceID } from '@ionic-native/unique-device-id';

@IonicPage()
@Component({
    selector: 'login-layout-1',
    templateUrl: 'login.html'
})
export class LoginLayout1 {
    @Input() data: any;
    @Input() events: any;

    public username: string;
    public password: string;

    public wrong: string;
    public uuid: string;

    private isUsernameValid: boolean = true;
    private isPasswordValid: boolean = true;
    private wrongUsernameOrPassword: boolean = false;
    responseUser: any;
    constructor(public navCtrl: NavController, public rest: RestProvider, public eventsTrigger: Events,
        private uniqueDeviceID: UniqueDeviceID, public plt: Platform) {
        let that = this;
        if (this.plt.is('cordova')) {
            this.uniqueDeviceID.get()
                .then((uuid: any) =>
                    that.uuid = uuid
                )
                .catch((error: any) => console.log(error));
        }else{
            this.uuid = 'browser';
        }
    }

    onEvent = (event: string): void => {
        if (event == "onLogin" && !this.validate()) {
            return;
        }
        if (this.events[event]) {
            this.events[event]({
                'username': this.username,
                'password': this.password
            });
        }
    }

    validate(): boolean {
        this.isUsernameValid = true;
        this.isPasswordValid = true;
        if (!this.username || this.username.length == 0) {
            this.isUsernameValid = false;
        }

        if (!this.password || this.password.length == 0) {
            this.isPasswordValid = false;
        }

        return this.isPasswordValid && this.isUsernameValid;
    }

    clickLogin() {

        let that = this;
        if (this.validate()) {
            if(!this.uuid){
                this.uuid = 'browser';
            }
            //this.username = 'user01'
            //this.password = 'test1'
            this.rest.login(this.username, this.password, this.uuid).then((result: any) => {
                if (result.aau_status == 0 || result.aau_status == 1) {
                    that.wrong = result.msg;
                    this.wrongUsernameOrPassword = true;
                } else {
                    this.responseUser = result;
                    if (this.responseUser) {

                        that.eventsTrigger.publish('login:changed');
                        this.navCtrl.setRoot(ProfilePage);
                    }
                }

            }, (err) => {
                // Error log
            });
        }

    }
}
