import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfileLayout } from './user-profile';

@NgModule({
    declarations: [
        //UserProfileLayout,
    ],
    imports: [
        IonicPageModule.forChild(UserProfileLayout),
    ],
    exports: [
        //UserProfileLayout
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class UserProfileModule { }
