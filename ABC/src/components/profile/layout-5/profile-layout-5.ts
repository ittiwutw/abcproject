import { Component, Input, ViewChild, OnChanges } from '@angular/core';
import { IonicPage, Content } from 'ionic-angular';

import { RestProvider } from '../../../providers/rest/rest';
import { CallNumber } from '@ionic-native/call-number/ngx';

@IonicPage()
@Component({
    selector: 'profile-layout-5',
    templateUrl: 'profile.html'
})
export class ProfileLayout5 implements OnChanges {

    @Input() data: any;
    @Input() events: any;
    @ViewChild(Content)
    content: Content;
    slider = {};

    companyData: any;
    companyId: any;
    hiringData: any;
    departmentContactData: any;
    companyImg: any;
    isPremium = true;

    constructor(public rest: RestProvider, private callNumber: CallNumber) {

        if (rest.currentUser) {
            if (rest.currentUser.member == 'Premium User') {
                //alert("PRE")
                this.isPremium = true;
            }
        }
    }

    phoneCall(number) {
        console.log("call")
        // this.callNumber.callNumber(number, true)
        //     .then(res => console.log('Launched dialer!', res))
        //     .catch(err => console.log('Error launching dialer', err));
        setTimeout(() => {
            let tel = '12345678890';
            window.open(`tel:${number}`, '_system');
        }, 100);
    }


    onClickEvent(index): void {
        if (this.slider[index]) {
            this.slider[index].slidePrev(300);
        }
    }

    onEvent(event: string, item: any, e: any) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    }

    ngOnChanges(changes: { [propKey: string]: any }) {
        let that = this;
        this.rest.getCompanyProfile(that.data.companyId).then((result: any) => {
            if (result) {
                that.companyData = result;
                that.hiringData = result.company_info.looking_for;
                that.departmentContactData = result.company_info.department_contact;
                that.companyImg = result.company_info.imgURL;

            } else {
                console.log("wrong")
            }
        });

    }
}
