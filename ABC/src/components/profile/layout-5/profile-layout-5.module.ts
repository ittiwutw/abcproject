import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileLayout5 } from './profile-layout-5';


import { AppSettings } from '../../../services/app-settings';
import { AgmCoreModule } from '@agm/core';

@NgModule({
    declarations: [
        //ProfileLayout5,
    ],
    imports: [
        AgmCoreModule.forRoot(AppSettings.MAP_KEY),
        IonicPageModule.forChild(ProfileLayout5),
    ],
    exports: [
        //ProfileLayout5
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ProfileLayout5Module { }
