import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCompanyLayout } from './add-company-layout';


import { AppSettings } from '../../../services/app-settings';
import { AgmCoreModule } from '@agm/core';

@NgModule({
    declarations: [
        //AddCompanyLayout
    ],
    imports: [
        IonicPageModule.forChild(AddCompanyLayout),
        AgmCoreModule.forRoot(AppSettings.MAP_KEY)
    ],
    exports: [
        //AddCompanyLayout
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AddCompanyModule { }
