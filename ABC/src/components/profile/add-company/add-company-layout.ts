import { Component, Input, ViewChild, OnChanges } from '@angular/core';
import { IonicPage, Content, normalizeURL, LoadingController, ToastController, Platform } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { AlertController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { RestProvider } from '../../../providers/rest/rest';

declare let window: any;
declare var cordova: any;

@IonicPage()
@Component({
    selector: 'add-company-layout',
    templateUrl: 'add-company-layout.html'
})
export class AddCompanyLayout implements OnChanges {

    @Input() data: any;
    @Input() events: any;
    @ViewChild(Content)
    @ViewChild('slides') slides: Slides;
    content: Content;
    slider = {};

    companyData: any;
    companyId: any;
    hiringData: any;
    departmentContactData: any;
    photos: any = [];

    options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true,
        saveToPhotoAlbum: true,
    }

    item = {
        'description': '',
        'stars': 1,
        'imageUrl': ''
    };
    imageURI: any;
    imageFileName: any;
    lastImage: string = null;

    base64img: string = '';

    constructor(private camera: Camera, public rest: RestProvider,
        public alertCtrl: AlertController, private transfer: FileTransfer,
        public loadingCtrl: LoadingController, public toastCtrl: ToastController, public platform: Platform) {

    }

    openGallery() {
        let cameraOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.FILE_URI,
            quality: 100,
            targetWidth: 1000,
            targetHeight: 1000,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
        }

        let that = this;
        // Get the data of an image
        this.camera.getPicture(cameraOptions).then((imagePath) => {
            // Special handling for Android library
            that.imageURI = imagePath;
            that.uploadFile();
        }, (err) => {
            this.presentToast('Error while selecting image.');
        });
    }

    uploadFile() {
        let loader = this.loadingCtrl.create({
            content: "Uploading..."
        });
        loader.present();
        const fileTransfer: FileTransferObject = this.transfer.create();

        let options: FileUploadOptions = {
            fileKey: 'ionicfile',
            fileName: 'ionicfile',
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'imageProfile': 'test' }
        }

        fileTransfer.upload(this.imageURI, 'http://fullsnacks.in/ABC-API/uploadImage.php', options)
            .then((data) => {
                console.log(data + " Uploaded Successfully");
                this.imageFileName = data;
                loader.dismiss();
                this.presentToast("Image uploaded successfully");
            }, (err) => {
                console.log("UploarFailed : " + err);
                loader.dismiss();
                this.presentToast(err);
            });

    }

    uploadPic() {
        let loader = this.loadingCtrl.create({
            content: "Uploading...."
        });
        loader.present();
        this.imageCapturedGallery();
        const fileTransfer: FileTransferObject = this.transfer.create();

        let options: FileUploadOptions = {
            fileKey: "photo",
            fileName: "test3.jpg",
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {}
        }

        fileTransfer.upload(this.base64img, 'http://localhost/ABC-API/upload.php', options).then(data => {
            alert(JSON.stringify(data));
            loader.dismiss();
        }, error => {
            alert("error");
            alert("error" + JSON.stringify(error));
            loader.dismiss();
        });
    }

    imageCapturedGallery(){
        let that = this;
        const options:CameraOptions={
          quality:70,
          destinationType:this.camera.DestinationType.DATA_URL,
          sourceType:this.camera.PictureSourceType.PHOTOLIBRARY,
          saveToPhotoAlbum:false
        }
        this.camera.getPicture(options).then((ImageData=>{
           this.base64img="data:image/jpeg;base64,"+ImageData;

           that.uploadPic();
        }),error=>{
          console.log(error);
        })
      }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });

        toast.present();
    }

    addPhoto(photo) {
        this.photos.push(photo);
        this.photos.reverse();
    }

    displayErrorAlert(err) {
        console.log(err);
        let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Error while trying to capture picture',
            buttons: ['OK']
        });
        alert.present();
    }


    onClickEvent(index): void {
        if (this.slider[index]) {
            this.slider[index].slidePrev(300);
        }
    }

    onEvent(event: string, item: any, e: any) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    }

    ngOnChanges(changes: { [propKey: string]: any }) {
        let that = this;
        this.rest.getCompanyProfile(that.data.companyId).then((result: any) => {
            if (result) {
                that.companyData = result;
                that.hiringData = result.company_info.looking_for;
                that.departmentContactData = result.company_info.department_contact;

            } else {
                console.log("wrong")
            }
        });

    }

    next() {
        this.slides.slideNext();
    }

    prev() {
        this.slides.slidePrev();
    }
}
